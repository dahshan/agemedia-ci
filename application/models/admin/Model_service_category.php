<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_service_category extends CI_Model 
{
	
    function show() {
        $sql = "SELECT * 
                FROM tbl_service_category t1
                JOIN tbl_lang t2
                ON t1.lang_id = t2.lang_id
                ORDER BY t1.category_id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function show_service_by_id($id) {
        $sql = "SELECT * FROM tbl_service WHERE id=?";
        $query = $this->db->query($sql,$id);
        return $query->result_array();
    }

    function show_service_photo_by_service_id($id) {
        $sql = "SELECT * FROM tbl_service_photo WHERE service_id=?";
        $query = $this->db->query($sql,$id);
        return $query->result_array();
    }

    function add($data) {
        $this->db->insert('tbl_service_category',$data);
        return $this->db->insert_id();
    }

    function update($id,$data) {
        $this->db->where('category_id',$id);
        $this->db->update('tbl_service_category',$data);
    }

    function delete($id)
    {
        $this->db->where('category_id',$id);
        $this->db->delete('tbl_service_category');
    }

    function delete1($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_service');
    }

    function delete2($id)
    {
        $this->db->where('service_id',$id);
        $this->db->delete('tbl_service_photo');
    }

    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_service_category WHERE category_id=?';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }

    function getData1($id)
    {
        $sql = 'SELECT * FROM tbl_service WHERE category_id=?';
        $query = $this->db->query($sql,array($id));
        return $query->result_array();
    }

    function service_category_check($id)
    {
        $sql = 'SELECT * FROM tbl_service_category WHERE category_id=?';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }

    function duplicate_check($var1,$var2) {
        $sql = 'SELECT * FROM tbl_service_category WHERE category_name=? and category_name!=?';
        $query = $this->db->query($sql,array($var1,$var2));
        return $query->num_rows();
    }
    
}