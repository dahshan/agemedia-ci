<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_subscription extends CI_Model 
{


    function show($count = false) {
        $this->db->select('tbl_sub.*, tbl_user.id as user_id, tbl_user.name as user_name, emp.name as employee_name');
        $this->db->from('tbl_sub');
        $this->db->join('tbl_user', 'tbl_sub.user_id = tbl_user.id');
        $this->db->join('tbl_user as emp', 'tbl_sub.employee_id = emp.id', 'left');
        $this->db->where('DATE_ADD(`date_time`,INTERVAL (`plan_duration` + 1 ) DAY) >','NOW()', FALSE);
        $this->db->where('employee_id !=', '');
        $this->db->where('payment_status', '1');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();

        if ($count) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    function new($count = false) {
        $this->db->select('tbl_sub.*, tbl_user.id as user_id, tbl_user.name as user_name, emp.name as employee_name');
        $this->db->from('tbl_sub');
        $this->db->join('tbl_user', 'tbl_sub.user_id = tbl_user.id');
        $this->db->join('tbl_user as emp', 'tbl_sub.employee_id = emp.id', 'left');
        $this->db->where('DATE_ADD(`date_time`,INTERVAL (`plan_duration` + 1 ) DAY) >','NOW()', FALSE);
        $this->db->where('employee_id', '');
        $this->db->where('payment_status', '1');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();

        if ($count) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    function archive($count = false) {
        $this->db->select('tbl_sub.*, tbl_user.id as user_id, tbl_user.name as user_name, emp.name as employee_name');
        $this->db->from('tbl_sub');
        $this->db->join('tbl_user', 'tbl_sub.user_id = tbl_user.id');
        $this->db->join('tbl_user as emp', 'tbl_sub.employee_id = emp.id', 'left');
        $this->db->where('DATE_ADD(`date_time`,INTERVAL (`plan_duration` + 1) DAY) <=','NOW()', FALSE);
        $this->db->where('payment_status', '1');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();

        if ($count) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    function show_one($id) {
        $this->db->select('tbl_sub.*, tbl_user.*, tbl_cv.*');
        $this->db->from('tbl_sub');
        $this->db->join('tbl_cv', 'tbl_cv.user_id = tbl_sub.user_id');
        $this->db->join('tbl_user', 'tbl_sub.user_id = tbl_user.id');
        $this->db->where('tbl_sub.id', $id);
        $query = $this->db->get();
        return $query->first_row('array');
    }

    
    public function employee()
    {
        $query = $this->db->query("SELECT * FROM tbl_user WHERE role='Editor' ORDER BY id ASC");
        return $query->result_array();
    }


    function insertToSubDay($data) {
        $this->db->insert('tbl_sub_day',$data);
        return $this->db->insert_id();
    }

    function insertToDailyWork($data) {
        $this->db->insert('tbl_daily_work',$data);
        return $this->db->insert_id();
    }


    function add($data) {
        $this->db->insert('tbl_task',$data);
        return $this->db->insert_id();
    }

    function getDailyWorkRemainingItems($where) {
        return $this->db->get_where('tbl_daily_work',$where)->result_array();
    }

    function updateDailyWorkRemainingItem($where,$employee) {
        $this->db->where($where);
        $this->db->update('tbl_daily_work', array('employee' => $employee));
    }

    function update($id,$data) {
        $this->db->where('id',$id);
        $this->db->update('tbl_sub',$data);
    }

    function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_task');
    }

    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_sub WHERE id=?';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }

    function subLastDay($id)
    {
        $sql = 'SELECT * FROM tbl_sub_day WHERE sub_id=?';
        $query = $this->db->query($sql,array($id));
        return $query->last_row('array');
    }

    function feature_check($id)
    {
        $sql = 'SELECT * FROM tbl_task WHERE id=?';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }
    

    function subscription_extended_number_of_days($id) {
        $query = $this->db->get_where('tbl_sub_day', array('sub_id' => $id, 'is_extend' => 1));
        return $query->num_rows();
    }


}