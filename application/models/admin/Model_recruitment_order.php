<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_recruitment_order extends CI_Model 
{
	
    function show() {
        $sql = "SELECT t1.* , t2.id, t2.name AS user_name, t3.id, t3.name AS team_member_name
                FROM tbl_recruitment_order t1
                JOIN tbl_user t2
                ON t1.user_id = t2.id
                LEFT JOIN tbl_team_member t3
                ON t1.team_member = t3.id
                ORDER BY t1.order_id DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function show_service_by_id($id) {
        $sql = "SELECT * FROM tbl_service WHERE id=?";
        $query = $this->db->query($sql,$id);
        return $query->result_array();
    }

    function show_service_photo_by_service_id($id) {
        $sql = "SELECT * FROM tbl_service_photo WHERE service_id=?";
        $query = $this->db->query($sql,$id);
        return $query->result_array();
    }

    function add($data) {
        $this->db->insert('tbl_recruitment_order',$data);
        return $this->db->insert_id();
    }

    function update($id,$data) {
        $this->db->where('order_id',$id);
        $this->db->update('tbl_recruitment_order',$data);
    }

    function delete($id)
    {
        $this->db->where('category_id',$id);
        $this->db->delete('tbl_recruitment_order');
    }

    function delete1($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_service');
    }

    function delete2($id)
    {
        $this->db->where('service_id',$id);
        $this->db->delete('tbl_service_photo');
    }

    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_recruitment_order WHERE order_id=?';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }

    function getTeam()
    {
        $sql = 'SELECT * FROM tbl_team_member';
        $query = $this->db->query($sql);
        return $query->result_array('array');
    }

    function getData1($id)
    {
        $sql = 'SELECT * FROM tbl_service WHERE category_id=?';
        $query = $this->db->query($sql,array($id));
        return $query->result_array();
    }

    function service_order_check($id)
    {
        $sql = 'SELECT * FROM tbl_recruitment_order WHERE order_id=?';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }

    function duplicate_check($var1,$var2) {
        $sql = 'SELECT * FROM tbl_recruitment_order WHERE category_name=? and category_name!=?';
        $query = $this->db->query($sql,array($var1,$var2));
        return $query->num_rows();
    }
    
}