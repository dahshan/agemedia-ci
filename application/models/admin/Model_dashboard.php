<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_dashboard extends CI_Model 
{
	public function show_total_category()
	{
		$sql = 'SELECT * from tbl_category';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    public function show_total_news()
	{
		$sql = 'SELECT * FROM tbl_news';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function show_total_team_member()
    {
        $sql = 'SELECT * from tbl_user WHERE role = "Editor"';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function show_total_client()
    {
        $sql = 'SELECT * from tbl_user WHERE role = "Client"';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    public function show_total_service()
    {
        $sql = 'SELECT * from tbl_service';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    public function show_total_testimonial()
    {
        $sql = 'SELECT * from tbl_testimonial';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    public function show_total_event()
    {
        $sql = 'SELECT * from tbl_event';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    public function show_total_photo()
    {
        $sql = 'SELECT * from tbl_photo';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    public function show_total_pricing_table()
    {
        $sql = 'SELECT * from tbl_pricing_table';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    

    function valid_sub() {
        $this->db->select('tbl_sub.*, tbl_user.id as user_id, tbl_user.name as user_name');
        $this->db->from('tbl_sub');
        $this->db->join('tbl_user', 'tbl_sub.user_id = tbl_user.id');
        $this->db->where('DATE_ADD(`date_time`,INTERVAL `plan_duration` DAY) >','NOW()', FALSE);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    function valid_sub_count() {
        $this->db->select('tbl_sub.*, tbl_user.id as user_id, tbl_user.name as user_name');
        $this->db->from('tbl_sub');
        $this->db->join('tbl_user', 'tbl_sub.user_id = tbl_user.id');
        $this->db->where('DATE_ADD(`date_time`,INTERVAL `plan_duration` DAY) >','NOW()', FALSE);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->num_rows();
    }

    function last_activity() {
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->limit(10);
        $this->db->where('last_activity != ""');
        $this->db->order_by('last_activity', 'DESC');

        $query = $this->db->get();
        return $query->result_array();
    }

    function message() {
        $this->db->select('tbl_user_message.*, tbl_user.id as user_id, tbl_user.name as user_name, tbl_user.phone as user_phone');
        $this->db->from('tbl_user_message');
        $this->db->join('tbl_user', 'tbl_user_message.user_id = tbl_user.id');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    function task() {
        $this->db->select('tbl_daily_work.*, tbl_sub.user_id, tbl_user.name as user_name, emp.name as employee_name');
        $this->db->from('tbl_daily_work');
        $this->db->join('tbl_sub', 'tbl_daily_work.sub_id = tbl_sub.id');
        $this->db->join('tbl_user', 'tbl_sub.user_id = tbl_user.id');
        $this->db->join('tbl_user as emp', 'tbl_daily_work.employee = emp.id');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

}