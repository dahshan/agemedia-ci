<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_assign extends CI_Model 
{

    function prepair_sub_day() {
        $this->db->select('tbl_sub.*');
        $this->db->from('tbl_sub');
        $this->db->where('DATE_ADD(`tbl_sub`.`date_time`,INTERVAL `plan_duration` DAY) >','NOW()', FALSE);
        $this->db->order_by('tbl_sub.id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    function show() {
        $this->db->select('tbl_sub.*, tbl_user.id as user_id, tbl_user.name as user_name, tbl_cv.specialization, tbl_daily_work.id as daily_work_id, tbl_daily_work.date as daily_work_date, tbl_daily_work.date_time as daily_work_date_time');
        $this->db->from('tbl_sub');
        $this->db->join('tbl_user', 'tbl_sub.user_id = tbl_user.id');
        $this->db->join('tbl_cv', 'tbl_sub.user_id = tbl_cv.user_id');
        $this->db->join('tbl_sub_day', 'tbl_sub_day.sub_id = tbl_sub.id');
        $this->db->join('tbl_daily_work', 'tbl_daily_work.sub_id = tbl_sub.id and tbl_daily_work.date = tbl_sub_day.day', 'left');
        $this->db->where('DATE_ADD(`tbl_sub`.`date_time`,INTERVAL `plan_duration` DAY) >','NOW()', FALSE);
        $this->db->where('tbl_daily_work.id IS NULL');
        $this->db->where('tbl_sub_day.day', date('Y-m-d'));
        $this->db->order_by('tbl_sub.id', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    function assign_to($data) {
        $this->db->insert('tbl_daily_work',$data);
        return $this->db->insert_id();
    }

    function have_sub_day_record($id)
    {
        $sql = 'SELECT * FROM tbl_sub_day WHERE sub_id=? AND day=?';
        $query = $this->db->query($sql,array($id, date('Y-m-d')));
        return $query->first_row('array');
    }

    function insert_sub_day_record($id) {
        $this->db->insert('tbl_sub_day',array('sub_id' => $id, 'day' => date('Y-m-d')));
        return $this->db->insert_id();
    }

    function update($id,$data) {
        $this->db->where('id',$id);
        $this->db->update('tbl_assign',$data);
    }

    function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_assign');
    }

    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_assign WHERE id=?';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }

    function feature_check($id)
    {
        $sql = 'SELECT * FROM tbl_assign WHERE id=?';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }

    public function employee()
    {
        $query = $this->db->query("SELECT * FROM tbl_user WHERE role='Editor' ORDER BY id ASC");
        return $query->result_array();
    }

    
}