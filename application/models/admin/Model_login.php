<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_login extends CI_Model 
{


    function get_auto_increment_id()
    {
        $sql = "SHOW TABLE STATUS LIKE 'tbl_user'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function get_setting_data()
    {
        $query = $this->db->query("SELECT * from tbl_settings WHERE id=1");
        return $query->first_row('array');
    }

	function check_email($email) 
	{
        $where = array(
            // 'role' => 'Admin',
			'email' => $email
		);
		$this->db->select('*');
		$this->db->from('tbl_user');
		$this->db->where($where);
		$query = $this->db->get();
		return $query->first_row('array');
    }

    function check_password($email,$password)
    {
        $where = array(
            // 'role' => 'Admin',
            'email' => $email,
            'password' => md5($password)
        );
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->first_row('array');
    }


    function add($data) {
        $this->db->insert('tbl_user',$data);
        return $this->db->insert_id();
    }

    function have_cv_record($userId) {
        $where = array(
            'user_id' => $userId
        );
        $this->db->select('*');
        $this->db->from('tbl_cv');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->first_row('array');
    }

    function have_valid_sub_record($userId) {
        $where = array(
            'user_id' => $userId
        );
        $this->db->select('*');
        $this->db->from('tbl_sub');
        $this->db->where($where);
        $this->db->where('DATE_ADD(`date_time`,INTERVAL `plan_duration` DAY) >','NOW()', FALSE);
        $query = $this->db->get();
        return $query->last_row('array');
    }


}