<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_specialization extends CI_Model 
{

    function show() {
        $sql = "SELECT * 
                FROM tbl_specialization t1";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function add($data) {
        $this->db->insert('tbl_specialization',$data);
        return $this->db->insert_id();
    }

    function update($id,$data) {
        $this->db->where('id',$id);
        $this->db->update('tbl_specialization',$data);
    }

    function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_specialization');
    }

    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_specialization WHERE id=?';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }

    function feature_check($id)
    {
        $sql = 'SELECT * FROM tbl_specialization WHERE id=?';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }
    
}