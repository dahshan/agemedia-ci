<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_task extends CI_Model 
{


    function show($user_id, $count = false) {
        $this->db->select('tbl_daily_work.*, tbl_daily_work.id as task_id, tbl_daily_work.status as task_status, tbl_sub.user_id, tbl_user.name as user_name, emp.name as employee_name');
        $this->db->from('tbl_daily_work');
        $this->db->join('tbl_sub', 'tbl_daily_work.sub_id = tbl_sub.id');
        $this->db->join('tbl_user', 'tbl_sub.user_id = tbl_user.id');
        $this->db->join('tbl_user as emp', 'tbl_daily_work.employee = emp.id');
        if ($user_id != 'Admin') {
            $this->db->where('tbl_daily_work.employee', $user_id);
        }        
        $this->db->where('tbl_daily_work.status', 'جديد');
        $this->db->where('tbl_daily_work.date', date('Y-m-d'));
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        if ($count) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    function archive($user_id, $count = false) {
        $this->db->select('tbl_daily_work.*, tbl_daily_work.id as task_id, tbl_sub.user_id, tbl_user.name as user_name, emp.name as employee_name');
        $this->db->from('tbl_daily_work');
        $this->db->join('tbl_sub', 'tbl_daily_work.sub_id = tbl_sub.id');
        $this->db->join('tbl_user', 'tbl_sub.user_id = tbl_user.id');
        $this->db->join('tbl_user as emp', 'tbl_daily_work.employee = emp.id');
        if ($user_id != 'Admin') {
            $this->db->where('tbl_daily_work.employee', $user_id);
        }        
        $this->db->where('tbl_daily_work.status', 'منتهي');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        if ($count) {
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }

    function show_one($id, $user_id) {
        $this->db->select('tbl_daily_work.*, tbl_daily_work.id as task_id, tbl_daily_work.status as task_status, tbl_sub.user_id, tbl_user.*, tbl_cv.*');
        $this->db->from('tbl_daily_work');
        $this->db->join('tbl_sub', 'tbl_daily_work.sub_id = tbl_sub.id');
        $this->db->join('tbl_cv', 'tbl_cv.user_id = tbl_sub.user_id');
        $this->db->join('tbl_user', 'tbl_sub.user_id = tbl_user.id');
        if ($user_id != 'Admin') {
            $this->db->where('tbl_daily_work.employee', $user_id);
        }
        $this->db->where('tbl_daily_work.id', $id);
        $query = $this->db->get();
        return $query->first_row('array');
    }

    function add($data) {
        $this->db->insert('tbl_task',$data);
        return $this->db->insert_id();
    }

    function update($id,$data) {
        $this->db->where('id',$id);
        $this->db->update('tbl_daily_work',$data);
    }

    function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_task');
    }

    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_task WHERE id=?';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }

    function feature_check($id)
    {
        $sql = 'SELECT * FROM tbl_task WHERE id=?';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }

    function message_check($arr)
    {
        $sql = 'SELECT * FROM tbl_message WHERE id=? AND message_to=?';
        $query = $this->db->query($sql,array($arr['id'], $arr['message_to']));
        return $query->first_row('array');
    }

    function message_seen($id) {
        $this->db->where('id',$id);
        $this->db->update('tbl_message',array('seen' => 1));
    }

    function send_reply($data) {
        $this->db->insert('tbl_message_reply',$data);
        return $this->db->insert_id();
    }

    
}