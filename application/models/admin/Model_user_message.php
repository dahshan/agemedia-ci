<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user_message extends CI_Model 
{

    function show($id=0) {
        if ($id == 0) {
            $this->db->select('*, tbl_message.date_time as message_date_time, tbl_message_reply.date_time as reply_date_time');
            $this->db->from('tbl_message');
            $this->db->join('tbl_message_reply', 'tbl_message.id = tbl_message_reply.message_id', 'left');
            $this->db->join('tbl_user', 'tbl_message.message_to = tbl_user.id');
            $this->db->order_by('tbl_message.id', 'DESC');
        } else {
            $this->db->select('*, tbl_message.date_time as message_date_time, tbl_message_reply.date_time as reply_date_time');
            $this->db->from('tbl_message');
            $this->db->join('tbl_message_reply', 'tbl_message.id = tbl_message_reply.message_id', 'left');
            $this->db->join('tbl_user', 'tbl_message.message_to = tbl_user.id');
            $this->db->where('tbl_message.message_to', $id);
            $this->db->order_by('tbl_message.id', 'DESC');
        }
        
        $query = $this->db->get();
        return $query->result_array();
    }

    function add($data) {
        $this->db->insert('tbl_place',$data);
        return $this->db->insert_id();
    }

    function update($id,$data) {
        $this->db->where('id',$id);
        $this->db->update('tbl_place',$data);
    }

    function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_place');
    }

    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_place WHERE id=?';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }

    function feature_check($id)
    {
        $sql = 'SELECT * FROM tbl_place WHERE id=?';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }
    
}