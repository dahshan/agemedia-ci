<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assign extends CI_Controller 
{
	function __construct() 
	{
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_assign');
    }

	public function index()
	{
		$data['setting'] = $this->Model_common->get_setting_data();

		$active_subs = $this->Model_assign->prepair_sub_day();
		foreach ($active_subs as $key => $value) {
			if (!$this->Model_assign->have_sub_day_record($value['id'])) {
				$this->Model_assign->insert_sub_day_record($value['id']);
			}
		}
		$data['task'] = $this->Model_assign->show();
		$data['employee'] = $this->Model_assign->employee();
		// echo "<pre>";
		// print_r($data['task']);
		// exit;
		$this->load->view('admin/view_header',$data);
		$this->load->view('admin/view_assign',$data);
		$this->load->view('admin/view_footer');
	}

	public function assign_to()
	{
		$data['setting'] = $this->Model_common->get_setting_data();
		$data['all_lang'] = $this->Model_common->all_lang();

		$error = '';
		$success = '';

		if(isset($_POST['assign'])) {
			// echo "<pre>";
			// print_r($_POST);
			// exit;
			$valid = 1;

			$this->form_validation->set_rules('sub[]', 'المهام يجب تحديدها', 'required');
			$this->form_validation->set_rules('employee', 'للموظف يجب إختياره', 'required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }

		    if($valid == 1) 
		    {
		    	foreach ($_POST['sub'] as $key => $value) {
			        $form_data = array(
						'date'    => date('Y-m-d'),
						'sub_id'    => $value,
						'status'    => 'جديد',
						'employee' => $_POST['employee']
		            );
		            $this->Model_assign->assign_to($form_data);
		    	}

		        $success = 'تم إسناد المهام للموظف بنجاح!';
		        $this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/assign');
		    } 
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/assign');
		    }
        }
		
	}


	public function edit($id)
	{
    	$tot = $this->Model_assign->feature_check($id);
    	if(!$tot) {
    		redirect(base_url().'admin/assign');
        	exit;
    	}
       	
       	$data['setting'] = $this->Model_common->get_setting_data();
       	$data['all_lang'] = $this->Model_common->all_lang();
		$error = '';
		$success = '';


		if(isset($_POST['form1'])) 
		{

			$valid = 1;

			$this->form_validation->set_rules('name', 'Name', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }
	    
		    if($valid == 1) 
		    {
		    	$data['feature'] = $this->Model_assign->getData($id);

	    		$form_data = array(
					'name'    => $_POST['name']
	            );
	            $this->Model_assign->update($id,$form_data);
				
				$success = 'Feature is updated successfully';
				$this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/assign');
		    }
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/assign/edit/'.$id);
		    }
           
		} else {
			$data['feature'] = $this->Model_assign->getData($id);
	       	$this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_assign_edit',$data);
			$this->load->view('admin/view_footer');
		}

	}


	public function delete($id) 
	{
    	$tot = $this->Model_assign->feature_check($id);
    	if(!$tot) {
    		redirect(base_url().'admin/assign');
        	exit;
    	}

        $this->Model_assign->delete($id);
        $success = 'assign is deleted successfully';
        $this->session->set_flashdata('success',$success);
        redirect(base_url().'admin/assign');
    }

}