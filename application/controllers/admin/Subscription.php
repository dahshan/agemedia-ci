<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscription extends CI_Controller 
{
	function __construct() 
	{
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_subscription');
    }

	public function index()
	{
		if(isset($_POST['re_assign'])) 
		{
			// echo "<pre>";
			// print_r($_POST);
			// exit;
			$valid = 1;

			$this->form_validation->set_rules('employee', 'الموظف مطلوب', 'trim|required');
			$this->form_validation->set_rules('sub_id', 'الإشتراك مطلوب', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }
	    
		    if($valid == 1) 
		    {
		    	$daily_work = array(
		    		'sub_id' => $_POST['sub_id'],
		    		'status' => 'جديد'
		    	);
		    	$remaining = $this->Model_subscription->getDailyWorkRemainingItems($daily_work);
		    	foreach ($remaining as $day) {
		    		$where = array(
		    			'id' => $day['id']
		    		);
		    		$this->Model_subscription->updateDailyWorkRemainingItem($where, $_POST['employee']);
		    	}

	    		$form_data = array(
					'employee_id'    => $_POST['employee']
	            );
	            $this->Model_subscription->update($_POST['sub_id'],$form_data);
				
				$success = 'تم تغيير الموظف بنجاح!';
				$this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/subscription');
		    }
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/subscription');
		    }
           
		} else {
			$data['setting'] = $this->Model_common->get_setting_data();

			$data['feature'] = $this->Model_subscription->show();
			foreach ($data['feature'] as $key => $value) {
				$data['feature'][$key]['extended'] = $this->Model_subscription->subscription_extended_number_of_days($value['id']);
			}

			$data['employee'] = $this->Model_subscription->employee();

			$this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_subscription',$data);
			$this->load->view('admin/view_footer');
		}

	}

	public function new()
	{


		if(isset($_POST['assign'])) 
		{
			// echo "<pre>";
			// print_r($_POST);
			// exit;
			$valid = 1;

			$this->form_validation->set_rules('employee', 'الموظف مطلوب', 'trim|required');
			$this->form_validation->set_rules('sub_id', 'الإشتراك مطلوب', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }
	    
		    if($valid == 1) 
		    {
		    	$sub = $this->Model_subscription->getData($_POST['sub_id']);
		    	for ($i=1; $i <= $sub['plan_duration']; $i++) { 
		    		$sub_day = array(
		    			'sub_id' => $_POST['sub_id'],
		    			'day' => date('Y-m-d',strtotime('+'.$i.' days',strtotime($sub['date_time'])))
		    		);
		    		$this->Model_subscription->insertToSubDay($sub_day);
		    		$daily_work = array(
		    			'sub_id' => $_POST['sub_id'],
		    			'employee' => $_POST['employee'],
		    			'status' => 'جديد',
		    			'date' => date('Y-m-d',strtotime('+'.$i.' days',strtotime($sub['date_time'])))
		    		);
		    		$this->Model_subscription->insertToDailyWork($daily_work);
		    	}

	    		$form_data = array(
					'employee_id'    => $_POST['employee']
	            );
	            $this->Model_subscription->update($_POST['sub_id'],$form_data);
				
				$success = 'تم إسناد الإشتراك بنجاح!';
				$this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/subscription/new');
		    }
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/subscription/new');
		    }
           
		} else {
			$data['setting'] = $this->Model_common->get_setting_data();

			$data['feature'] = $this->Model_subscription->new();
			$data['employee'] = $this->Model_subscription->employee();

			$this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_subscription_new',$data);
			$this->load->view('admin/view_footer');
		}
	}

	public function archive()
	{
		$data['setting'] = $this->Model_common->get_setting_data();
		$data['feature'] = $this->Model_subscription->archive();

		$this->load->view('admin/view_header',$data);
		$this->load->view('admin/view_subscription_archive',$data);
		$this->load->view('admin/view_footer');
	}


	public function view($id)
	{
		$tot = $this->Model_subscription->show_one($id, 'Admin');
    	// echo "<pre>";
    	// print_r($tot);
    	// exit;
    	if(!$tot) {
    		redirect(base_url().'admin/task');
        	exit;
    	}
       	
       	$data['setting'] = $this->Model_common->get_setting_data();
       	$data['all_lang'] = $this->Model_common->all_lang();
		$error = '';
		$success = '';


		if(isset($_POST['finish'])) 
		{
			// echo "<pre>";
			// print_r($_POST);
			// exit;
			$valid = 1;

			$this->form_validation->set_rules('comment', 'التعليق مطلوب', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }
	    
		    if($valid == 1) 
		    {

	    		$form_data = array(
					'status'    => 'منتهي',
					'comment'    => $_POST['comment']
	            );
	            $this->Model_subscription->update($id,$form_data);
				
				$success = 'تم إنهاء المهمة بنجاح!';
				$this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/task');
		    }
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/task');
		    }
           
		} else {
			if ($this->session->userdata('role') == 'Admin') {
				$data['task'] = $this->Model_subscription->show_one($id, 'Admin');
			} else {
				$data['task'] = $this->Model_subscription->show_one($id, $this->session->userdata('id'));
			}

			$data['task']['files'] = explode(',', $data['task']['files']);
	       	$this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_subscription_view',$data);
			$this->load->view('admin/view_footer');
		}

	}

	public function extend($id)
	{
		$tot = $this->Model_subscription->show_one($id, 'Admin');
    	// echo "<pre>";
    	// print_r($tot);
    	// exit;
    	if(!$tot) {
    		redirect(base_url().'admin/task');
        	exit;
    	}
       	
       	$data['setting'] = $this->Model_common->get_setting_data();
       	$data['all_lang'] = $this->Model_common->all_lang();
		$error = '';
		$success = '';


		if(isset($_POST['extend'])) 
		{
			// echo "<pre>";
			// print_r($_POST);
			// exit;
			$valid = 1;

			$this->form_validation->set_rules('days', 'ايام التمديد', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }
	    
		    if($valid == 1) 
		    {
		    	$sub = $this->Model_subscription->getData($id);
		    	$last_day = $this->Model_subscription->subLastDay($id);
		    	// echo "<pre>";
		    	// print_r($last_day);
		    	// exit;

		    	for ($i=1; $i <= $_POST['days']; $i++) { 
		    		$sub_day = array(
		    			'sub_id' => $id,
		    			'is_extend' => 1,
		    			'day' => date('Y-m-d',strtotime('+'.$i.' days',strtotime($last_day['day'])))
		    		);
		    		$this->Model_subscription->insertToSubDay($sub_day);
		    		$daily_work = array(
		    			'sub_id' => $id,
		    			'is_extend' => 1,
		    			'employee' => $sub['employee_id'],
		    			'status' => 'جديد',
		    			'date' => date('Y-m-d',strtotime('+'.$i.' days',strtotime($last_day['day'])))
		    		);
		    		$this->Model_subscription->insertToDailyWork($daily_work);
		    		// echo "<pre>";
		    		// print_r($sub_day);
		    		// print_r($daily_work);
		    		// exit;

		    	}
				
				$success = 'تم تمديد مدة الإشتراك بنجاح!';
				$this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/subscription');
		    }
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/subscription');
		    }
           
		} else {
			if ($this->session->userdata('role') == 'Admin') {
				$data['task'] = $this->Model_subscription->show_one($id, 'Admin');
			} else {
				$data['task'] = $this->Model_subscription->show_one($id, $this->session->userdata('id'));
			}
	       	$this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_subscription_extend',$data);
			$this->load->view('admin/view_footer');
		}

	}


	public function edit($id)
	{
    	$tot = $this->Model_subscription->feature_check($id);
    	if(!$tot) {
    		redirect(base_url().'admin/task');
        	exit;
    	}
       	
       	$data['setting'] = $this->Model_common->get_setting_data();
       	$data['all_lang'] = $this->Model_common->all_lang();
		$error = '';
		$success = '';


		if(isset($_POST['form1'])) 
		{

			$valid = 1;

			$this->form_validation->set_rules('name', 'Name', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }
	    
		    if($valid == 1) 
		    {
		    	$data['feature'] = $this->Model_subscription->getData($id);

	    		$form_data = array(
					'name'    => $_POST['name']
	            );
	            $this->Model_subscription->update($id,$form_data);
				
				$success = 'Feature is updated successfully';
				$this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/task');
		    }
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/task/edit/'.$id);
		    }
           
		} else {
			$data['feature'] = $this->Model_subscription->getData($id);
	       	$this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_subscription_edit',$data);
			$this->load->view('admin/view_footer');
		}

	}


	public function delete($id) 
	{
    	$tot = $this->Model_subscription->feature_check($id);
    	if(!$tot) {
    		redirect(base_url().'admin/task');
        	exit;
    	}

        $this->Model_subscription->delete($id);
        $success = 'task is deleted successfully';
        $this->session->set_flashdata('success',$success);
        redirect(base_url().'admin/task');
    }

}