<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_category extends CI_Controller 
{
	function __construct() 
	{
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_service_category');
    }

	public function index()
	{
		$data['setting'] = $this->Model_common->get_setting_data();

		$data['service_category'] = $this->Model_service_category->show();

		$this->load->view('admin/view_header',$data);
		$this->load->view('admin/view_service_category',$data);
		$this->load->view('admin/view_footer');
	}

	public function add()
	{
		$data['setting'] = $this->Model_common->get_setting_data();
		$data['all_lang'] = $this->Model_common->all_lang();

		$error = '';
		$success = '';

		if(isset($_POST['form1'])) {

			$valid = 1;

			$this->form_validation->set_rules('category_name', 'Category Name', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error = validation_errors();
            }

		    if($valid == 1) 
		    {
				
		        $form_data = array(
					'category_name'=> $_POST['category_name'],
					'status'       => $_POST['status'],
					'lang_id'      => $_POST['lang_id']
	            );
	            $this->Model_service_category->add($form_data);

		        $success = 'service category is added successfully!';
		        $this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/service_category');
		    }
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/service_category/add');
		    }
            
        } else {
            
            $this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_service_category_add',$data);
			$this->load->view('admin/view_footer');
        }
		
	}


	public function edit($id)
	{
    	$tot = $this->Model_service_category->service_category_check($id);
    	if(!$tot) {
    		redirect(base_url().'admin/service_category');
        	exit;
    	}
       	
       	$data['setting'] = $this->Model_common->get_setting_data();
       	$data['all_lang'] = $this->Model_common->all_lang();

		$error = '';
		$success = '';


		if(isset($_POST['form1'])) 
		{

			$valid = 1;

			$this->form_validation->set_rules('category_name', 'Category Name', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error = validation_errors();
            } else {

            	// Duplicate Category Checking
            	$data['service_category'] = $this->Model_service_category->getData($id);
            	$total = $this->Model_service_category->duplicate_check($_POST['category_name'],$data['service_category']['category_name']);				
		    	if($total) {
		    		$valid = 0;
		        	$error = 'Category name already exists';
		    	}
            }

		    if($valid == 1) 
		    {
	    		$form_data = array(
					'category_name'=> $_POST['category_name'],
					'status'       => $_POST['status'],
					'lang_id'      => $_POST['lang_id']
	            );
	            $this->Model_service_category->update($id,$form_data);
				
				$success = 'service Category is updated successfully';
				$this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/service_category');
		    }
		    else 
		    {
				$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/service_category/add');
		    }
           
		} else {
			$data['service_category'] = $this->Model_service_category->getData($id);
			$this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_service_category_edit',$data);
			$this->load->view('admin/view_footer');
		}

	}


	public function delete($id) 
	{
    	$tot = $this->Model_service_category->service_category_check($id);
    	if(!$tot) {
    		redirect(base_url().'admin/service_category');
        	exit;
    	}


    	$result = $this->Model_service_category->getData1($id);
		foreach ($result as $row) {
			$result1 = $this->Model_service_category->show_service_by_id($row['id']);
			foreach ($result1 as $row1) {
				$photo = $row1['photo'];
			}
			if($photo!='') {
				unlink('./public/uploads/'.$photo);
			}
			$result1 = $this->Model_service_category->show_service_photo_by_service_id($row['id']);
			foreach ($result1 as $row1) {
				$photo = $row1['photo'];
				unlink('./public/uploads/service_photos/'.$photo);
			}

			$this->Model_service_category->delete1($row['id']);
			$this->Model_service_category->delete2($row['id']);
		}
        $this->Model_service_category->delete($id);
        
        $success = 'service category is deleted successfully';
        $this->session->set_flashdata('success',$success);
        redirect(base_url().'admin/service_category');
    }

}