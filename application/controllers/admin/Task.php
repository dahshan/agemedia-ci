<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task extends CI_Controller 
{
	function __construct() 
	{
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_task');
    }

	public function index()
	{
		$data['setting'] = $this->Model_common->get_setting_data();
		$data['have_messages'] = $this->Model_common->get_user_messages($this->session->userdata('id'));


		if ($this->session->userdata('role') == 'Admin') {
			$data['feature'] = $this->Model_task->show('Admin');
		} else {
			$data['feature'] = $this->Model_task->show($this->session->userdata('id'));
		}

		$this->load->view('admin/view_header',$data);
		$this->load->view('admin/view_task',$data);
		$this->load->view('admin/view_footer');
	}

	public function archive()
	{
		$data['setting'] = $this->Model_common->get_setting_data();
		$data['have_messages'] = $this->Model_common->get_user_messages($this->session->userdata('id'));

		if ($this->session->userdata('role') == 'Admin') {
			$data['feature'] = $this->Model_task->archive('Admin');
		} else {
			$data['feature'] = $this->Model_task->archive($this->session->userdata('id'));
		}
		

		$this->load->view('admin/view_header',$data);
		$this->load->view('admin/view_task_archive',$data);
		$this->load->view('admin/view_footer');
	}


	public function view($id)
	{
		if ($this->session->userdata('role') == 'Admin') {
			$tot = $this->Model_task->show_one($id, 'Admin');
		} else {
			$tot = $this->Model_task->show_one($id, $this->session->userdata('id'));
		}

    	// echo "<pre>";
    	// print_r($tot);
    	// exit;
    	if(!$tot) {
    		redirect(base_url().'admin/task');
        	exit;
    	}
       	
       	$data['setting'] = $this->Model_common->get_setting_data();
       	$data['all_lang'] = $this->Model_common->all_lang();
		$error = '';
		$success = '';


		if(isset($_POST['finish'])) 
		{
			// echo "<pre>";
			// print_r($_POST);
			// exit;
			$valid = 1;

			$this->form_validation->set_rules('comment', 'التعليق مطلوب', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }
	    
		    if($valid == 1) 
		    {

	    		$form_data = array(
					'status'    => 'منتهي',
					'comment'    => $_POST['comment']
	            );
	            $this->Model_task->update($id,$form_data);
				
				$success = 'تم إنهاء المهمة بنجاح!';
				$this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/task');
		    }
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/task');
		    }
           
		} else {
			if ($this->session->userdata('role') == 'Admin') {
				$data['task'] = $this->Model_task->show_one($id, 'Admin');
			} else {
				$data['task'] = $this->Model_task->show_one($id, $this->session->userdata('id'));
			}

			$data['task']['files'] = explode(',', $data['task']['files']);
	       	$this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_task_view',$data);
			$this->load->view('admin/view_footer');
		}

	}


	public function edit($id)
	{
    	$tot = $this->Model_task->feature_check($id);
    	if(!$tot) {
    		redirect(base_url().'admin/task');
        	exit;
    	}
       	
       	$data['setting'] = $this->Model_common->get_setting_data();
       	$data['all_lang'] = $this->Model_common->all_lang();
		$error = '';
		$success = '';


		if(isset($_POST['form1'])) 
		{

			$valid = 1;

			$this->form_validation->set_rules('name', 'Name', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }
	    
		    if($valid == 1) 
		    {
		    	$data['feature'] = $this->Model_task->getData($id);

	    		$form_data = array(
					'name'    => $_POST['name']
	            );
	            $this->Model_task->update($id,$form_data);
				
				$success = 'Feature is updated successfully';
				$this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/task');
		    }
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/task/edit/'.$id);
		    }
           
		} else {
			$data['feature'] = $this->Model_task->getData($id);
	       	$this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_task_edit',$data);
			$this->load->view('admin/view_footer');
		}

	}


	public function delete($id) 
	{
    	$tot = $this->Model_task->feature_check($id);
    	if(!$tot) {
    		redirect(base_url().'admin/task');
        	exit;
    	}

        $this->Model_task->delete($id);
        $success = 'task is deleted successfully';
        $this->session->set_flashdata('success',$success);
        redirect(base_url().'admin/task');
    }

	public function message_seen($id) 
	{
    	$tot = $this->Model_task->message_check(array('id' => $id, 'message_to' => $this->session->userdata('id')));
    	if(!$tot) {
    		redirect(base_url().'admin/task');
        	exit;
    	}

       	$data['setting'] = $this->Model_common->get_setting_data();
       	$data['all_lang'] = $this->Model_common->all_lang();
		$error = '';
		$success = '';


		if(isset($_POST['reply_form'])) 
		{

			$valid = 1;

			$this->form_validation->set_rules('reply', 'رد الموظف', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }
	    
		    if($valid == 1) 
		    {

	    		$form_data = array(
					'message_id'    => $tot['id'], 
					'reply_from'    => $tot['message_to'], 
					'reply_to'    => $tot['message_from'], 
					'reply'    => $_POST['reply'], 
	            );
	            $this->Model_task->send_reply($form_data);
				
				$success = 'تم الرد على رسالة الإدارة بنجاح!';
				$this->session->set_flashdata('success',$success);
		        $this->Model_task->message_seen($id);
				redirect(base_url().'admin/task');
		    }
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/task');
		    }
		}
    }

}