<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_order extends CI_Controller 
{
	function __construct() 
	{
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_service_order');
    }

	public function index()
	{
		$data['setting'] = $this->Model_common->get_setting_data();

		$data['service_orders'] = $this->Model_service_order->show();
		foreach ($data['service_orders'] as $key => $value) {
			if ($data['service_orders'][$key]['order_files'] !== '') {
				$data['service_orders'][$key]['order_files'] = explode(',', $value['order_files']);
			}
			$data['service_orders'][$key]['required_inputs'] = json_decode($value['required_inputs']);
			// echo "<pre>";
			// print_r(json_decode($value['required_inputs']));
			// exit;
		}

		$this->load->view('admin/view_header',$data);
		$this->load->view('admin/view_service_order',$data);
		$this->load->view('admin/view_footer');
	}

	public function add()
	{
		$data['setting'] = $this->Model_common->get_setting_data();
		$data['all_lang'] = $this->Model_common->all_lang();

		$error = '';
		$success = '';

		if(isset($_POST['form1'])) {

			$valid = 1;

			$this->form_validation->set_rules('category_name', 'Category Name', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error = validation_errors();
            }

		    if($valid == 1) 
		    {
				
		        $form_data = array(
					'category_name'=> $_POST['category_name'],
					'status'       => $_POST['status'],
					'lang_id'      => $_POST['lang_id']
	            );
	            $this->Model_service_order->add($form_data);

		        $success = 'service category is added successfully!';
		        $this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/service_category');
		    }
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/service_category/add');
		    }
            
        } else {
            
            $this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_service_category_add',$data);
			$this->load->view('admin/view_footer');
        }
		
	}


	public function edit($id)
	{
    	$tot = $this->Model_service_order->service_order_check($id);
    	if(!$tot) {
    		redirect(base_url().'admin/service_order');
        	exit;
    	}
       	
       	$data['setting'] = $this->Model_common->get_setting_data();
       	$data['all_lang'] = $this->Model_common->all_lang();

		$error = '';
		$success = '';


		if(isset($_POST['form1'])) 
		{

			$valid = 1;

		    if($valid == 1) 
		    {
	    		$form_data = array(
					'team_member'=> $_POST['team_member'],
					'order_status'      => $_POST['order_status']
	            );
	            $this->Model_service_order->update($id,$form_data);
				
				$success = 'تم تحديث حالة الطلب بنجاح';
				$this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/service_order');
		    }
           
		} else {
			$data['service_order'] = $this->Model_service_order->getData($id);
			$data['team'] = $this->Model_service_order->getTeam();
			$this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_service_order_edit',$data);
			$this->load->view('admin/view_footer');
		}

	}


	public function delete($id) 
	{
    	$tot = $this->Model_service_order->service_category_check($id);
    	if(!$tot) {
    		redirect(base_url().'admin/service_category');
        	exit;
    	}


    	$result = $this->Model_service_order->getData1($id);
		foreach ($result as $row) {
			$result1 = $this->Model_service_order->show_service_by_id($row['id']);
			foreach ($result1 as $row1) {
				$photo = $row1['photo'];
			}
			if($photo!='') {
				unlink('./public/uploads/'.$photo);
			}
			$result1 = $this->Model_service_order->show_service_photo_by_service_id($row['id']);
			foreach ($result1 as $row1) {
				$photo = $row1['photo'];
				unlink('./public/uploads/service_photos/'.$photo);
			}

			$this->Model_service_order->delete1($row['id']);
			$this->Model_service_order->delete2($row['id']);
		}
        $this->Model_service_order->delete($id);
        
        $success = 'service category is deleted successfully';
        $this->session->set_flashdata('success',$success);
        redirect(base_url().'admin/service_category');
    }

}