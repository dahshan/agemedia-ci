<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plan extends CI_Controller 
{
	function __construct() 
	{
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_plan');
    }


	public function index()
	{
		$data['setting'] = $this->Model_common->get_setting_data();

		$data['feature'] = $this->Model_plan->show();

		$this->load->view('admin/view_header',$data);
		$this->load->view('admin/view_plan',$data);
		$this->load->view('admin/view_footer');
	}

	public function add()
	{
		$data['setting'] = $this->Model_common->get_setting_data();
		$data['all_lang'] = $this->Model_common->all_lang();

		$error = '';
		$success = '';

		if(isset($_POST['form1'])) {

			$valid = 1;

			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('price', 'Price', 'trim|required');
			$this->form_validation->set_rules('duration', 'Duration', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }
		    
		    if($valid == 1) 
		    {
		        $form_data = array(
					'name'    => $_POST['name'], 
					'price'    => $_POST['price'], 
					'duration'    => $_POST['duration']
	            );
	            $this->Model_plan->add($form_data);

		        $success = 'Feature is added successfully!';
		        $this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/plan');
		    } 
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/plan/add');
		    }
            
        } else {
            
            $this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_plan_add',$data);
			$this->load->view('admin/view_footer');
        }
		
	}


	public function edit($id)
	{
    	$tot = $this->Model_plan->feature_check($id);
    	if(!$tot) {
    		redirect(base_url().'admin/plan');
        	exit;
    	}
       	
       	$data['setting'] = $this->Model_common->get_setting_data();
       	$data['all_lang'] = $this->Model_common->all_lang();
		$error = '';
		$success = '';


		if(isset($_POST['form1'])) 
		{

			$valid = 1;

			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			$this->form_validation->set_rules('price', 'Price', 'trim|required');
			$this->form_validation->set_rules('duration', 'Duration', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }
	    
		    if($valid == 1) 
		    {
		    	$data['feature'] = $this->Model_plan->getData($id);

	    		$form_data = array(
					'name'    => $_POST['name'], 
					'price'    => $_POST['price'], 
					'duration'    => $_POST['duration']
	            );
	            $this->Model_plan->update($id,$form_data);
				
				$success = 'Feature is updated successfully';
				$this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/plan');
		    }
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/plan/edit/'.$id);
		    }
           
		} else {
			$data['feature'] = $this->Model_plan->getData($id);
	       	$this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_plan_edit',$data);
			$this->load->view('admin/view_footer');
		}

	}


	public function delete($id) 
	{
    	$tot = $this->Model_plan->feature_check($id);
    	if(!$tot) {
    		redirect(base_url().'admin/plan');
        	exit;
    	}

        $this->Model_plan->delete($id);
        $success = 'plan is deleted successfully';
        $this->session->set_flashdata('success',$success);
        redirect(base_url().'admin/plan');
    }

}