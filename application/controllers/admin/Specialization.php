<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Specialization extends CI_Controller 
{
	function __construct() 
	{
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_specialization');
    }

	public function index()
	{
		$data['setting'] = $this->Model_common->get_setting_data();

		$data['feature'] = $this->Model_specialization->show();

		$this->load->view('admin/view_header',$data);
		$this->load->view('admin/view_specialization',$data);
		$this->load->view('admin/view_footer');
	}

	public function add()
	{
		$data['setting'] = $this->Model_common->get_setting_data();
		$data['all_lang'] = $this->Model_common->all_lang();

		$error = '';
		$success = '';

		if(isset($_POST['form1'])) {

			$valid = 1;

			$this->form_validation->set_rules('name', 'Name', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }
		    
		    if($valid == 1) 
		    {
		        $form_data = array(
					'name'    => $_POST['name']
	            );
	            $this->Model_specialization->add($form_data);

		        $success = 'Feature is added successfully!';
		        $this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/specialization');
		    } 
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/specialization/add');
		    }
            
        } else {
            
            $this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_specialization_add',$data);
			$this->load->view('admin/view_footer');
        }
		
	}


	public function edit($id)
	{
    	$tot = $this->Model_specialization->feature_check($id);
    	if(!$tot) {
    		redirect(base_url().'admin/specialization');
        	exit;
    	}
       	
       	$data['setting'] = $this->Model_common->get_setting_data();
       	$data['all_lang'] = $this->Model_common->all_lang();
		$error = '';
		$success = '';


		if(isset($_POST['form1'])) 
		{

			$valid = 1;

			$this->form_validation->set_rules('name', 'Name', 'trim|required');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }
	    
		    if($valid == 1) 
		    {
		    	$data['feature'] = $this->Model_specialization->getData($id);

	    		$form_data = array(
					'name'    => $_POST['name']
	            );
	            $this->Model_specialization->update($id,$form_data);
				
				$success = 'Feature is updated successfully';
				$this->session->set_flashdata('success',$success);
				redirect(base_url().'admin/specialization');
		    }
		    else
		    {
		    	$this->session->set_flashdata('error',$error);
				redirect(base_url().'admin/specialization/edit/'.$id);
		    }
           
		} else {
			$data['feature'] = $this->Model_specialization->getData($id);
	       	$this->load->view('admin/view_header',$data);
			$this->load->view('admin/view_specialization_edit',$data);
			$this->load->view('admin/view_footer');
		}

	}


	public function delete($id) 
	{
    	$tot = $this->Model_specialization->feature_check($id);
    	if(!$tot) {
    		redirect(base_url().'admin/specialization');
        	exit;
    	}

        $this->Model_specialization->delete($id);
        $success = 'Specialization is deleted successfully';
        $this->session->set_flashdata('success',$success);
        redirect(base_url().'admin/specialization');
    }

}