      <div class="content">
         <section class="inner-head" style="background-image: url(public/site/img/home-cover0.jpeg);">
            <div class="container">
               <h1 class="title"> أعمالنا </h1>
            </div>
         </section>
         <section class="works pt-40 pb-40">
         <div class="mb-40">
            <h2 class="section-title"> أعمالنا </h2>
            <p class="section-subtitle"> يسعدنا مشاركتكم أعمالنا دائماً</p>
         </div>
         <div class="container mixer">
            <div class="mix-tabs advanced">
               <div class="owl-carousel works-slide down-nav-arr">
                  <div class="item"> <button class="mix-btn" data-filter=".cat-1"> <img src="public/storage/images/workcategories/1613483218_2423.png" alt=""> تصميم تطبيقات الجوال </button> </div>
                  <div class="item"> <button class="mix-btn" data-filter=".cat-2"> <img src="public/storage/images/workcategories/1613483359_4555.png" alt=""> تصميم تطبيقات توصيل </button> </div>
                  <div class="item"> <button class="mix-btn" data-filter=".cat-3"> <img src="public/storage/images/workcategories/1613483451_3940.png" alt=""> تصميم تطبيقات ذبائح </button> </div>
                  <div class="item"> <button class="mix-btn" data-filter=".cat-5"> <img src="public/storage/images/workcategories/1613483564_7251.png" alt=""> تصميم تطبيقات أسر منتجة </button> </div>
                  <div class="item"> <button class="mix-btn" data-filter=".cat-6"> <img src="public/storage/images/workcategories/1613483681_3254.png" alt=""> تصميم مواقع </button> </div>
                  <div class="item"> <button class="mix-btn" data-filter=".cat-7"> <img src="public/storage/images/workcategories/1613483852_2110.png" alt=""> تصميم الجرافيك </button> </div>
                  <div class="item"> <button class="mix-btn" data-filter=".cat-8"> <img src="public/storage/images/workcategories/1613483985_3020.png" alt=""> تصميم موقع مثل حراج </button> </div>
                  <div class="item"> <button class="mix-btn" data-filter=".cat-9"> <img src="public/storage/images/workcategories/1613484128_2655.png" alt=""> تصميم متجر إلكتروني </button> </div>
                  <div class="item"> <button class="mix-btn" data-filter=".cat-10"> <img src="public/storage/images/workcategories/1613484297_5333.png" alt=""> تصميم موشن جرافيك </button> </div>
                  <div class="item"> <button class="mix-btn" data-filter=".cat-11"> <img src="public/storage/images/workcategories/1613484422_8026.png" alt=""> تصميم تطبيقات مشاغل </button> </div>
                  <div class="item"> <button class="mix-btn" data-filter=".cat-13"> <img src="public/storage/images/workcategories/1613484828_2493.png" alt=""> تصميم اللوجو </button> </div>
                  <div class="item"> <button class="mix-btn" data-filter=".cat-14"> <img src="public/storage/images/workcategories/1613486395_1435.png" alt=""> تصميم تطبيق مثل حراج </button> </div>
                  <div class="item"> <button class="mix-btn" data-filter=".cat-15"> <img src="public/storage/images/workcategories/1617802015_2242.png" alt=""> التسويق الالكتروني </button> </div>
               </div>
            </div>
            <div class="justify-content-center advanced-works">
               <div class="row advanced-work">
                  <div class="col-12 col-md-6 mix cat-14">
                     <div class="img"> <img src="public/storage/images/works/delta1.jpg" alt="دلتا لتنظيم الرحلات"> </div>
                  </div>
                  <div class="col-12 col-md-6 mix cat-14">
                     <div class="info">
                        <div class="logo-img"> <img src="public/storage/images/works/delta.png" alt=""> </div>
                        <p class="title">دلتا لتنظيم الرحلات</p>
                        <p class="info-content">
                        <p>
                           تصميم الهوية البصرية والدليل الشامل وبناء استراتيجية العلامة التجارية والموقع الالكتروني لمؤسسة دلتا لتنظيم الرحلات
                           لأكثر من 40 عامًا ،مؤسسو دلتا ادفنتشرز الروّاد في تنظيم رحلات الصحراء والمغامرات في المملكة العربية السعودية.
                           https://deltadventures.com/
                        </p>
                        <a href="public/storage/images/works/Delta Brand Guidelines.pdf" class="button blue">مشاهده التفاصيل</a> 
                     </div>
                  </div>
               </div>

               <div class="row advanced-work">
                  <div class="col-12 col-md-6 mix cat-14">
                     <div class="img"> <img src="public/storage/images/works/nb3.jpg" alt="نبع الرؤية للمقاولات"> </div>
                  </div>
                  <div class="col-12 col-md-6 mix cat-14">
                     <div class="info">
                        <div class="logo-img"> <img src="public/storage/images/works/nb3icon.png" alt=""> </div>
                        <p class="title">نبع الرؤية للمقاولات</p>
                        <p class="info-content">
                        <p>
                           تطوير الشعار وتصميم الهوية البصرية الخاصة بمؤسسة نبع الرؤية للمقاولات
                        </p>
                        <a href="public/storage/images/works/Naba AlRoaya Identity2021.pdf" class="button blue">مشاهده التفاصيل</a> 
                     </div>
                  </div>
               </div>

               <div class="row advanced-work">
                  <div class="col-12 col-md-6 mix cat-14">
                     <div class="img"> <img src="public/storage/images/works/cafe.jpg" alt="Jolr Coffe"> </div>
                  </div>
                  <div class="col-12 col-md-6 mix cat-14">
                     <div class="info">
                        <div class="logo-img"> <img src="public/storage/images/works/jolr.png" alt=""> </div>
                        <p class="title">Jolr Coffe</p>
                        <p class="info-content">
                        <p>
                           تصميم براند 
                           Jolr Coffe Roasters
                        </p>
                        <a href="public/storage/images/works/Jolr Brand2020.pdf" class="button blue">مشاهده التفاصيل</a> 
                     </div>
                  </div>
               </div>

               <div class="row advanced-work">
                  <div class="col-12 col-md-6 mix cat-14">
                     <div class="img"> <img src="public/storage/images/works/Mozn.jpg" alt="مزن للمقاولات العامة وحفر الآبار"> </div>
                  </div>
                  <div class="col-12 col-md-6 mix cat-14">
                     <div class="info">
                        <div class="logo-img"> <img src="public/storage/images/works/Mozn.png" alt=""> </div>
                        <p class="title">مزن للمقاولات العامة وحفر الآبار</p>
                        <p class="info-content">
                        <p>
                           تصميم الشعار والهوية البصرية الخاصة بمؤسسة مزن للمقاولات العامة وحفر الآبار
                        </p>
                        <a href="public/storage/images/works/Mozn Identity_2020c.pdf" class="button blue">مشاهده التفاصيل</a> 
                     </div>
                  </div>
               </div>

               <div class="row advanced-work">
                  <div class="col-12 col-md-6 mix cat-14">
                     <div class="img"> <img src="public/storage/images/works/kbsa.jpg" alt="مطعم كبسة بكاس"> </div>
                  </div>
                  <div class="col-12 col-md-6 mix cat-14">
                     <div class="info">
                        <div class="logo-img"> <img src="public/storage/images/works/Kabsa-Bikas-icon.png" alt=""> </div>
                        <p class="title">مطعم كبسة بكاس</p>
                        <p class="info-content">
                        <p>
                           تصميم الشعار والهوية البصرية لمطعم كبسة بكاس
                        </p>
                        <a href="public/storage/images/works/Kabsa bi Kas Identity_2020.pdf" class="button blue">مشاهده التفاصيل</a> 
                     </div>
                  </div>
               </div>

               <div class="row advanced-work">
                  <div class="col-12 col-md-6 mix cat-14">
                     <div class="img"> <img src="public/storage/images/works/pixel.jpg" alt=""> </div>
                  </div>
                  <div class="col-12 col-md-6 mix cat-14">
                     <div class="info">
                        <div class="logo-img"> <img src="public/storage/images/works/Kabsa-Bikas-icon.png" alt="بكسل التجارية للتكييف والاجهزة المنزلية "> </div>
                        <p class="title">بكسل التجارية للتكييف والاجهزة المنزلية </p>
                        <p class="info-content">
                        <p>
                           تصميم العلامة التجارية والهوية البصرية لشركة بكسل التجارية للتكييف والاجهزة المنزلية 
                        </p>
                        <a href="public/storage/images/works/Pixel Identity_2020.pdf" class="button blue">مشاهده التفاصيل</a> 
                     </div>
                  </div>
               </div>
         </div>
         <section class="contact-us pt-40 pb-40 blue-bg"> <div class="container"> <div class="mb-40"> <h2 class="section-title"> اتصل بنا </h2> <p class="section-subtitle"> نتشرف بخدمتكم وتواصلكم معنا </p> </div> <div class="row"> <div class="col-12 col-md-4"> <div class="contact-box green"> <i class="fab icon-fa-whatsapp"></i> <p class="title dark-color bold font-15 mb-10"> للتواصل عبر الواتساب </p> <p class="info-content"> 0550016001 </p> <a href="https://api.whatsapp.com/send?phone=966550016001" class="box-btn"> للتواصل عبر الواتساب </a> </div> </div> <div class="col-12 col-md-4"> <div class="contact-box blue"> <i class="fas icon-fa-phone-1"></i> <p class="title dark-color bold font-15 mb-10"> للإتصال والإستفسار </p> <p class="info-content"> 0550016001 </p> <a href="tel:0550016001" class="box-btn"> اتصل بنا </a> </div> </div> <div class="col-12 col-md-4"> <div class="contact-box dark"> <i class="far icon-fa-envelope"></i> <p class="title dark-color bold font-15 mb-10"> للمراسلة المباشرة </p> <p class="info-content"> <a href="cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="6c1f0d00091f2c0d0d0518421f0d">[email&#160;protected]</a> </p> <a href="cdn-cgi/l/email-protection.html#3241535e57417253535b461c4153" class="box-btn"> للمراسلة المباشرة </a> </div> </div> </div> <div class="row justify-content-center"> <div class="col-12 col-md-8"> <form action="https://agemedia.sa/contact_us" class="contact-form mt-20" id="contact_us_form"> <input type="hidden" name="_token" value="xGpbyi1bg7S0dKzIOK8kd80hN9HbnukZYxRI4uk1"> <input type="text" name="name" class="input" placeholder="الاسم : "> <input type="number" name="phone" class="input" placeholder="رقم الهاتف "> <input type="email" name="email" class="input" placeholder="البريد الالكتروني "> <textarea class="input" name="message" placeholder="الرسالة"></textarea> <div class="text-end"> <button class="button blue">ارسال</button> </div> </form> </div> </div> </div> </section>  
      </div>