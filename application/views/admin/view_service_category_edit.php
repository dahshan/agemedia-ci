<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || $this->session->userdata('role') !== 'Admin') {
	redirect(base_url().'admin/login');
}
?>

<section class="content-header">
	<div class="content-header-left">
		<h1>تعديل تصنيف</h1>
	</div>
	<div class="content-header-right">
		<a href="<?php echo base_url(); ?>admin/service_category" class="btn btn-primary btn-sm">عرض الكل</a>
	</div>
</section>

<section class="content">

  	<div class="row">
	    <div class="col-md-12">

			<?php
	        if($this->session->flashdata('error')) {
	            ?>
	            <div class="callout callout-danger">
	                <p><?php echo $this->session->flashdata('error'); ?></p>
	            </div>
	            <?php
	        }
	        if($this->session->flashdata('success')) {
	            ?>
	            <div class="callout callout-success">
	                <p><?php echo $this->session->flashdata('success'); ?></p>
	            </div>
	            <?php
	        }
	        ?>

	       <?php echo form_open(base_url().'admin/service_category/edit/'.$service_category['category_id'],array('class' => 'form-horizontal')); ?>

	        <div class="box box-info">

	            <div class="box-body">
	                <div class="form-group">
	                    <label for="" class="col-sm-2 control-label">إسم التصنيف *</label>
	                    <div class="col-sm-4">
	                        <input type="text" class="form-control" name="category_name" value="<?php echo $service_category['category_name']; ?>">
	                    </div>
	                </div>
	                <div class="form-group">
			            <label for="" class="col-sm-2 control-label">الحالة *</label>
			            <div class="col-sm-4">
			            	<select name="status" class="form-control select2">
			            		<option value="Active" <?php if($service_category['status']=='Active') {echo 'selected';} ?>>نشط</option>
			            		<option value="Inactive" <?php if($service_category['status']=='Inactive') {echo 'selected';} ?>>غير نشط</option>
			            	</select>
			            </div>
			        </div>
			        <div class="form-group">
						<label for="" class="col-sm-2 control-label">اللغة </label>
						<div class="col-sm-2">
							<select name="lang_id" class="form-control select2">
								<?php
								foreach($all_lang as $row)
								{
									?><option value="<?php echo $row['lang_id']; ?>" <?php if($service_category['lang_id'] == $row['lang_id']) {echo 'selected';} ?>><?php echo $row['lang_name']; ?></option><?php
								}
								?>
							</select>
						</div>
					</div>
	                <div class="form-group">
	                	<label for="" class="col-sm-2 control-label"></label>
	                    <div class="col-sm-6">
	                      <button type="submit" class="btn btn-success pull-left" name="form1">تحديث</button>
	                    </div>
	                </div>

	            </div>

	        </div>

	        <?php echo form_close(); ?>

	    </div>
  	</div>

</section>