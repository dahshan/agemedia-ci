<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || (($this->session->userdata('role') !== 'Editor') && ($this->session->userdata('role') !== 'Admin'))) {
    redirect(base_url().'admin/login');
}
?>
<section class="content-header">
	<div class="content-header-left">
		<h1>إرسال رسالة</h1>
	</div>
	<div class="content-header-right">
		<a href="<?php echo base_url(); ?>admin/client/index/Editor" class="btn btn-primary btn-sm">عرض الكل</a>
	</div>
</section>

<section class="content">

	<div class="row">
		<div class="col-md-12">


			<?php
			if($this->session->flashdata('error')) {
				?>
				<div class="callout callout-danger">
					<p><?php echo $this->session->flashdata('error'); ?></p>
				</div>
				<?php
			}
			if($this->session->flashdata('success')) {
				?>
				<div class="callout callout-success">
					<p><?php echo $this->session->flashdata('success'); ?></p>
				</div>
				<?php
			}
			?>

			<?php echo form_open_multipart("",array('class' => 'form-horizontal'));?>
				<div class="box box-info">
					<div class="box-body">						
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">محتوى الرسالة *</label>
							<div class="col-sm-8">
								<textarea required="required" rows="8" style="width: 100%;padding: 10px 5px;" name="message"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label"></label>
							<div class="col-sm-6">
								<button type="submit" class="btn btn-success pull-right" name="send_message">إرسال</button>
							</div>
						</div>
					</div>
				</div>
			<?php echo form_close(); ?>



		</div>
	</div>

</section>


