<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || $this->session->userdata('role') !== 'Admin') {
    redirect(base_url().'admin');
}
?>

<section class="content-header">
    <div class="content-header-left">
        <h1>ضبط إعدادات الفوتر</h1>
    </div>
    <div class="content-header-right">
        <a href="<?php echo base_url(); ?>admin/footer-setting" class="btn btn-primary btn-sm">عرض الكل</a>
    </div>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-12">

            <?php
            if($this->session->flashdata('error')) {
                ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
                <?php
            }
            if($this->session->flashdata('success')) {
                ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
                <?php
            }
            ?>

            <?php echo form_open_multipart(base_url().'admin/footer-setting/edit/'.$footer_setting['id'], array('class' => 'form-horizontal'));?>
                <div class="box box-info" style="">
                    <div class="box-body" style="">

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Copyright نص </label>
                            <div class="col-sm-9">
                                <input type="text" autocomplete="off" class="form-control" name="footer_copyright" value="<?php echo $footer_setting['footer_copyright']; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"> رابط خدمة معروف </label>
                            <div class="col-sm-9">
                                <input type="text" autocomplete="off" class="form-control" name="maroof_url" value="<?php echo $footer_setting['maroof_url']; ?>">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form1">تحديث</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div>

</section>