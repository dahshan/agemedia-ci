<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || ($this->session->userdata('role') !== 'Admin' && $this->session->userdata('role') !== 'Editor')) {
	redirect(base_url().'admin/login');
}
?>

<section class="content-header">
	<div class="content-header-left">
		<h1>تعديل طلب خدمة</h1>
	</div>
	<div class="content-header-right">
		<a href="<?php echo base_url(); ?>admin/service_category" class="btn btn-primary btn-sm">عرض الكل</a>
	</div>
</section>

<section class="content">

  	<div class="row">
	    <div class="col-md-12">

			<?php
	        if($this->session->flashdata('error')) {
	            ?>
	            <div class="callout callout-danger">
	                <p><?php echo $this->session->flashdata('error'); ?></p>
	            </div>
	            <?php
	        }
	        if($this->session->flashdata('success')) {
	            ?>
	            <div class="callout callout-success">
	                <p><?php echo $this->session->flashdata('success'); ?></p>
	            </div>
	            <?php
	        }
	        ?>

	       <?php echo form_open(base_url().'admin/service_order/edit/'.$service_order['order_id'],array('class' => 'form-horizontal')); ?>

	        <div class="box box-info">

	            <div class="box-body">
			        <div class="form-group">
						<label for="" class="col-sm-2 control-label">الموظف </label>
						<div class="col-sm-2">
							<select name="team_member" class="form-control select2">
								<option value="">
								</option>
								<?php
								foreach($team as $row)
								{
									?><option value="<?php echo $row['id']; ?>" <?php if($service_order['team_member'] == $row['id']) {echo 'selected';} ?>><?php echo $row['name']; ?></option><?php
								}
								?>
							</select>
						</div>
					</div>
			        <div class="form-group">
						<label for="" class="col-sm-2 control-label">الحالة </label>
						<div class="col-sm-2">
							<select name="order_status" class="form-control select2">
								<option value="new" <?php if($service_order['order_status'] == 'new') {echo 'selected';} ?>>
									جديد
								</option>
								<option value="process" <?php if($service_order['order_status'] == 'process') {echo 'selected';} ?>>
									تحت المعالجة
								</option>
								<option value="done" <?php if($service_order['order_status'] == 'done') {echo 'selected';} ?>>
									تم
								</option>
								<option value="cancelled" <?php if($service_order['order_status'] == 'cancelled') {echo 'selected';} ?>>
									ملغي
								</option>
							</select>
						</div>
					</div>
	                <div class="form-group">
	                	<label for="" class="col-sm-2 control-label"></label>
	                    <div class="col-sm-6">
	                      <button type="submit" class="btn btn-success pull-left" name="form1">تحديث</button>
	                    </div>
	                </div>

	            </div>

	        </div>

	        <?php echo form_close(); ?>

	    </div>
  	</div>

</section>