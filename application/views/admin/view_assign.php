<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || $this->session->userdata('role') !== 'Admin') {
	redirect(base_url().'admin');
}
?>
<section class="content-header">
	<div class="content-header-left">
		<h1>توزيع المهام اليومي على الموظفين</h1>
	</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">

			<?php
	        if($this->session->flashdata('error')) {
	            ?>
				<div class="callout callout-danger">
					<p><?php echo $this->session->flashdata('error'); ?></p>
				</div>
	            <?php
	        }
	        if($this->session->flashdata('success')) {
	            ?>
				<div class="callout callout-success">
					<p><?php echo $this->session->flashdata('success'); ?></p>
				</div>
	            <?php
	        }
	        ?>

	        <?php $fattr = array('class' => 'form-signin');
	             echo form_open(base_url().'admin/assign/assign_to', $fattr); ?>

			<div class="box box-info">
				<div class="box-body table-responsive">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>رقم الإشتراك</th>
								<th>إسم العميل</th>
								<th>التخصص</th>
								<th>الخطة</th>
								<th>مدة الخطة</th>
								<th>بداية الإشتراك</th>
								<th>نهاية الإشتراك</th>
								<th>تحديد</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=0;							
							foreach ($task as $row) {
								$i++;
								?>
								<tr>
									<td><?php echo $row['id']; ?></td>
									<td><?php echo $row['user_name']; ?></td>
									<td><?php echo $row['specialization']; ?></td>
									<td><?php echo $row['plan_name']; ?></td>
									<td><?php echo $row['plan_duration']; ?> يوم</td>
									<td><?php echo $row['date_time']; ?></td>
									<td><?php echo date('Y-m-d H:i:s',strtotime('+'.$row['plan_duration'].' days',strtotime($row['date_time']))) ?></td>
									<td>
										<?php 
										echo 
										form_checkbox(array(
										    'name'=>'sub[]', 
										    'value'=>$row['id']));
										    ?>
									</td>
								</tr>
								<?php
							}
							?>							
						</tbody>
					</table>
					<div class="box padding" style="text-align: right;">
						<div class="box-body">
							<div class="form-group">
								<label class="col-sm-1 control-label" for="inputGroupSelect02">إسناد للموظف </label>

							  <div class="col-sm-2">
							  	<select class="form-control select2" id="inputGroupSelect02" name="employee">
							  	  <?php
							  	  foreach ($employee as $row) {
							  	      ?>
							  	      <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
							  	      <?php
							  	  }
							  	  ?>
							  	</select>
							  </div>
							    <div class="col-sm-2">
							    	<?php echo form_submit(array('value'=>'تنفيذ', 'class'=>'btn btn-lg btn-primary btn-sm','name'=>'assign')); ?>
							    </div>
							</div>


						</div>
					</div>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>