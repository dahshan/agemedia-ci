<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || ($this->session->userdata('role') !== 'Admin' && $this->session->userdata('role') !== 'Editor')) {
	redirect(base_url().'admin');
}
?>
<section class="content-header">
	<div class="content-header-left">
		<h1>عرض رسائل الإدارة</h1>
	</div>
	<div class="content-header-right">
		<a href="<?php echo base_url(); ?>admin/client/index/Editor" class="btn btn-primary btn-sm">إرسال رسالة</a>
	</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">

			<?php
	        if($this->session->flashdata('error')) {
	            ?>
				<div class="callout callout-danger">
					<p><?php echo $this->session->flashdata('error'); ?></p>
				</div>
	            <?php
	        }
	        if($this->session->flashdata('success')) {
	            ?>
				<div class="callout callout-success">
					<p><?php echo $this->session->flashdata('success'); ?></p>
				</div>
	            <?php
	        }
	        if($have_messages) {
	        	foreach ($have_messages as $key => $value) {
	            ?>
	            <?php echo form_open_multipart(base_url() . "admin/task/message_seen/" . $value['id'],array('class' => 'form-horizontal'));?>
	            <div class="alert alert-info" role="alert">
	              <h4 class="alert-heading">رسائل الإدارة <small>[<?php echo $value['date_time'] ?>]</small></h4>
	              <p style="font-size: large;"><?php echo $value['message'] ?></p>
	              <hr>


	              <div class="row">
	                <div class="col-lg-12">
	                  <div class="input-group">
	                    <input type="text" name="reply" required="required" class="form-control" placeholder="كتابة رد..">
	                    <span class="input-group-btn">
	                    	<button type="submit" name="reply_form" class="btn btn-primary" type="button">إرسال</button>
	                    </span>
	                  </div><!-- /input-group -->
	                </div><!-- /.col-lg-6 -->
	              </div><!-- /.row -->
	            </div>
	            <?php echo form_close(); ?>
	            <?php
	        	}
	        }
	        ?>
	        
			<div class="box box-info">
				<div class="box-body table-responsive">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>موجهة للموظف</th>
								<th>رسالة الإدارة</th>
								<th>رد الموظف</th>
								<th width="140">الحالة</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=0;							
							foreach ($feature as $row) {
								$i++;
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $row['name']; ?></td>
									<td>
										<?php echo $row['message']; ?>
										<br>
										<small>
											[
											<?php echo $row['message_date_time']; ?>
											]
										</small>
									</td>
									<td>
										<?php if ($row['reply']): ?>
											<?php echo $row['reply']; ?>
											<br>
											<small>
												[
												<?php echo $row['reply_date_time']; ?>
												]
											</small>
										<?php endif ?>
									</td>
									<td>
										<?php if ($row['seen'] == 0): ?>
											غير مقروءة
										<?php else: ?>
											تمت القراءة
										<?php endif ?>
									</td>
								</tr>
								<?php
							}
							?>							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>