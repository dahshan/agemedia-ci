<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || (($this->session->userdata('role') !== 'Editor') && ($this->session->userdata('role') !== 'Admin'))) {
    redirect(base_url().'admin/login');
}
?>
<section class="content-header">
	<div class="content-header-left">
		<h1>عرض المهام المنتهية</h1>
	</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">

			<?php
	        if($this->session->flashdata('error')) {
	            ?>
				<div class="callout callout-danger">
					<p><?php echo $this->session->flashdata('error'); ?></p>
				</div>
	            <?php
	        }
	        if($this->session->flashdata('success')) {
	            ?>
				<div class="callout callout-success">
					<p><?php echo $this->session->flashdata('success'); ?></p>
				</div>
	            <?php
	        }
	        if($have_messages) {
	        	foreach ($have_messages as $key => $value) {
	            ?>
	            <?php echo form_open_multipart(base_url() . "admin/task/message_seen/" . $value['id'],array('class' => 'form-horizontal'));?>
	            <div class="alert alert-info" role="alert">
	              <h4 class="alert-heading">رسائل الإدارة <small>[<?php echo $value['date_time'] ?>]</small></h4>
	              <p style="font-size: large;"><?php echo $value['message'] ?></p>
	              <hr>


	              <div class="row">
	                <div class="col-lg-12">
	                  <div class="input-group">
	                    <input type="text" name="reply" required="required" class="form-control" placeholder="كتابة رد..">
	                    <span class="input-group-btn">
	                    	<button type="submit" name="reply_form" class="btn btn-primary" type="button">إرسال</button>
	                    </span>
	                  </div><!-- /input-group -->
	                </div><!-- /.col-lg-6 -->
	              </div><!-- /.row -->
	            </div>
	            <?php echo form_close(); ?>
	            <?php
	        	}
	        }
	        ?>	        
			<div class="box box-info">
				<div class="box-body table-responsive">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>رقم الإشتراك</th>
								<th>العميل</th>
								<th>الموظف</th>
								<th>تاريخ المهمة</th>
								<th>الحالة</th>
								<th width="140">الإجراء</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=0;							
							foreach ($feature as $row) {
								$i++;
								?>
								<tr>
									<td><?php echo $row['sub_id']; ?></td>
									<td><?php echo $row['user_name']; ?></td>
									<td><?php echo $row['employee_name']; ?></td>
									<td><?php echo $row['date']; ?></td>
									<td>
										<span class="badge badge-secondary">
										<?php echo $row['status']; ?>
										</span>
									</td>
									<td>
										<a href="<?php echo base_url(); ?>admin/task/view/<?php echo $row['task_id']; ?>" class="btn btn-primary btn-xs">عرض</a>
									</td>
								</tr>
								<?php
							}
							?>							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>