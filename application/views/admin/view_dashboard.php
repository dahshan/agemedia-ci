<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || $this->session->userdata('role') !== 'Admin') {
	redirect(base_url().'admin');
}
?>
<section class="content-header">
  <h1>الرئيسية</h1>
</section>

<section class="content">

  <div class="row">

    
    
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-hand-o-right" style="transform: rotateY(180deg);"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">أعضاء الفريق</span>
          <span class="info-box-number"><?php echo $total_team_member; ?></span>
        </div>
      </div>
    </div>

   <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-hand-o-right" style="transform: rotateY(180deg);"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">العملاء</span>
          <span class="info-box-number"><?php echo $total_client; ?></span>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-hand-o-right" style="transform: rotateY(180deg);"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">عدد المهام لليوم</span>
          <span class="info-box-number"><?php echo $total_valid_sub; ?></span>
        </div>
      </div>
    </div>


    

  </div>


  <section class="content-header margin-bottom">
    <h1>رسائل المشكلات والمقترحات</h1>
  </section>

  <div class="row">
    <div class="col-lg-12">
      <div class="box box-info">
        <div class="box-body table-responsive">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>نوع الرسالة</th>
                <th>قادمة من</th>
                <th>محتوى الرسالة</th>
                <th>الوقت والتاريخ</th>
                <th width="140">الإجراء</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $i=0;
              foreach ($message as $row) {
                $i++;
                ?>
                <tr>
                  <td><?php echo $row['type']; ?></td>
                  <td><?php echo $row['user_name']; ?></td>
                  <td><?php echo $row['message']; ?></td>
                  <td><?php echo $row['date_time']; ?></td>
                  <td>
                    <a target="_blank" href="https://api.whatsapp.com/send?phone=<?php echo $row['user_phone'] ?>" class="btn bg-green"><i class="fa fa-whatsapp"></i>  الرد عبر واتساب</a> 
                  </td>
                </tr>
                <?php
              }
              ?>              
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
  </div>

  
  
  <section class="content-header margin-bottom">
    <h1>آخر المستخدمين نشاطاً</h1>
  </section>

  <div class="row">
    <div class="col-lg-12">
      <div class="box box-info">
        <div class="box-body table-responsive">
          <table id="example4" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>إسم المستخدم</th>
                <th>آخر نشاط</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $i=0;
              foreach ($activity as $row) {
                $i++;
                ?>
                <tr>
                  <td><?php echo $row['name']; ?></td>
                  <td><?php echo date('Y-m-d H:i:s', $row['last_activity']); ?></td>
                </tr>
                <?php
              }
              ?>              
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
  </div>


</section>