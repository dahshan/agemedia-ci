<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || (($this->session->userdata('role') !== 'Editor') && ($this->session->userdata('role') !== 'Admin'))) {
    redirect(base_url().'admin/login');
}
?>
<section class="content-header">
	<div class="content-header-left">
		<h1>تفاصيل المهمة</h1>
	</div>
	<div class="content-header-right">
		<a href="<?php echo base_url(); ?>admin/task" class="btn btn-primary btn-sm">عرض الكل</a>
	</div>
</section>

<section class="content">

	<div class="row">
		<div class="col-md-12">

			<table class="table table-striped" style="width: 90%; margin: 20px auto;">
 			  <tbody>
 			  	<tr>
 			  	    <td>إسم العميل</td>
 			  	    <td><?php echo $task['name']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>رقم هاتف العميل</td>
 			  	    <td><?php echo $task['phone']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>المؤهل الدراسي</td>
 			  	    <td><?php echo $task['qualification']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>التخصص</td>
 			  	    <td><?php echo $task['specialization']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>دولة المؤهل</td>
 			  	    <td><?php echo $task['country']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>مدينة المؤهل</td>
 			  	    <td><?php echo $task['city']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>الشركة او الجامعة</td>
 			  	    <td><?php echo $task['organization']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>تاريخ التخرج</td>
 			  	    <td><?php echo $task['graduation_date']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>التقدير او الدرجة</td>
 			  	    <td><?php echo $task['graduation_grade']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>الحد الأدنى للراتب</td>
 			  	    <td><?php echo $task['minimum_salary']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>مكان العمل المفضل</td>
 			  	    <td><?php echo $task['favorite_place']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>نمط العمل المفضل</td>
 			  	    <td><?php echo $task['favorite_type']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>حالة العمل الآن</td>
 			  	    <td><?php echo $task['work_status']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>الوظيفة الحالية</td>
 			  	    <td><?php echo $task['current_job']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>الراتب الحالي</td>
 			  	    <td><?php echo $task['current_salary']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>مكان العمل الحالي</td>
 			  	    <td><?php echo $task['current_company']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>يقبل فرص العمل الأخرى</td>
 			  	    <td><?php echo $task['accept_other_jobs'] == 1? "نعم":"لا";; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>مسجل في طاقات</td>
 			  	    <td><?php echo $task['taqaat_registered'] == 1? "نعم":"لا"; ?></td>
 			  	</tr>
          <tr>
              <td>تعليق الموظف</td>
              <td><?php echo $task['comment']; ?></td>
          </tr>
 			  	<tr>
 			  	    <td>الملفات</td>
 			  	    <td>
 			  	    	<?php foreach ($task['files'] as $key => $value): ?>
 			  	    		<a href="<?php echo base_url() ."public/uploads/" . $value ?>"><?php echo $value ?></a>
 			  	    	<?php endforeach ?>
 			  	    </td>
 			  	</tr>
			  </tbody>
			</table>
      <?php if ($this->session->userdata('role') == 'Editor'): ?>
        <?php if ($task['task_status'] == 'جديد'): ?>
          <a href="" data-dismiss="modal" data-toggle="modal" data-target="#finishModal" class="btn btn-success btn-block">إنهاء المهمة</a>
        <?php else: ?>
          <a href="javascript:void()" class="btn btn-success btn-block" disabled="disabled">المهمة منتهية</a>
        <?php endif ?>
      <?php endif ?>


		</div>
	</div>

</section>



<!--Modal-Finish Start-->
<div style="direction: rtl!important;text-align: right!important;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div>
                    <div class="modal fade" id="finishModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">

                            <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="staticBackdropLabel">من فضلك قم بكتابة تعقيب على المهمة المنجزة</h5>
                                  <button type="button" class="close" style="padding: 1rem 1rem;margin: -1rem auto -1rem -1rem;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                    <?php $fattr = array('class' => 'form-signin');
                                         echo form_open(base_url().'admin/task/view/'.$task['task_id'], $fattr); ?>
                                    <div>
                                      <?php 
                                      echo 
                                      form_textarea(array(
                                          'name'=>'comment', 
                                          'id'=> 'comment', 
                                          'placeholder'=>'كتابة تعليق للإنهاء',
                                          'cols' => '92',
                                          'rows' => '5',
                                          'required' => 'required',
                                          'style' => 'border: 1px solid #f2f2f2;padding: 10px;margin: 16px 0;width: 100%;'
                                          ));
                                          ?>
                                      <?php echo form_error('type') ?>
                                    </div>
                                    <?php echo form_submit(array('value'=>'إنهاء', 'class'=>'btn btn-lg btn-success btn-block','name'=>'finish')); ?>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Modal-Finish End-->
