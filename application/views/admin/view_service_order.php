<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || ($this->session->userdata('role') !== 'Admin' && $this->session->userdata('role') !== 'Editor')) {
    redirect(base_url().'admin/login');
}
?>
<section class="content-header">
	<div class="content-header-left">
		<h1>عرض طلبات الخدمات</h1>
	</div>
</section>


<section class="content">

  <div class="row">
    <div class="col-md-12">
        
        <?php
        if($this->session->flashdata('error')) {
            ?>
            <div class="callout callout-danger">
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
            <?php
        }
        if($this->session->flashdata('success')) {
            ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
            <?php
        }
        ?>

       <div class="box box-info">
        
        <div class="box-body table-responsive">
          <table id="example1" class="table table-bordered table-striped">
			<thead>
			    <tr>
			        <th>#</th>
			        <th>التفاصيل</th>
                    <th>العميل</th>
                    <th>الموظف</th>
                    <th>حالة الطلب</th>
                    <th>تاريخ الطلب</th>
			        <th>الإجراء</th>
			    </tr>
			</thead>
            <tbody>
            	<?php
            	$i=0;
            	foreach ($service_orders as $row) {
            		$i++;
            		?>
					<tr>
	                    <td><?php echo $row['order_id']; ?></td>
                        <td>
                            <ul>
                                <li>
                                    نوع الطلب: 
                                    <b>[<?php echo $row['name']; ?>]</b>
                                </li>
                                <?php if (is_array($row['required_inputs'])): ?>
                                    <?php
                                    $i=0;
                                    foreach ($row['required_inputs'] as $input) {
                                        $i++;
                                        ?>

                                        <li>
                                            <?php echo $input->label; ?>:
                                            <b>[<?php echo $input->value; ?>]</b>
                                        </li>

                                    <?php 
                                        };
                                        ?>
                                <?php endif ?>
                                <?php if (is_array($row['order_files'])): ?>
                                    <li>
                                        الملفات المرفقة: 
                                        <ul>
                                            <?php
                                            $i=0;
                                            foreach ($row['order_files'] as $file) {
                                                $i++;
                                                ?>

                                                <li>
                                                    <a href="<?php echo base_url(); ?>public/uploads/<?php echo $file; ?>"><?php echo $file; ?></a>
                                                </li>

                                            <?php 
                                                };
                                                ?>
                                        </ul>
                                    </li>                                    
                                <?php endif ?>

                            </ul>
                            
                        </td>
                        <td><?php echo $row['user_name']; ?></td>
                        <td><?php echo $row['team_member_name']; ?></td>
                        <td><?php echo $row['order_status']; ?></td>
                        <td><?php echo $row['date_time']; ?></td>
	                    <td>
	                        <a href="<?php echo base_url(); ?>admin/service_order/edit/<?php echo $row['order_id']; ?>" class="btn btn-primary btn-xs">تعديل</a>
	                    </td>
	                </tr>
            		<?php
            	}
            	?>
            </tbody>
          </table>
        </div>
      </div>
  

</section>