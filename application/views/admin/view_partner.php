<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || $this->session->userdata('role') !== 'Admin') {
    redirect(base_url().'admin');
}
?>
<section class="content-header">
	<div class="content-header-left">
		<h1>شركاء التوظيف</h1>
	</div>
	<div class="content-header-right">
		<a href="<?php echo base_url(); ?>admin/partner/add" class="btn btn-primary btn-sm">إضافة شريك توظيف</a>
	</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info">
				<div class="box-body table-responsive">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="30">#</th>
								<th>الصورة الرمزية</th>
								<th width="100">إسم الشركة</th>
								<th width="80">الإجراء</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=0;
							foreach ($partner as $row) {
								$i++;
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td style="width:130px;"><img src="<?php echo base_url(); ?>public/uploads/<?php echo $row['photo']; ?>" alt="<?php echo $row['name']; ?>" style="width:120px;"></td>
									<td><?php echo $row['name']; ?></td>
									<td>										
										<a href="<?php echo base_url(); ?>admin/partner/edit/<?php echo $row['id']; ?>" class="btn btn-primary btn-xs">تعديل</a>
										<a href="#" class="btn btn-danger btn-xs" data-href="<?php echo base_url(); ?>admin/partner/delete/<?php echo $row['id']; ?>" data-toggle="modal" data-target="#confirm-delete">حذف</a>  
									</td>
								</tr>
								<?php
							}
							?>							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


</section>


<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">تأكيد الحذف</h4>
            </div>
            <div class="modal-body">
                <p>هل تريد اتمام عملية الحذف؟</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">إلغاء</button>
                <a class="btn btn-danger btn-ok">حذف</a>
            </div>
        </div>
    </div>
</div>