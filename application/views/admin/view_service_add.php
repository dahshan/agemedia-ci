<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || $this->session->userdata('role') !== 'Admin') {
	redirect(base_url().'admin');
}
?>

<section class="content-header">
	<div class="content-header-left">
		<h1>إضافة خدمة</h1>
	</div>
	<div class="content-header-right">
		<a href="<?php echo base_url(); ?>admin/service" class="btn btn-primary btn-sm">عرض الكل</a>
	</div>
</section>


<section class="content">

	<div class="row">
		<div class="col-md-12">

			<?php
			if($this->session->flashdata('error')) {
				?>
				<div class="callout callout-danger">
					<p><?php echo $this->session->flashdata('error'); ?></p>
				</div>
				<?php
			}
			if($this->session->flashdata('success')) {
				?>
				<div class="callout callout-success">
					<p><?php echo $this->session->flashdata('success'); ?></p>
				</div>
				<?php
			}
			?>

			<?php echo form_open_multipart(base_url().'admin/service/add',array('class' => 'form-horizontal')); ?>
				<div class="box box-info">
					<div class="box-body">
						
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">إسم الخدمة *</label>
							<div class="col-sm-6">
								<input type="text" autocomplete="off" class="form-control" name="name" value="">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">وصف قصير *</label>
							<div class="col-sm-8">
								<textarea class="form-control" name="short_description" style="height:140px;"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">الوصف *</label>
							<div class="col-sm-8">
								<textarea class="form-control editor" name="description"></textarea>
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-2 control-label">الوقت المستغرق *</label>
							<div class="col-sm-6">
								<input type="text" autocomplete="off" class="form-control" name="duration" value="">
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-2 control-label">السعر *</label>
							<div class="col-sm-6">
								<input type="number" autocomplete="off" class="form-control" name="price" value="">
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-2 control-label">الحقول المطلوبة للتقديم *</label>
							<div class="col-sm-8">
								<textarea class="form-control" name="required_inputs" style="height:140px;direction: ltr;font-size: large !important;">[
	{"name": "current_owner_number", "label": "رقم المالك الحالي"},
	{"name": "serial_number", "label": "الرقم التسلسلي"}
]</textarea>
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-2 control-label">التصنيف *</label>
							<div class="col-sm-4">
								<select name="category_id" class="form-control select2">
									<?php
									foreach ($all_service_category as $row) {
										?>
										<option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name']; ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-2 control-label">الصورة *</label>
							<div class="col-sm-9" style="padding-top:5px">
								<input type="file" name="photo">(Only jpg, jpeg, gif and png are allowed)
							</div>
						</div>
						<h3 class="seo-info">معلومات محركات البحث</h3>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">عنوان ميتا</label>
							<div class="col-sm-6">
								<input type="text" autocomplete="off" class="form-control" name="meta_title" value="">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">كلمات مفتاحية</label>
							<div class="col-sm-8">
								<textarea class="form-control" name="meta_keyword" style="height:80px;"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">وصف ميتا</label>
							<div class="col-sm-8">
								<textarea class="form-control" name="meta_description" style="height:80px;"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">اللغة </label>
							<div class="col-sm-2">
								<select name="lang_id" class="form-control select2">
									<?php
									foreach($all_lang as $row)
									{
										?><option value="<?php echo $row['lang_id']; ?>"><?php echo $row['lang_name']; ?></option><?php
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label"></label>
							<div class="col-sm-6">
								<button type="submit" class="btn btn-success pull-left" name="form1">إرسال</button>
							</div>
						</div>

					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>

</section>