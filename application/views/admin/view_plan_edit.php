<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || $this->session->userdata('role') !== 'Admin') {
	redirect(base_url().'admin');
}
?>
<section class="content-header">
	<div class="content-header-left">
		<h1>تعديل خطة</h1>
	</div>
	<div class="content-header-right">
		<a href="<?php echo base_url(); ?>admin/plan" class="btn btn-primary btn-sm">عرض الكل</a>
	</div>
</section>

<section class="content">

	<div class="row">
		<div class="col-md-12">

			<?php
			if($this->session->flashdata('error')) {
				?>
				<div class="callout callout-danger">
					<p><?php echo $this->session->flashdata('error'); ?></p>
				</div>
				<?php
			}
			if($this->session->flashdata('success')) {
				?>
				<div class="callout callout-success">
					<p><?php echo $this->session->flashdata('success'); ?></p>
				</div>
				<?php
			}
			?>

			<?php echo form_open_multipart(base_url().'admin/plan/edit/'.$feature['id'],array('class' => 'form-horizontal'));?>
				<div class="box box-info">
					<div class="box-body">						
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">إسم الخطة *</label>
							<div class="col-sm-8">
								<input type="text" autocomplete="off" class="form-control" name="name" value="<?php echo $feature['name']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">سعر الخطة بالريال *</label>
							<div class="col-sm-8">
								<input type="number" autocomplete="off" class="form-control" name="price" value="<?php echo $feature['price']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">مدة الخطة بالأيام *</label>
							<div class="col-sm-8">
								<input type="number" autocomplete="off" class="form-control" name="duration" value="<?php echo $feature['duration']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label"></label>
							<div class="col-sm-6">
								<button type="submit" class="btn btn-success pull-right" name="form1">تحديث</button>
							</div>
						</div>
					</div>
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>

</section>