<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || (($this->session->userdata('role') !== 'Editor') && ($this->session->userdata('role') !== 'Admin'))) {
    redirect(base_url().'admin/login');
}
?>
<section class="content-header">
	<div class="content-header-left">
		<h1>عرض الإشتراكات النشطة</h1>
	</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">

			<?php
	        if($this->session->flashdata('error')) {
	            ?>
				<div class="callout callout-danger">
					<p><?php echo $this->session->flashdata('error'); ?></p>
				</div>
	            <?php
	        }
	        if($this->session->flashdata('success')) {
	            ?>
				<div class="callout callout-success">
					<p><?php echo $this->session->flashdata('success'); ?></p>
				</div>
	            <?php
	        }
	        ?>
	        
			<div class="box box-info">
				<div class="box-body table-responsive">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>رقم الإشتراك</th>
								<th>العميل</th>
								<th>الموظف</th>
								<th>الخطة</th>
								<th>يبدأ في</th>
								<th>ينتهي في</th>
								<th>ايام التمديد</th>
								<th width="140">الإجراء</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=0;							
							foreach ($feature as $row) {
								$i++;
								?>
								<tr>
									<td><?php echo $row['id']; ?></td>
									<td><?php echo $row['user_name']; ?></td>
									<td><?php echo $row['employee_name']; ?>
										(
										<a href="" data-dismiss="modal" data-toggle="modal" data-target="#reassignModal<?php echo $row['id']; ?>">تغيير</a>
										)
									</td>
									<td><?php echo $row['plan_name']; ?></td>
									<td><?php echo date('Y-m-d',strtotime('+1 days',strtotime($row['date_time']))); ?></td>
									<td><?php echo date('Y-m-d',strtotime('+'.$row['plan_duration'] .' days',strtotime($row['date_time']))); ?>
									</td>
									<td><?php echo $row['extended']; ?></td>
									<td>
										<a href="<?php echo base_url(); ?>admin/subscription/view/<?php echo $row['id']; ?>" class="btn btn-primary btn-xs">عرض</a>
										<a href="<?php echo base_url(); ?>admin/subscription/extend/<?php echo $row['id']; ?>" class="btn btn-primary btn-xs">تمديد</a>
									</td>
								</tr>
								<?php
							}
							?>							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<?php foreach ($feature as $sub): ?>
	

<!--Modal-Re-assign Start-->
<div style="direction: rtl!important;text-align: right!important;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div>
                    <div class="modal fade" id="reassignModal<?php echo $sub['id'] ?>" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">

                            <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="staticBackdropLabel">تغيير الموظف</h5>
                                  <button type="button" class="close" style="padding: 1rem 1rem;margin: -1rem auto -1rem -1rem;" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                    <?php $fattr = array('class' => 'form-signin');
                                         echo form_open(base_url().'admin/subscription', $fattr); ?>
                                    <input type="hidden" name="sub_id" value="<?php echo $sub['id'] ?>">
                                    <div>
                                    	<label>الموظف:</label>
                                    	<br>
                                    	<?php foreach ($employee as $row): ?>
                                    		<?php 
                                    		echo 
                                    		form_radio(array(
                                    		    'name'=>'employee', 
                                    		    'id'=> 'employee'.$row['id'].'-'.$sub['id'], 
                                    		    'value'=>$row['id'], 
                                    		    'required' => 'required',
                                    		    'checked'=> ($sub['employee_id'] == $row['id'])? true : false
                                    		));
                                    		echo form_label($row['name'], 'employee'.$row['id'].'-'.$sub['id']);
                                    		    ?>
                                    		    <br>
                                    	<?php endforeach ?>
	                                    <br>
	                                    <br>
	                                    <br>
                                      <?php echo form_error('type') ?>
                                    </div>
                                    <?php echo form_submit(array('value'=>'تنفيذ', 'class'=>'btn btn-lg btn-success btn-block','name'=>'re_assign')); ?>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Modal-Re-assign End-->
<?php endforeach ?>
