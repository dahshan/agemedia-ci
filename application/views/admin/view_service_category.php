<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || $this->session->userdata('role') !== 'Admin') {
    redirect(base_url().'admin/login');
}
?>
<section class="content-header">
	<div class="content-header-left">
		<h1>عرض تصنيفات الخدمات</h1>
	</div>
	<div class="content-header-right">
		<a href="<?php echo base_url(); ?>admin/service_category/add" class="btn btn-primary btn-sm">إضافة تصنيف</a>
	</div>
</section>


<section class="content">

  <div class="row">
    <div class="col-md-12">
        
        <?php
        if($this->session->flashdata('error')) {
            ?>
            <div class="callout callout-danger">
                <p><?php echo $this->session->flashdata('error'); ?></p>
            </div>
            <?php
        }
        if($this->session->flashdata('success')) {
            ?>
            <div class="callout callout-success">
                <p><?php echo $this->session->flashdata('success'); ?></p>
            </div>
            <?php
        }
        ?>

       <div class="box box-info">
        
        <div class="box-body table-responsive">
          <table id="example1" class="table table-bordered table-striped">
			<thead>
			    <tr>
			        <th>#</th>
			        <th>إسم التصنيف</th>
			        <th>الحالة</th>
                    <th>اللغة</th>
			        <th>الإجراء</th>
			    </tr>
			</thead>
            <tbody>
            	<?php
            	$i=0;
            	foreach ($service_category as $row) {
            		$i++;
            		?>
					<tr>
	                    <td><?php echo $i; ?></td>
	                    <td><?php echo $row['category_name']; ?></td>
	                    <td><?php echo $row['status']; ?></td>
                        <td><?php echo $row['lang_name']; ?></td>
	                    <td>
	                        <a href="<?php echo base_url(); ?>admin/service_category/edit/<?php echo $row['category_id']; ?>" class="btn btn-primary btn-xs">تعديل</a>
                            <a href="<?php echo base_url(); ?>admin/service_category/delete/<?php echo $row['category_id']; ?>" class="btn btn-danger btn-xs" onClick="return confirm('Are you sure?');">حذف</a>
	                    </td>
	                </tr>
            		<?php
            	}
            	?>
            </tbody>
          </table>
        </div>
      </div>
  

</section>