<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || (($this->session->userdata('role') !== 'Editor') && ($this->session->userdata('role') !== 'Admin'))) {
    redirect(base_url().'admin/login');
}
?>
<section class="content-header">
	<div class="content-header-left">
		<h1>عرض الإشتراكات المنتهية</h1>
	</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">

			<?php
	        if($this->session->flashdata('error')) {
	            ?>
				<div class="callout callout-danger">
					<p><?php echo $this->session->flashdata('error'); ?></p>
				</div>
	            <?php
	        }
	        if($this->session->flashdata('success')) {
	            ?>
				<div class="callout callout-success">
					<p><?php echo $this->session->flashdata('success'); ?></p>
				</div>
	            <?php
	        }
	        ?>
	        
			<div class="box box-info">
				<div class="box-body table-responsive">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>رقم الإشتراك</th>
								<th>العميل</th>
								<th>الموظف</th>
								<th>الخطة</th>
								<th>يبدأ في</th>
								<th>ينتهي في</th>
								<th width="140">الإجراء</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=0;							
							foreach ($feature as $row) {
								$i++;
								?>
								<tr>
									<td><?php echo $row['id']; ?></td>
									<td><?php echo $row['user_name']; ?></td>
									<td><?php echo $row['employee_name']; ?></td>
									<td><?php echo $row['plan_name']; ?></td>
									<td><?php echo date('Y-m-d',strtotime('+1 days',strtotime($row['date_time']))); ?></td>
									<td><?php echo date('Y-m-d',strtotime('+'.$row['plan_duration'] .' days',strtotime($row['date_time']))); ?>
									</td>
									<td>
										<a href="<?php echo base_url(); ?>admin/subscription/view/<?php echo $row['id']; ?>" class="btn btn-primary btn-xs">عرض</a>
									</td>
								</tr>
								<?php
							}
							?>							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>