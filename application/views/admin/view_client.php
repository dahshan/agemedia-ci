<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || $this->session->userdata('role') !== 'Admin') {
    redirect(base_url().'admin');
}
?>

<section class="content-header">
	<div class="content-header-left">
		<h1>عرض المستخدمين</h1>
	</div>
	<div class="content-header-right">
		<a href="<?php echo base_url(); ?>admin/client/add" class="btn btn-primary btn-sm">إضافة مستخدم</a>
	</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">

			<?php
	        if($this->session->flashdata('error')) {
	            ?>
				<div class="callout callout-danger">
					<p><?php echo $this->session->flashdata('error'); ?></p>
				</div>
	            <?php
	        }
	        if($this->session->flashdata('success')) {
	            ?>
				<div class="callout callout-success">
					<p><?php echo $this->session->flashdata('success'); ?></p>
				</div>
	            <?php
	        }
	        ?>

	        
			<div class="box box-info">
				<div class="box-body table-responsive">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="30">#</th>
								<th>الصورة الرمزية</th>
								<th width="100">الإسم</th>
								<th width="100">الصلاحية</th>
								<th width="80">الإجراء</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=0;							
							foreach ($client as $row) {
								$i++;
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td style="width:130px;"><img src="<?php echo base_url(); ?>public/uploads/<?php echo $row['photo']; ?>" alt="<?php echo $row['name']; ?>" style="width:120px;"></td>
									<td><?php echo $row['name']; ?></td>
									<td><?php echo $row['role']; ?></td>
									<td>										
										<a href="<?php echo base_url(); ?>admin/client/edit/<?php echo $row['id']; ?>" class="btn btn-primary btn-xs">تعديل</a>
										<a href="<?php echo base_url(); ?>admin/client/delete/<?php echo $row['id']; ?>" class="btn btn-danger btn-xs" onClick="return confirm('Are you sure?');">حذف</a>
										<?php if ($this->uri->segment(4) == 'Editor'): ?>
											<a href="<?php echo base_url(); ?>admin/client/message/<?php echo $row['id']; ?>" class="btn btn-primary btn-xs" style="margin-right: 25px;">إرسال رسالة <i class="fa fa-send"></i></a>
										<?php endif ?>
									</td>
								</tr>
								<?php
							}
							?>							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


</section>