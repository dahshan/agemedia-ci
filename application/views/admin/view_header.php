<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Admin Panel</title>

	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/datepicker3.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/all.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/select2.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/dataTables.bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/jquery.fancybox.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/AdminLTE.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/_all-skins.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/style.css">
	


	<style>
		.skin-blue .wrapper,
		.skin-blue .main-header .logo,
		.skin-blue .main-header .navbar,
		.skin-blue .main-sidebar,
		.content-header .content-header-right a,
		.content .form-horizontal .btn-success {
			background-color: #4172a5!important;
		}

		.content-header .content-header-right a,
		.content .form-horizontal .btn-success {
			border-color: #4172a5!important;
		}
		
		.content-header>h1,
		.content-header .content-header-left h1,
		h3 {
			color: #4172a5!important;
		}

		.box.box-info {
			border-top-color: #4172a5!important;
		}

		.nav-tabs-custom>.nav-tabs>li.active {
			border-top-color: #f4f4f4!important;
		}

		.skin-blue .sidebar a {
			color: #fff!important;
		}

		.skin-blue .sidebar-menu>li>.treeview-menu {
			margin: 0!important;
		}
		.skin-blue .sidebar-menu>li>a {
			border-left: 0!important;
		}

		.nav-tabs-custom>.nav-tabs>li {
			border-top-width: 1px!important;
		}

	</style>



</head>

<body class="hold-transition fixed skin-blue sidebar-mini">

	<div class="wrapper">

		<header class="main-header">

			<a href="<?php echo base_url(); ?>admin/dashboard" class="logo" style="background-color: #588cc3!important;">
				<span class="logo-lg"><?php echo $setting['website_name']; ?></span>
			</a>

			<nav class="navbar navbar-static-top">
				
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>

				<span style="float:right;line-height:50px;color:#fff;padding-left:15px;font-size:18px;">
					<?php if ($this->session->userdata('role') == 'Admin'): ?>
						لوحة التحكم
					<?php else: ?>
						لوحة الموظف
					<?php endif ?>
				</span>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li>
							<a href="<?php echo base_url(); ?>" target="_blank">زيارة الموقع</a>
						</li>

						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<?php if($this->session->userdata('photo') == ''): ?>
									<img src="<?php echo base_url(); ?>public/img/no-photo.jpg" class="user-image" alt="user photo">
								<?php else: ?>
									<img src="<?php echo base_url(); ?>public/uploads/<?php echo $this->session->userdata('photo'); ?>" class="user-image" alt="user photo">
								<?php endif; ?>
								
								<span class="hidden-xs"><?php echo $this->session->userdata('full_name'); ?></span>
							</a>
							<ul class="dropdown-menu">
								<li class="user-footer">
									<?php if ($this->session->userdata('role') == 'Admin'): ?>
										<div>
											<a href="<?php echo base_url(); ?>admin/profile" class="btn btn-default btn-flat">تعديل البيانات</a>
										</div>										
									<?php endif ?>
									<div>
										<a href="<?php echo base_url(); ?>admin/login/logout" class="btn btn-default btn-flat">تسجيل الخروج</a>
									</div>
								</li>
							</ul>
						</li>
						
					</ul>
				</div>

			</nav>
		</header>

  		<?php
			$class_name = '';
		    $segment_2 = 0;
		    $segment_3 = 0;
		    $class_name = $this->router->fetch_class();
		    $segment_2 = $this->uri->segment('2');
		    $segment_3 = $this->uri->segment('3');
		?>

  		<aside class="main-sidebar">
    		<section class="sidebar">

     
      			<ul class="sidebar-menu">

      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if($class_name == 'dashboard') {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/dashboard">
      					    <i class="fa fa-laptop"></i> <span>الرئيسية</span>
      					  </a>
      					</li>      					
      				<?php endif ?>

      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if( ($class_name == 'setting') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/setting">
      					    <i class="fa fa-cog"></i> <span>الإعدادات</span>
      					  </a>
      					</li>      					
      				<?php endif ?>

      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if( ($class_name == 'plan') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/plan">
      					    <i class="fa fa-cog"></i> <span>باقات الإشتراك</span>
      					  </a>
      					</li>      					
      				<?php endif ?>



      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
				        <li class="treeview <?php if( ($class_name == 'client') ) {echo 'active';} ?>">
							<a href="#">
								<i class="fa fa-life-ring"></i>
								<span>المستخدمين</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-down pull-left"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<li><a href="<?php echo base_url(); ?>admin/client/index/Admin"><i class="fa fa-circle-o"></i> المديرين</a></li>
								<li><a href="<?php echo base_url(); ?>admin/client/index/Editor"><i class="fa fa-circle-o"></i> الموظفين</a></li>
								<li><a href="<?php echo base_url(); ?>admin/client/index/Client"><i class="fa fa-circle-o"></i> العملاء</a></li>
							</ul>
						</li>
      				<?php endif ?>


      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
				        <li class="treeview <?php if( $class_name == 'subscription' ) {echo 'active';} ?>">
							<a href="#">
								<i class="fa fa-life-ring"></i>
								<span>الإشتراكات</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-down pull-left"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<li><a href="<?php echo base_url(); ?>admin/subscription/new"><i class="fa fa-circle-o"></i>
								 الإشتراكات الجديدة
								 (
								 <?php 
								 echo $this->Model_subscription->new(true);
								  ?>
								  )
								</a></li>
								<li><a href="<?php echo base_url(); ?>admin/subscription"><i class="fa fa-circle-o"></i>
								 الإشتراكات النشطه
								 (
								 <?php 
								 echo $this->Model_subscription->show(true);
								  ?>
								  )
								</a></li>
								<li><a href="<?php echo base_url(); ?>admin/subscription/archive"><i class="fa fa-circle-o"></i>
								 الإشتراكات المنتهية
								 (
								 <?php 
								 echo $this->Model_subscription->archive(true);
								  ?>
								  )
								</a></li>
							</ul>
						</li>
      				<?php endif ?>


      				<?php if ($this->session->userdata('role') == 'Admin' || $this->session->userdata('role') == 'Editor'): ?>
				        <li class="treeview <?php if($class_name == 'task') {echo 'active';} ?>">
							<a href="#">
								<i class="fa fa-life-ring"></i>
								<span>المهام</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-down pull-left"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<li><a href="<?php echo base_url(); ?>admin/task"><i class="fa fa-circle-o"></i>
								 المهام النشطة 
									(
									<?php 
									if ($this->session->userdata('role') == 'Admin') {
										echo $this->Model_task->show('Admin', true);
									} else {
										echo $this->Model_task->show($this->session->userdata('id'), true);
									}
									 ?>
									 )
								</a></li>
								<li><a href="<?php echo base_url(); ?>admin/task/archive"><i class="fa fa-circle-o"></i>
								 المهام المنتهية
								 (
								 <?php 
								 if ($this->session->userdata('role') == 'Admin') {
								 	echo $this->Model_task->archive('Admin', true);
								 } else {
								 	echo $this->Model_task->archive($this->session->userdata('id'), true);
								 }
								  ?>
								  )
								</a></li>
							</ul>
						</li>
      				<?php endif ?>

      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if( ($class_name == 'team_member') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/team_member">
      					    <i class="fa fa-clone"></i> <span>فريق العمل</span>
      					  </a>
      					</li>      					
      				<?php endif ?>


      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if( ($class_name == 'partner') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/partner">
      					    <i class="fa fa-clone"></i> <span>خيارات التوظيف</span>
      					  </a>
      					</li>      					
      				<?php endif ?>


      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if( ($class_name == 'specialization') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/specialization">
      					    <i class="fa fa-clone"></i> <span>تخصصات التوظيف</span>
      					  </a>
      					</li>      					
      				<?php endif ?>


      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if( ($class_name == 'place') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/place">
      					    <i class="fa fa-clone"></i> <span>أماكن التوظيف</span>
      					  </a>
      					</li>      					
      				<?php endif ?>


      				<?php if (0 && $this->session->userdata('role') == 'Admin'): ?>
				        <li class="treeview <?php if( ($class_name == 'service')||($class_name == 'service_category') ) {echo 'active';} ?>">
							<a href="#">
								<i class="fa fa-life-ring"></i>
								<span>الخدمات</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-down pull-left"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<li><a href="<?php echo base_url(); ?>admin/service_category"><i class="fa fa-circle-o"></i> تصنيفات الخدمات</a></li>
								<li><a href="<?php echo base_url(); ?>admin/service"><i class="fa fa-circle-o"></i> الخدمات</a></li>
							</ul>
						</li>
      				<?php endif ?>


      				<?php if (0 && $this->session->userdata('role') == 'Admin' || 0 &&$this->session->userdata('role') == 'Editor'): ?>
				        <li class="treeview <?php if( ($class_name == 'service_order')||($class_name == 'recruitment_order') ) {echo 'active';} ?>">
							<a href="#">
								<i class="fa fa-life-ring"></i>
								<span>الطلبات</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-down pull-left"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<li><a href="<?php echo base_url(); ?>admin/service_order"><i class="fa fa-circle-o"></i>طلبات الخدمات</a></li>
								<li><a href="<?php echo base_url(); ?>admin/recruitment_order"><i class="fa fa-circle-o"></i>طلبات الوظائف</a></li>
							</ul>
						</li>
      				<?php endif ?>

      				<?php if (0 && $this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if( ($class_name == 'team_member') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/team_member">
      					    <i class="fa fa-users"></i> <span>اعضاء الفريق</span>
      					  </a>
      					</li>      					
      				<?php endif ?>


      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if( ($class_name == 'slider') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/slider">
      					    <i class="fa fa-picture-o"></i> <span>الشرائح</span>
      					  </a>
      					</li>      					
      				<?php endif ?>


      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if( ($class_name == 'testimonial') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/testimonial">
      					    <i class="fa fa-user-plus"></i> <span>آراء العملاء</span>
      					  </a>
      					</li>      					
      				<?php endif ?>



      				<?php if (0 && $this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if( ($class_name == 'feature') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/feature">
      					    <i class="fa fa-cube"></i> <span>المميزات</span>
      					  </a>
      					</li>      					
      				<?php endif ?>


      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if( ($class_name == 'why_choose') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/why_choose">
      					    <i class="fa fa-paper-plane-o"></i> <span>لماذا أخترتنا</span>
      					  </a>
      					</li>      					
      				<?php endif ?>

      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if( ($class_name == 'page_privacy') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/page_privacy">
      					    <i class="fa fa-paper-plane-o"></i> <span>سياسة الخصوصية</span>
      					  </a>
      					</li>      					
      				<?php endif ?>

      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if( ($class_name == 'page_term') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/page_term">
      					    <i class="fa fa-paper-plane-o"></i> <span>الشروط والأحكام</span>
      					  </a>
      					</li>      					
      				<?php endif ?>

      				<?php if ($this->session->userdata('role') == 'Admin' || $this->session->userdata('role') == 'Editor'): ?>
      					<li class="treeview <?php if( ($class_name == 'user_message') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/user_message">
      					    <i class="fa fa-paper-plane-o"></i> <span>رسائل الإدارة</span>
      					  </a>
      					</li>      					
      				<?php endif ?>


      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if( ($class_name == 'footer_setting') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/footer_setting">
      					    <i class="fa fa-address-book"></i> <span>ضبط إعدادات الفوتر</span>
      					  </a>
      					</li>      					
      				<?php endif ?>

      				<?php if ($this->session->userdata('role') == 'Admin'): ?>
      					<li class="treeview <?php if( ($class_name == 'social_media') ) {echo 'active';} ?>">
      					  <a href="<?php echo base_url(); ?>admin/social_media">
      					    <i class="fa fa-address-book"></i> <span>حسابات مواقع التواصل</span>
      					  </a>
      					</li>      					
      				<?php endif ?>



			        	        
       
      			</ul>
    		</section>
  		</aside>

  		<div class="content-wrapper">