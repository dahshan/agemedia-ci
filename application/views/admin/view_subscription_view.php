<?php
if(!$this->session->userdata('id') || !$this->session->userdata('role') || (($this->session->userdata('role') !== 'Editor') && ($this->session->userdata('role') !== 'Admin'))) {
    redirect(base_url().'admin/login');
}
?>
<section class="content-header">
	<div class="content-header-left">
		<h1>تفاصيل الإشتراك</h1>
	</div>
	<div class="content-header-right">
		<a href="<?php echo base_url(); ?>admin/subscription" class="btn btn-primary btn-sm">عرض الكل</a>
	</div>
</section>

<section class="content">

	<div class="row">
		<div class="col-md-12">

			<table class="table table-striped" style="width: 90%; margin: 20px auto;">
 			  <tbody>
 			  	<tr>
 			  	    <td>إسم العميل</td>
 			  	    <td><?php echo $task['name']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>رقم هاتف العميل</td>
 			  	    <td><?php echo $task['phone']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>المؤهل الدراسي</td>
 			  	    <td><?php echo $task['qualification']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>التخصص</td>
 			  	    <td><?php echo $task['specialization']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>دولة المؤهل</td>
 			  	    <td><?php echo $task['country']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>مدينة المؤهل</td>
 			  	    <td><?php echo $task['city']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>الشركة او الجامعة</td>
 			  	    <td><?php echo $task['organization']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>تاريخ التخرج</td>
 			  	    <td><?php echo $task['graduation_date']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>التقدير او الدرجة</td>
 			  	    <td><?php echo $task['graduation_grade']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>الحد الأدنى للراتب</td>
 			  	    <td><?php echo $task['minimum_salary']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>مكان العمل المفضل</td>
 			  	    <td><?php echo $task['favorite_place']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>نمط العمل المفضل</td>
 			  	    <td><?php echo $task['favorite_type']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>حالة العمل الآن</td>
 			  	    <td><?php echo $task['work_status']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>الوظيفة الحالية</td>
 			  	    <td><?php echo $task['current_job']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>الراتب الحالي</td>
 			  	    <td><?php echo $task['current_salary']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>مكان العمل الحالي</td>
 			  	    <td><?php echo $task['current_company']; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>يقبل فرص العمل الأخرى</td>
 			  	    <td><?php echo $task['accept_other_jobs'] == 1? "نعم":"لا";; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>مسجل في طاقات</td>
 			  	    <td><?php echo $task['taqaat_registered'] == 1? "نعم":"لا"; ?></td>
 			  	</tr>
 			  	<tr>
 			  	    <td>الملفات</td>
 			  	    <td>
 			  	    	<?php foreach ($task['files'] as $key => $value): ?>
 			  	    		<a href="<?php echo base_url() ."public/uploads/" . $value ?>"><?php echo $value ?></a>
 			  	    	<?php endforeach ?>
 			  	    </td>
 			  	</tr>
			  </tbody>
			</table>


		</div>
	</div>

</section>


