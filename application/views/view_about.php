      <div class="content">
         <section class="content">
         <section class="inner-head" style="background-image: url(public/site/img/home-cover0.jpeg);">
            <div class="container">
               <h1 class="title">عن الشركة</h1>
            </div>
         </section>
         <section class="about pt-40 pb-40">
            <div class="container">
               <div class="row">
                  <div class="col-12 col-lg-6">
                     <h2 class="title font-25 bold dark-color mb-10">شركة إيج ميديا لتقنية المعلومات</h2>
                     <p class="info-content font-20 mb-20">إيج ميديا لتقنية المعلومات، إسم رائد فى مجال برمجة وتصميم المواقع، تطبيقات الجوال، المتاجر الإلكترونية، وخدمات التسويق الإلكتروني المتنوعة، في المملكة العربية السعودية، خبرة أكثر من ثلاثة عشر عاماً، تضم أكثر من مئة وخمسون موظف، يسعون دائما للإبداع و التطور المستمر من أجل تحويل أفكاركم الإبداعية إلى واقع مليئ بالنجاحات.</p>
                     <h2 class="font-25 bold dark-color mb-10">رؤيتنا:</h2>
                     <p class="font-20 mb-20">نسعى الي تحويل الافكار والمبادرات الي ارض الواقع .</p>
                     <h2 class="font-25 bold dark-color mb-10">رسالتنا:</h2>
                     <p class="font-20 mb-20">نسعى فى شركة إيج ميديا الارتقاء بمجال تكنولوجيا المعلومات من خلال الخدمات التي تقدمها الشركة والتي تتناسب مع جميع الافراد والمؤسسات باعلي جودة وفي اسرع وقت</p>
                  </div>
                  <div class="col-12 col-lg-6"> <img src="public/site/img/about.png" alt="" class="img w-100"> </div>
               </div>
            </div>
         </section>
         <section class="contact-us pt-40 pb-40 blue-bg">
            <div class="container">
               <div class="mb-40">
                  <h2 class="section-title"> اتصل بنا </h2>
                  <p class="section-subtitle"> نتشرف بخدمتكم وتواصلكم معنا </p>
               </div>
               <div class="row">
                  <div class="col-12 col-md-4">
                     <div class="contact-box green">
                        <i class="fab icon-fa-whatsapp"></i> 
                        <p class="title dark-color bold font-15 mb-10"> للتواصل عبر الواتساب </p>
                        <p class="info-content"> 0550016001 </p>
                        <a href="https://api.whatsapp.com/send?phone=966550016001" class="box-btn"> للتواصل عبر الواتساب </a> 
                     </div>
                  </div>
                  <div class="col-12 col-md-4">
                     <div class="contact-box blue">
                        <i class="fas icon-fa-phone-1"></i> 
                        <p class="title dark-color bold font-15 mb-10"> للإتصال والإستفسار </p>
                        <p class="info-content"> 0550016001 </p>
                        <a href="tel:0550016001" class="box-btn"> اتصل بنا </a> 
                     </div>
                  </div>
                  <div class="col-12 col-md-4">
                     <div class="contact-box dark">
                        <i class="far icon-fa-envelope"></i> 
                        <p class="title dark-color bold font-15 mb-10"> للمراسلة المباشرة </p>
                        <p class="info-content"> <a href="cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="9ceffdf0f9efdcfdfdf5e8b2effd">[email&#160;protected]</a> </p>
                        <a href="cdn-cgi/l/email-protection.html#90e3f1fcf5e3d0f1f1f9e4bee3f1" class="box-btn"> للمراسلة المباشرة </a> 
                     </div>
                  </div>
               </div>
               <div class="row justify-content-center">
                  <div class="col-12 col-md-8">
                     <form action="https://agemedia.sa/contact_us" class="contact-form mt-20" id="contact_us_form">
                        <input type="hidden" name="_token" value="xGpbyi1bg7S0dKzIOK8kd80hN9HbnukZYxRI4uk1"> <input type="text" name="name" class="input" placeholder="الاسم : "> <input type="number" name="phone" class="input" placeholder="رقم الهاتف "> <input type="email" name="email" class="input" placeholder="البريد الالكتروني "> 
                        <textarea class="input" name="message" placeholder="الرسالة"></textarea>
                        <div class="text-end"> <button class="button blue">ارسال</button> </div>
                     </form>
                  </div>
               </div>
            </div>
         </section>
      </div>