      <div class="content">
         <section class="home-cover" style="background-image: url(public/site/img/home-cover0.jpeg);">
            <div class="container">
               <div class="info">
                  <h1 class="title"> شركة إيج ميديا لتقنية المعلومات </h1>
                  <p class="sub-title">شركة سعودية مهمتها الأولى مساعدة العملاء في التطور والنجاح والانتقال للسوق الإلكتروني بمنتهى السرعة والقوة بداية من حجز مكان خاص في سوق المستقبل وحتى وضع خطة تسويقية احترافية تساعدهم على الدخول بين المنافسين بقوة . </p>
                  <div class="btns"> <a href="public/uploads/setting/156802388685.pdf" target="_blank" class="button blue"> <i class="fas icon-fa-file-download"></i> ملف الاعمال </a> <a href="contact" class="button green">اتصل بنا</a> </div>
               </div>
            </div>
         </section>
         <section class="about pt-40 pb-40">
            <div class="container">
               <div class="row align-items-center justify-content-between">
                  <div class="col-12 col-lg-6">
                     <h2 class="title font-25 bold dark-color mb-10"> شركة إيج ميديا لتقنية المعلومات </h2>
                     <p class="info-content font-15 mb-30">إيج ميديا لتقنية المعلومات، إسم رائد فى مجال برمجة وتصميم المواقع، تطبيقات الجوال، المتاجر الإلكترونية، وخدمات التسويق الإلكتروني المتنوعة، في المملكة العربية السعودية، خبرة أكثر من ثلاثة عشر عاماً، تضم أكثر من مئة وخمسون موظف، يسعون دائما للإبداع و التطور المستمر من أجل تحويل أفكاركم الإبداعية إلى واقع مليئ بالنجاحات.</p>
                  </div>
                  <div class="col-12 col-lg-5"> <img loading="lazy" src="public/site/img/about.png" alt=" إيج ميديا للبرمجة" class="img"> </div>
               </div>
            </div>
         </section>
         <section class="services pt-40 pb-20">
            <div class="container">
               <div class="mb-40">
                  <h2 class="section-title"> خدماتنا </h2>
                  <p class="section-subtitle">نقوم بتقديم جميع الخدمات التي تُسرع من الانتقال للسوق الالكتروني باحترافية وقوة بداية من برمجة المواقع وحتى التسويق لها لتحقيق كل ما تريد .</p>
               </div>
               <div class="row justify-content-center">
                  <div class="col-6  col-md-4">
                     <a href="%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%a7%d9%84%d9%85%d9%88%d8%a7%d9%82%d8%b9.html" class="serv-block" title="تصميم المواقع">
                        <div class="serv-img"> <img loading="lazy" alt="تصميم المواقع" src="public/storage/images/services/161096439473238.png"> </div>
                        <p> تصميم المواقع </p>
                        <span>قائمة على نجاح تجربة المستخدم ، احترافية ، متجاوبة مع جميع الأجهزة ، ذات تصميم جذاب مريح للنظر ، سريعة ، متوافقة مع متطلبات محركات البحث .</span> 
                     </a>
                  </div>
                  <div class="col-6  col-md-4">
                     <a href="%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%aa%d8%b7%d8%a8%d9%8a%d9%82%d8%a7%d8%aa-%d8%a7%d9%84%d8%ac%d9%88%d8%a7%d9%84.html" class="serv-block" title="تصميم تطبيقات الجوال">
                        <div class="serv-img"> <img loading="lazy" alt="تصميم تطبيقات الجوال" src="public/storage/images/services/161096922310301.png"> </div>
                        <p> تصميم تطبيقات الجوال </p>
                        <span>متوافقة مع مستخدمي الـ ios والأندرويد ، تدعم أكثر من لغة ، ذات لوحة تحكم سهلة ، سريعة ، متعددة الصفحات بما يخدم فكرة التطبيق .</span> 
                     </a>
                  </div>
                  <div class="col-6  col-md-4">
                     <a href="%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d9%85%d8%aa%d8%ac%d8%b1-%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a.html" class="serv-block" title="تصميم متجر الكتروني">
                        <div class="serv-img"> <img loading="lazy" alt="تصميم متجر الكتروني" src="public/storage/images/services/161096931426570.png"> </div>
                        <p> تصميم متجر الكتروني </p>
                        <span>متعدد الأقسام ، يدعم أكثر من طريقة للدفع ، سهل الاستخدام ، آمن على بيانات المستخدمين ، يدعم أكثر من لغة .</span> 
                     </a>
                  </div>
                  <div class="col-6  col-md-4">
                     <a href="%d8%a7%d9%84%d8%aa%d8%b3%d9%88%d9%8a%d9%82-%d8%a7%d9%84%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a.html" class="serv-block" title="التسويق الالكتروني">
                        <div class="serv-img"> <img loading="lazy" alt="التسويق الالكتروني" src="public/storage/images/services/161217465254162.png"> </div>
                        <p> التسويق الالكتروني </p>
                        <span>بداية من دراسة السوق والمنافسين والوضع الحالي للنشاط التجاري وبدء العمل على تطويره من خلال خطة احترافية طويلة المدى للوصول للهدف المطلوب .</span> 
                     </a>
                  </div>
                  <div class="col-6  col-md-4">
                     <a href="%d8%a7%d9%84%d9%87%d9%88%d9%8a%d8%a9-%d8%a7%d9%84%d8%aa%d8%ac%d8%a7%d8%b1%d9%8a%d8%a9.html" class="serv-block" title="الهوية التجارية">
                        <div class="serv-img"> <img loading="lazy" alt="الهوية التجارية" src="public/storage/images/services/161217539756419.png"> </div>
                        <p> الهوية التجارية </p>
                        <span>هويتك هي كيانك، وصورتك في أذهان عملائك، أصبحت الهويّة الآن بديلًا للاسم والتخصص والوصف، بل من أهم ما يميز النشاط التجاري، ويبرزك بين منافسيك</span> 
                     </a>
                  </div>
                  <div class="col-6  col-md-4">
                     <a href="%d9%81%d9%8a%d8%af%d9%8a%d9%88-%d9%85%d9%88%d8%b4%d9%86-%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83.html" class="serv-block" title="موشن جرافيك">
                        <div class="serv-img"> <img loading="lazy" alt="موشن جرافيك" src="public/storage/images/services/161217553169452.png"> </div>
                        <p> موشن جرافيك </p>
                        <span>تعتبر فيديوهات الموشن جرافيك من أسهل الأدوات التسويقية لعرض فكرة مشروعك/شركتك او التعريف عنه والذي يبقى في أذهان عملائك طوال الوقت.</span> 
                     </a>
                  </div>
               </div>
            </div>
         </section>
         <section class="home-features" style="background-image: url(public/site/img/footer-img.webp);">
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col-12 col-md-3">
                     <div class="feat-box">
                        <div class="img"> <img loading="lazy" src="public/site/img/feature.png" alt=" إيج ميديا للبرمجة"> </div>
                        <p class="title">تواصل مع العملاء</p>
                     </div>
                  </div>
                  <div class="col-12 col-md-3">
                     <div class="feat-box">
                        <div class="img"> <img loading="lazy" src="public/site/img/feature2.png" alt=""> </div>
                        <p class="title">جودة متميزة</p>
                     </div>
                  </div>
                  <div class="col-12 col-md-3">
                     <div class="feat-box">
                        <div class="img"> <img loading="lazy" src="public/site/img/feature3.png" alt=""> </div>
                        <p class="title">نجاح مستمر</p>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="contact-us pt-40">
            <div class="container">
               <div class="mb-40">
                  <h2 class="section-title">اتصل بنا</h2>
                  <p class="section-subtitle">نحن متاحين لخدمتك في أي وقت ، نوفر طرق متعددة للتواصل لاختيار الطريقة الأفضل لك .</p>
               </div>
            </div>
            <div class="map">
               <div class="container">
                  <div class="contact-side">
                     <div class="contact-box green">
                        <p class="info-content">للتواصل عبر الواتساب</p>
                        <div class="down"> <i class="fab icon-fa-whatsapp"></i> <a href="https://api.whatsapp.com/send?phone=966550016001" class="box-btn">0550016001</a> </div>
                     </div>
                     <div class="contact-box blue">
                        <p class="info-content">للإتصال والإستفسار</p>
                        <div class="down"> <i class="fas icon-fa-phone-1"></i> <a href="tel:0550016001" class="box-btn">0550016001</a> </div>
                     </div>
                     <div class="contact-box dark">
                        <p class="info-content">للمراسلة المباشرة</p>
                        <div class="down"> <i class="far icon-fa-envelope"></i> <a href="mailto:sales@agemedia.sa" class="box-btn">sales@agemedia.sa</a> </div>
                     </div>
                     <div class="contact-box dark-blue">
                        <p class="info-content">الوصول للعنوان</p>
                        <div class="down"> <i class="fas icon-fa-map-marked-alt"></i> <a href="https://goo.gl/maps/CbpV7KdDHPScUF7n7" target="_blank" class="box-btn">الوصول للعنوان</a> </div>
                     </div>
                  </div>
               </div>
               <div id="map"></div>
            </div>
            <div class="container cont-form">
               <div class="row">
                  <div class="col-12 col-md-6 pt-10"> <img loading="lazy" src="public/site/img/about.png" alt=" إيج ميديا للبرمجة" class="img-fluid d-block m-auto"> </div>
                  <div class="col-12 col-md-6 pt-40 pb-20">
                     <form action="https://agemedia.sa/contact_us" class="contact-form mt-20" id="contact_us_form">
                        <input type="hidden" name="_token" value="xGpbyi1bg7S0dKzIOK8kd80hN9HbnukZYxRI4uk1"> <input type="text" class="input" name="name" placeholder="الاسم : "> <input type="number" class="input" name="phone" placeholder="رقم الهاتف "> <input type="email" class="input" name="email" placeholder="البريد الالكتروني "> 
                        <textarea class="input" name="message" placeholder="الرسالة"></textarea>
                        <div class="text-end"> <button class="button blue"> ارسال </button> </div>
                     </form>
                  </div>
               </div>
            </div>
         </section>
      </div>
