      <div class="content">
         <section class="inner-head" style="background-image: url(public/site/img/home-cover0.jpeg);">
            <div class="container">
               <h1 class="title">مدونة إيج ميديا</h1>
            </div>
         </section>
         <section class="blogs pt-40 pb-40">
            <div class="container">
               <div class="row">
                  <div class="col-12 col-md-6">
                     <div class="blog-item d-flex">
                        <div class="blog-txt">
                           <a href="%d9%83%d9%8a%d9%81%d9%8a%d8%a9-%d8%a5%d8%af%d8%a7%d8%b1%d8%a9-%d8%a7%d9%84%d9%85%d8%aa%d8%a7%d8%ac%d8%b1-%d8%a7%d9%84%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a%d8%a9-%d9%84%d8%b2%d9%8a%d8%a7%d8%af%d8%a9-%d8%a7%d9%84%d8%ac%d9%85%d9%87%d9%88%d8%b1.html" class="blue-color title" title="كيفية إدارة المتاجر الالكترونية لزيادة الجمهور"> كيفية إدارة المتاجر الالكترونية لزيادة الجمهور </a> 
                           <p class="grey-txt">بعدما تقوم بالانتهاء من الإجراءات التقنية لإنشاء المتجر الإلكتروني وإطلاقه توجد الكثير من الأسئلة حول كيفية إدارة المتاجر الالكترونية ويجب عليك أن تبحث عن إجابة لها:
                              أين يمكنني تخزين المنتجات �
                           </p>
                           <a href="%d9%83%d9%8a%d9%81%d9%8a%d8%a9-%d8%a5%d8%af%d8%a7%d8%b1%d8%a9-%d8%a7%d9%84%d9%85%d8%aa%d8%a7%d8%ac%d8%b1-%d8%a7%d9%84%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a%d8%a9-%d9%84%d8%b2%d9%8a%d8%a7%d8%af%d8%a9-%d8%a7%d9%84%d8%ac%d9%85%d9%87%d9%88%d8%b1.html" class="read-blog green" title="كيفية إدارة المتاجر الالكترونية لزيادة الجمهور">مشاهده التفاصيل</a> 
                        </div>
                        <a href="%d9%83%d9%8a%d9%81%d9%8a%d8%a9-%d8%a5%d8%af%d8%a7%d8%b1%d8%a9-%d8%a7%d9%84%d9%85%d8%aa%d8%a7%d8%ac%d8%b1-%d8%a7%d9%84%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a%d8%a9-%d9%84%d8%b2%d9%8a%d8%a7%d8%af%d8%a9-%d8%a7%d9%84%d8%ac%d9%85%d9%87%d9%88%d8%b1.html" title="كيفية إدارة المتاجر الالكترونية لزيادة الجمهور" class="blog-img"> <img src="public/storage/images/blogs/58001591825048.3249.jpg" alt="كيفية إدارة المتاجر الالكترونية لزيادة الجمهور"> </a> 
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="blog-item d-flex">
                        <div class="blog-txt">
                           <a href="%d8%b4%d8%b1%d9%83%d8%a9-%d8%aa%d8%b3%d9%88%d9%8a%d9%82-%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a.html" class="blue-color title" title="شركة تسويق الكتروني"> شركة تسويق الكتروني </a> 
                           <p class="grey-txt">شركة إيج ميديا هي أفضل شركة تسويق الكتروني على المستوى المحلي والعالمي لأن بها أقوى خدمات التسويق الإلكتروني عبر شبكات التواصل الاجتماعي المختلفة كجزء من العديد من الخدمات المتميزة التي تقدمها في مجال التجار</p>
                           <a href="%d8%b4%d8%b1%d9%83%d8%a9-%d8%aa%d8%b3%d9%88%d9%8a%d9%82-%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a.html" class="read-blog green" title="شركة تسويق الكتروني">مشاهده التفاصيل</a> 
                        </div>
                        <a href="%d8%b4%d8%b1%d9%83%d8%a9-%d8%aa%d8%b3%d9%88%d9%8a%d9%82-%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a.html" title="شركة تسويق الكتروني" class="blog-img"> <img src="public/storage/images/blogs/95381591825778.0852.jpg" alt="شركة تسويق الكتروني"> </a> 
                     </div>
                  </div>
                  <div class="col-md-4 col-12">
                     <div class="fullimg-item-form">
                        <div class="div-content">
                           <h3>اشترك معنا في المدونة</h3>
                           <form action="#" method="post">
                              <input type="hidden" name="_token" value="h4zTwbVphSesgBKAP7VpGCwGj1fXjGBl9DGGmnEt"> 
                              <div class="form-group"> <input type="email" required="" name="email" class="form-control validation" placeholder="البريد الألكتروني"> </div>
                              <button type="submit" class="blue-color" id="btnSubmit"> اشترك الآن </button> 
                           </form>
                           <p>اشترك معنا في المدونة لتحصل علي كل ماهو جديد في المدونة</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-8 col-12">
                     <div class="fullimg-item ">
                        <div class="blog-txt-full">
                           <span class="cat-span">الهوية التجارية</span> 
                           <a href="%c2%a0%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d8%af%d9%8a%d8%b2%d8%a7%d9%8a%d9%86-%d8%a8%d8%a7%d9%84%d8%b1%d9%8a%d8%a7%d8%b6.html">
                              <h6> جرافيك ديزاين بالرياض</h6>
                           </a>
                        </div>
                        <img src="public/storage/images/blogs/58461591825970.1325.jpg" alt=" جرافيك ديزاين بالرياض"> 
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="blog-item d-flex">
                        <div class="blog-txt">
                           <a href="%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%a7%d9%84%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d9%81%d9%8a-%d8%a7%d9%84%d8%b3%d8%b9%d9%88%d8%af%d9%8a%d8%a9.html" class="blue-color title" title="تصميم الجرافيك في السعودية"> تصميم الجرافيك في السعودية </a> 
                           <p class="grey-txt">يوجد العديد من شركات تصميم الجرافيك في السعودية ونحن في شركة إيج ميديا نعد من أفضل وأقدم الشركات في هذا المجال.
                              ولنا سابقة أعمال كبيرة تشهد لنا بما لدينا من أفضل فريق من المصممين والمطورين يعمل على تقديم أفضل تصميم 
                           </p>
                           <a href="%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%a7%d9%84%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d9%81%d9%8a-%d8%a7%d9%84%d8%b3%d8%b9%d9%88%d8%af%d9%8a%d8%a9.html" class="read-blog green" title="تصميم الجرافيك في السعودية">مشاهده التفاصيل</a> 
                        </div>
                        <a href="%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%a7%d9%84%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d9%81%d9%8a-%d8%a7%d9%84%d8%b3%d8%b9%d9%88%d8%af%d9%8a%d8%a9.html" title="تصميم الجرافيك في السعودية" class="blog-img"> <img src="public/storage/images/blogs/59021591826356.1741.jpg" alt="تصميم الجرافيك في السعودية"> </a> 
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="blog-item d-flex">
                        <div class="blog-txt">
                           <a href="%d8%b4%d8%b1%d9%83%d8%a9-%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d8%a8%d8%a7%d9%84%d8%b1%d9%8a%d8%a7%d8%b6.html" class="blue-color title" title="شركة جرافيك بالرياض"> شركة جرافيك بالرياض </a> 
                           <p class="grey-txt">مما لا شك فيه أن شركة جرافيك بالرياض لها تاريخ طويل يمتد لعدة سنوات وهذا أضاف لنا تميزًا على مدى الأعوام الماضية عن باقي الشركات، وذلك من خلال تقديم الخدمات بشكل دقيق ومميز حيث تتسم شركة إيج ميديا بسمة الابتكار والإب</p>
                           <a href="%d8%b4%d8%b1%d9%83%d8%a9-%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d8%a8%d8%a7%d9%84%d8%b1%d9%8a%d8%a7%d8%b6.html" class="read-blog green" title="شركة جرافيك بالرياض">مشاهده التفاصيل</a> 
                        </div>
                        <a href="%d8%b4%d8%b1%d9%83%d8%a9-%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d8%a8%d8%a7%d9%84%d8%b1%d9%8a%d8%a7%d8%b6.html" title="شركة جرافيك بالرياض" class="blog-img"> <img src="public/storage/images/blogs/54161591912458.7636.png" alt="شركة جرافيك بالرياض"> </a> 
                     </div>
                  </div>
                  <div class="col-md-8 col-12">
                     <div class="fullimg-item ">
                        <div class="blog-txt-full">
                           <span class="cat-span">التسويق الالكتروني</span> 
                           <a href="%d8%ae%d8%af%d9%85%d8%a7%d8%aa-%d9%85%d9%88%d8%a7%d9%82%d8%b9-%d8%a7%d9%84%d8%aa%d9%88%d8%a7%d8%b5%d9%84-%d8%a7%d9%84%d8%a7%d8%ac%d8%aa%d9%85%d8%a7%d8%b9%d9%8a.html">
                              <h6>خدمات مواقع التواصل الاجتماعي</h6>
                           </a>
                        </div>
                        <img src="public/storage/images/blogs/90711591912648.4181.png" alt="خدمات مواقع التواصل الاجتماعي"> 
                     </div>
                  </div>
                  <div class="col-md-4 col-12">
                     <div class="fullimg-item-form adv-bg"> <a href="%d8%b9%d8%b1%d9%88%d8%b6%d9%86%d8%a7.html"> <img src="public/site/img/offers-aait.jpg" alt="offers" class="w-100"> </a> </div>
                  </div>
               </div>
               <div class="text-center mt-20"> <a href="%d8%a7%d9%84%d9%85%d9%82%d8%a7%d9%84%d8%a7%d8%aa.html" class="button blue">عرض المزيد من المقالات</a> </div>
            </div>
         </section>
         <section class="our-works pt-40 pb-40">
            <div class="container">
               <div class="mb-40">
                  <h2 class="section-title">اعمالنا</h2>
               </div>
               <div class="owl-carousel slider-2">
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%b1%d9%88%d8%af%d9%8a%d9%86%d8%a7.html" class="work-box" target="_blank" title="تطبيق رودينا">
                        <div class="img"> <img src="public/storage/images/works/1613311408_9025.jpg" alt="تطبيق رودينا"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق رودينا </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%ac%d8%a7%d9%8a.html" class="work-box" target="_blank" title="تطبيق جاي">
                        <div class="img"> <img src="public/storage/images/works/1613313147_3330.png" alt="تطبيق جاي"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق جاي </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%a7%d8%aa%d8%b5%d9%84-%d9%86%d8%b5%d9%84.html" class="work-box" target="_blank" title="تطبيق اتصل نصل">
                        <div class="img"> <img src="public/storage/images/works/1613313805_9902.jpg" alt="تطبيق اتصل نصل"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق اتصل نصل </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%b4%d8%ba%d9%84%d9%86%d9%8a.html" class="work-box" target="_blank" title="تطبيق شغلني">
                        <div class="img"> <img src="public/storage/images/works/1613317013_5203.jpg" alt="تطبيق شغلني"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق شغلني </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-score-567.html" class="work-box" target="_blank" title="تطبيق score 567">
                        <div class="img"> <img src="public/storage/images/works/1613378383_4885.png" alt="تطبيق score 567"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق score 567 </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%ae%d8%af%d9%85%d8%a7%d8%aa-%d8%a7%d9%88%d9%86%d9%84%d8%a7%d9%8a%d9%86.html" class="work-box" target="_blank" title="تطبيق خدمات اونلاين">
                        <div class="img"> <img src="public/storage/images/works/1613379249_6174.jpg" alt="تطبيق خدمات اونلاين"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق خدمات اونلاين </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%b9%d8%b2%d9%83.html" class="work-box" target="_blank" title="تطبيق عزك">
                        <div class="img"> <img src="public/storage/images/works/1613380164_3045.png" alt="تطبيق عزك"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق عزك </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%ad%d8%b1%d8%a7%d8%ac-%d8%a8%d9%8a%d8%aa%d8%b3.html" class="work-box" target="_blank" title="تطبيق حراج بيتس">
                        <div class="img"> <img src="public/storage/images/works/1613381174_2910.jpg" alt="تطبيق حراج بيتس"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق حراج بيتس </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%a8%d8%b1%d9%88%d9%82.html" class="work-box" target="_blank" title="تطبيق بروق">
                        <div class="img"> <img src="public/storage/images/works/1613382177_2310.png" alt="تطبيق بروق"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق بروق </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%b4%d9%87%d9%8a%d8%b1.html" class="work-box" target="_blank" title="تطبيق شهير">
                        <div class="img"> <img src="public/storage/images/works/1613383730_8003.png" alt="تطبيق شهير"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق شهير </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%a7%d9%86%d8%b9%d8%a7%d9%85%d9%8a.html" class="work-box" target="_blank" title="تطبيق انعامي">
                        <div class="img"> <img src="public/storage/images/works/1613384612_4635.jpg" alt="تطبيق انعامي"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق انعامي </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%a8%d9%8a%d8%aa-%d8%a7%d9%84%d9%82%d8%b7%d8%b7.html" class="work-box" target="_blank" title="تطبيق بيت القطط">
                        <div class="img"> <img src="public/storage/images/works/1613386597_5151.jpg" alt="تطبيق بيت القطط"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق بيت القطط </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d9%81%d9%88%d9%88.html" class="work-box" target="_blank" title="تطبيق فوو">
                        <div class="img"> <img src="public/storage/images/works/1613387280_9042.png" alt="تطبيق فوو"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق فوو </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%ac%d8%b1%d8%a7%d8%a8-%d8%a7%d9%84%d8%ad%d8%a7%d9%88%d9%8a.html" class="work-box" target="_blank" title="تطبيق جراب الحاوي">
                        <div class="img"> <img src="public/storage/images/works/1613388094_3832.png" alt="تطبيق جراب الحاوي"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق جراب الحاوي </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%ad%d8%b1%d8%a7%d8%ac-%d8%a7%d9%84%d9%85%d9%85%d9%84%d9%83%d8%a9-%d9%84%d9%84%d9%85%d9%88%d8%a7%d8%b4%d9%8a.html" class="work-box" target="_blank" title="تطبيق حراج المملكة للمواشي">
                        <div class="img"> <img src="public/storage/images/works/1613389203_4864.png" alt="تطبيق حراج المملكة للمواشي"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق حراج المملكة للمواشي </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%a8%d8%b1%d8%a7%d9%8a%d8%b3-%d9%84%d9%88.html" class="work-box" target="_blank" title="تطبيق برايس لو">
                        <div class="img"> <img src="public/storage/images/works/1613390100_5737.png" alt="تطبيق برايس لو"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق برايس لو </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%ac%d9%85%d9%8a%d8%b9%d8%a9-%d8%a7%d9%84%d8%a3%d8%b3%d8%b1-%d8%a7%d9%84%d9%85%d9%86%d8%aa%d8%ac%d8%a9-%d8%a7%d9%84%d8%a3%d9%87%d9%84%d9%8a%d8%a9.html" class="work-box" target="_blank" title="تطبيق جميعة الأسر المنتجة الأهلية">
                        <div class="img"> <img src="public/storage/images/works/1613391358_1562.png" alt="تطبيق جميعة الأسر المنتجة الأهلية"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق جميعة الأسر المنتجة الأهلية </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%a7%d9%84%d9%81%d9%88%d8%b2%d8%a7%d9%86.html" class="work-box" target="_blank" title="تطبيق الفوزان">
                        <div class="img"> <img src="public/storage/images/works/1613392029_3900.png" alt="تطبيق الفوزان"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق الفوزان </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%ad%d9%8a%d8%a7%d9%83.html" class="work-box" target="_blank" title="تطبيق حياك">
                        <div class="img"> <img src="public/storage/images/works/1613397099_7611.png" alt="تطبيق حياك"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق حياك </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%b1%d9%8a%d8%b4.html" class="work-box" target="_blank" title="تطبيق ريش">
                        <div class="img"> <img src="public/storage/images/works/1613397557_5425.jpg" alt="تطبيق ريش"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق ريش </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%b9%d9%84%d9%89-%d8%a7%d9%84%d8%b3%d8%b1%d9%8a%d8%b9.html" class="work-box" target="_blank" title="تطبيق على السريع">
                        <div class="img"> <img src="public/storage/images/works/1613397972_6485.png" alt="تطبيق على السريع"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق على السريع </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%b0%d8%a8%d8%a7%d8%a6%d8%ad-%d8%a7%d9%84%d9%8a%d9%88%d9%85.html" class="work-box" target="_blank" title="تطبيق ذبائح اليوم">
                        <div class="img"> <img src="public/storage/images/works/1613398428_6868.png" alt="تطبيق ذبائح اليوم"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق ذبائح اليوم </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d9%82%d8%af%d8%b1%d9%87%d8%a7.html" class="work-box" target="_blank" title="تطبيق قدرها">
                        <div class="img"> <img src="public/storage/images/works/1613398793_9717.png" alt="تطبيق قدرها"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق قدرها </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d9%88%d9%82%d8%b9-%d8%aa%d8%b9%d8%b1%d9%8a%d9%81%d9%8a-%d8%a7%d9%84%d8%ac%d9%88%d8%af%d8%a9.html" class="work-box" target="_blank" title="موقع تعريفي الجودة">
                        <div class="img"> <img src="public/storage/images/works/1613644197_4809.jpg" alt="موقع تعريفي الجودة"> </div>
                        <p class="title"></p>
                        <p class="info-content"> موقع تعريفي الجودة </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d9%88%d9%82%d8%b9-%d8%aa%d8%b9%d8%b1%d9%8a%d9%81%d9%8a-%d9%85%d8%b5%d9%86%d8%b9-%d9%86%d8%a8%d8%b1%d8%a7%d8%b3-%d8%a7%d9%84%d9%85%d8%b9%d8%a7%d8%af%d9%86.html" class="work-box" target="_blank" title="موقع تعريفي مصنع نبراس المعادن">
                        <div class="img"> <img src="public/storage/images/works/1613646118_6948.jpg" alt="موقع تعريفي مصنع نبراس المعادن"> </div>
                        <p class="title"></p>
                        <p class="info-content"> موقع تعريفي مصنع نبراس المعادن </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d8%b7%d8%a7%d8%b9%d9%85-%d8%a7%d9%88%d8%a7%d9%85%d8%b1-%d8%a7%d9%84%d8%b4%d8%a8%d9%83%d8%a9.html" class="work-box" target="_blank" title="مطاعم إيج ميديا">
                        <div class="img"> <img src="public/storage/images/works/1613898615_5229.png" alt="مطاعم إيج ميديا"> </div>
                        <p class="title"></p>
                        <p class="info-content"> مطاعم إيج ميديا </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d9%88%d9%82%d8%b9-%d9%86%d9%85%d8%aa%d8%a7%d8%b2.html" class="work-box" target="_blank" title="موقع نمتاز">
                        <div class="img"> <img src="public/storage/images/works/1613909993_5518.png" alt="موقع نمتاز"> </div>
                        <p class="title"></p>
                        <p class="info-content"> موقع نمتاز </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d9%85%d9%88%d9%8a%d9%86%d8%a7%d8%aa-%d8%a3%d9%88%d8%a7%d9%85%d8%b1.html" class="work-box" target="_blank" title="تموينات أوامر">
                        <div class="img"> <img src="public/storage/images/works/1613917186_2809.jpg" alt="تموينات أوامر"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تموينات أوامر </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%aa%d8%a7%d9%83%d8%b3%d9%8a-%d8%a3%d9%88%d8%a7%d9%85%d8%b1.html" class="work-box" target="_blank" title="تطبيق تاكسي أوامر">
                        <div class="img"> <img src="public/storage/images/works/1613918353_5378.jpg" alt="تطبيق تاكسي أوامر"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق تاكسي أوامر </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d8%b1%d8%b3%d9%88%d9%84-%d8%a3%d9%88%d8%a7%d9%85%d8%b1.html" class="work-box" target="_blank" title="مرسول أوامر">
                        <div class="img"> <img src="public/storage/images/works/1613919361_8428.png" alt="مرسول أوامر"> </div>
                        <p class="title"></p>
                        <p class="info-content"> مرسول أوامر </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%a7%d9%84%d9%87%d8%af%d9%81-%d9%84%d9%84%d8%b0%d8%a8%d8%a7%d8%a6%d8%ad.html" class="work-box" target="_blank" title="تطبيق الهدف للذبائح">
                        <div class="img"> <img src="public/storage/images/works/1614169839_4523.jpg" alt="تطبيق الهدف للذبائح"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق الهدف للذبائح </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%a7%d9%86%d8%aa%d8%a7%d8%ac-%d9%86%d8%ac%d8%af.html" class="work-box" target="_blank" title="تطبيق انتاج نجد">
                        <div class="img"> <img src="public/storage/images/works/1614174151_1208.jpg" alt="تطبيق انتاج نجد"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق انتاج نجد </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%a7%d9%84%d8%ae%d8%b1%d9%88%d9%81-%d8%a7%d9%84%d8%a8%d8%b1%d9%8a.html" class="work-box" target="_blank" title="تطبيق الخروف البري">
                        <div class="img"> <img src="public/storage/images/works/1614175553_6704.jpg" alt="تطبيق الخروف البري"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق الخروف البري </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d8%aa%d8%ac%d8%b1-%d9%85%d8%a7%d8%b1%d9%81%d9%8a.html" class="work-box" target="_blank" title="متجر مارفي">
                        <div class="img"> <img src="public/storage/images/works/1614244174_1688.jpg" alt="متجر مارفي"> </div>
                        <p class="title"></p>
                        <p class="info-content"> متجر مارفي </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d9%88%d9%82%d8%b9-%d8%ac%d8%b1%d8%a7%d8%a8-%d8%a7%d9%84%d8%ad%d8%a7%d9%88%d9%8a.html" class="work-box" target="_blank" title="موقع جراب الحاوي">
                        <div class="img"> <img src="public/storage/images/works/1614253782_5899.png" alt="موقع جراب الحاوي"> </div>
                        <p class="title"></p>
                        <p class="info-content"> موقع جراب الحاوي </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d9%88%d8%b4%d9%86-%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%b1%d9%88%d8%af%d9%8a%d9%86%d8%a7.html" class="work-box" target="_blank" title="موشن جرافيك تطبيق رودينا">
                        <div class="img"> <img src="public/storage/images/works/1614254898_2022.jpg" alt="موشن جرافيك تطبيق رودينا"> </div>
                        <p class="title"></p>
                        <p class="info-content"> موشن جرافيك تطبيق رودينا </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d9%88%d8%b4%d9%86-%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%a3%d8%ac%d9%8a%d8%a8%d8%a9.html" class="work-box" target="_blank" title="موشن جرافيك تطبيق أجيبة">
                        <div class="img"> <img src="public/storage/images/works/1614260311_7628.jpg" alt="موشن جرافيك تطبيق أجيبة"> </div>
                        <p class="title"></p>
                        <p class="info-content"> موشن جرافيك تطبيق أجيبة </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d9%88%d8%b4%d9%86-%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%b5%d8%a7%d9%84%d9%88%d9%86%d8%a7%d8%aa.html" class="work-box" target="_blank" title="موشن جرافيك تطبيق صالونات">
                        <div class="img"> <img src="public/storage/images/works/1614260879_4304.jpg" alt="موشن جرافيك تطبيق صالونات"> </div>
                        <p class="title"></p>
                        <p class="info-content"> موشن جرافيك تطبيق صالونات </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d9%88%d8%b4%d9%86-%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d9%85%d8%b3%d8%b1%d9%91%d9%87.html" class="work-box" target="_blank" title="موشن جرافيك تطبيق مسرّه">
                        <div class="img"> <img src="public/storage/images/works/1614261612_1906.jpg" alt="موشن جرافيك تطبيق مسرّه"> </div>
                        <p class="title"></p>
                        <p class="info-content"> موشن جرافيك تطبيق مسرّه </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d9%88%d8%b4%d9%86-%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d8%ac%d9%85%d8%b9%d9%8a%d8%a9-%d8%a7%d9%84%d8%ac%d8%b4%d8%a9-%d8%a8%d8%a7%d9%84%d8%a5%d8%ad%d8%b3%d8%a7%d8%a1.html" class="work-box" target="_blank" title="موشن جرافيك جمعية الجشة بالإحساء">
                        <div class="img"> <img src="public/storage/images/works/1614262530_8692.jpg" alt="موشن جرافيك جمعية الجشة بالإحساء"> </div>
                        <p class="title"></p>
                        <p class="info-content"> موشن جرافيك جمعية الجشة بالإحساء </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d9%88%d9%82%d8%b9-%d9%81%d9%87%d8%af-%d8%a8%d9%86-%d9%85%d8%ad%d9%85%d8%af-%d8%a8%d8%a7%d8%b1%d8%a8%d8%a7%d8%b9-%d9%84%d9%84%d9%85%d8%ad%d8%a7%d9%85%d8%a7%d8%a9.html" class="work-box" target="_blank" title="موقع فهد بن محمد بارباع للمحاماة">
                        <div class="img"> <img src="public/storage/images/works/1614265174_9649.jpg" alt="موقع فهد بن محمد بارباع للمحاماة"> </div>
                        <p class="title"></p>
                        <p class="info-content"> موقع فهد بن محمد بارباع للمحاماة </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d9%85%d9%88%d9%82%d8%b9-%d8%ad%d8%b1%d8%a7%d8%ac-%d8%a7%d9%84%d9%83%d9%84.html" class="work-box" target="_blank" title="تصميم موقع حراج الكل">
                        <div class="img"> <img src="public/storage/images/works/1614508875_4907.png" alt="تصميم موقع حراج الكل"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تصميم موقع حراج الكل </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%ad%d8%b1%d8%a7%d8%ac-%d9%86%d8%ac%d8%b1%d8%a7%d9%86-%d9%84%d9%84%d8%b9%d9%82%d8%a7%d8%b1%d8%a7%d8%aa.html" class="work-box" target="_blank" title="حراج نجران للعقارات">
                        <div class="img"> <img src="public/storage/images/works/1614513315_9655.jpg" alt="حراج نجران للعقارات"> </div>
                        <p class="title"></p>
                        <p class="info-content"> حراج نجران للعقارات </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d8%aa%d8%ac%d8%b1-%d9%87%d9%86%d8%af%d8%a7%d9%85%d8%b2.html" class="work-box" target="_blank" title="متجر هندامز">
                        <div class="img"> <img src="public/storage/images/works/1614585725_9200.jpg" alt="متجر هندامز"> </div>
                        <p class="title"></p>
                        <p class="info-content"> متجر هندامز </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d8%aa%d8%ac%d8%b1-%d9%87%d8%a7%d9%84%d9%88%d9%88.html" class="work-box" target="_blank" title="متجر هالوو">
                        <div class="img"> <img src="public/storage/images/works/1614588622_1564.jpg" alt="متجر هالوو"> </div>
                        <p class="title"></p>
                        <p class="info-content"> متجر هالوو </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d8%aa%d8%ac%d8%b1-%d8%aa%d8%b3%d9%88%d9%82.html" class="work-box" target="_blank" title="متجر تسوق">
                        <div class="img"> <img src="public/storage/images/works/1614590331_7490.jpg" alt="متجر تسوق"> </div>
                        <p class="title"></p>
                        <p class="info-content"> متجر تسوق </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%b0%d8%a8%d8%a7%d8%a6%d8%ad-%d8%a7%d9%84%d8%b4%d8%a8%d9%83%d8%a9.html" class="work-box" target="_blank" title="تطبيق ذبائح الشبكة">
                        <div class="img"> <img src="public/storage/images/works/1614686310_5359.jpg" alt="تطبيق ذبائح الشبكة"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق ذبائح الشبكة </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d8%b4%d8%a7%d8%ba%d9%84-%d8%a3%d9%88%d8%a7%d9%85%d8%b1.html" class="work-box" target="_blank" title="مشاغل أوامر">
                        <div class="img"> <img src="public/storage/images/works/1614694867_6486.jpg" alt="مشاغل أوامر"> </div>
                        <p class="title"></p>
                        <p class="info-content"> مشاغل أوامر </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%ad%d8%b1%d8%a7%d8%ac-%d8%a3%d9%88%d8%a7%d9%85%d8%b1-%d8%a7%d9%84%d9%85%d8%b7%d9%88%d8%b1.html" class="work-box" target="_blank" title="حراج أوامر المطور">
                        <div class="img"> <img src="public/storage/images/works/1614849200_8324.jpg" alt="حراج أوامر المطور"> </div>
                        <p class="title"></p>
                        <p class="info-content"> حراج أوامر المطور </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%ad%d8%b1%d8%a7%d8%ac-%d8%a3%d9%88%d8%a7%d9%85%d8%b1.html" class="work-box" target="_blank" title="حراج أوامر">
                        <div class="img"> <img src="public/storage/images/works/1614850003_9042.jpg" alt="حراج أوامر"> </div>
                        <p class="title"></p>
                        <p class="info-content"> حراج أوامر </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%a7%d9%84%d9%84%d9%88%d8%ac%d9%88.html" class="work-box" target="_blank" title="تصميم اللوجو">
                        <div class="img"> <img src="public/storage/images/works/1615191233_8088.jpg" alt="تصميم اللوجو"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تصميم اللوجو </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%a7%d9%84%d9%84%d9%88%d8%ac%d9%88-%d9%84%d9%80-%d8%b3%d9%85%d8%b1-%d8%b3%d8%a7%d9%84%d9%85.html" class="work-box" target="_blank" title="تصميم اللوجو لـ سمر سالم">
                        <div class="img"> <img src="public/storage/images/works/1615198107_9198.jpg" alt="تصميم اللوجو لـ سمر سالم"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تصميم اللوجو لـ سمر سالم </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%b5%d8%a7%d9%84%d9%88%d9%86%d8%a7%d8%aa.html" class="work-box" target="_blank" title="تطبيق صالونات">
                        <div class="img"> <img src="public/storage/images/works/1615202557_2702.jpg" alt="تطبيق صالونات"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق صالونات </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d9%85%d9%82%d8%b5%d9%8a.html" class="work-box" target="_blank" title="تطبيق مقصي">
                        <div class="img"> <img src="public/storage/images/works/1615217495_4228.jpg" alt="تطبيق مقصي"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق مقصي </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%b5%d8%a7%d9%84%d9%88%d9%86.html" class="work-box" target="_blank" title="تطبيق صالون">
                        <div class="img"> <img src="public/storage/images/works/1615278690_2749.jpg" alt="تطبيق صالون"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق صالون </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%a7%d9%84%d9%84%d9%88%d8%ac%d9%88-%d9%84%d9%80-%d9%82%d8%a7%d8%b9%d8%af%d8%a9-%d8%a7%d9%84%d8%a7%d9%86%d8%aa%d8%a7%d8%ac-%d8%a7%d9%84%d8%b9%d9%84%d9%85%d9%8a.html" class="work-box" target="_blank" title="تصميم اللوجو لـ قاعدة الانتاج العلمي">
                        <div class="img"> <img src="public/storage/images/works/1615282456_9740.jpg" alt="تصميم اللوجو لـ قاعدة الانتاج العلمي"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تصميم اللوجو لـ قاعدة الانتاج العلمي </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%ad%d8%b1%d8%a7%d8%ac-%d8%b9%d9%84%d8%a7%d8%a1-%d8%a7%d9%84%d8%af%d9%8a%d9%86.html" class="work-box" target="_blank" title="حراج علاء الدين">
                        <div class="img"> <img src="public/storage/images/works/1615711014_8135.png" alt="حراج علاء الدين"> </div>
                        <p class="title"></p>
                        <p class="info-content"> حراج علاء الدين </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%ad%d8%b1%d8%a7%d8%ac-%d8%a7%d8%b4%d8%aa%d8%b1%d9%8a%d8%aa.html" class="work-box" target="_blank" title="حراج اشتريت">
                        <div class="img"> <img src="public/storage/images/works/1615713218_8836.png" alt="حراج اشتريت"> </div>
                        <p class="title"></p>
                        <p class="info-content"> حراج اشتريت </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d9%84%d9%88%d8%ac%d9%88-%d9%83%d8%b1%d9%8a%d8%a7%d8%aa%d9%88%d9%81%d9%8a%d8%a7.html" class="work-box" target="_blank" title="تصميم لوجو كرياتوفيا">
                        <div class="img"> <img src="public/storage/images/works/1615732691_4961.png" alt="تصميم لوجو كرياتوفيا"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تصميم لوجو كرياتوفيا </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-Golden-Hand.html" class="work-box" target="_blank" title="تطبيق Golden Hand">
                        <div class="img"> <img src="public/storage/images/works/1615797633_2294.jpg" alt="تطبيق Golden Hand"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق Golden Hand </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%a7%d9%84%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d9%84%d9%80%d9%85%d8%b4%d8%b1%d9%88%d8%b9-Fruits-juices.html" class="work-box" target="_blank" title="تصميم الجرافيك لـمشروع Fruits juices">
                        <div class="img"> <img src="public/storage/images/works/1615801869_4800.jpg" alt="تصميم الجرافيك لـمشروع Fruits juices"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تصميم الجرافيك لـمشروع Fruits juices </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%a7%d9%84%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d9%84%d9%85%d8%b4%d8%b1%d9%88%d8%b9-%d8%ad%d8%af%d9%8a%d9%82%d8%aa%d9%8a.html" class="work-box" target="_blank" title="تصميم الجرافيك لمشروع حديقتي">
                        <div class="img"> <img src="public/storage/images/works/1615805540_3556.jpg" alt="تصميم الجرافيك لمشروع حديقتي"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تصميم الجرافيك لمشروع حديقتي </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%a7%d9%84%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d9%84%d9%85%d8%b4%d8%b1%d9%88%d8%b9-%d8%a3%d8%ac%d9%8a%d8%a8%d8%a9.html" class="work-box" target="_blank" title="تصميم الجرافيك لمشروع أجيبة">
                        <div class="img"> <img src="public/storage/images/works/1615809338_5873.jpg" alt="تصميم الجرافيك لمشروع أجيبة"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تصميم الجرافيك لمشروع أجيبة </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d9%85%d9%86%d8%aa%d8%ac%d8%a9.html" class="work-box" target="_blank" title="تطبيق منتجة">
                        <div class="img"> <img src="public/storage/images/works/1615885668_1296.jpg" alt="تطبيق منتجة"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق منتجة </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d9%84%d9%81%d9%8a%d9%81.html" class="work-box" target="_blank" title="تطبيق لفيف">
                        <div class="img"> <img src="public/storage/images/works/1615888119_8670.jpg" alt="تطبيق لفيف"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق لفيف </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%a7%d9%84%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83-%d9%84%d9%80-%d9%85%d8%b4%d8%b1%d9%88%d8%b9-caffitaly.html" class="work-box" target="_blank" title="تصميم الجرافيك لـ مشروع caffitaly">
                        <div class="img"> <img src="public/storage/images/works/1616662628_8399.jpg" alt="تصميم الجرافيك لـ مشروع caffitaly"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تصميم الجرافيك لـ مشروع caffitaly </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d9%82%d9%88%d8%a7%d9%81%d9%84.html" class="work-box" target="_blank" title="تطبيق قوافل">
                        <div class="img"> <img src="public/storage/images/works/1617116453_1450.jpg" alt="تطبيق قوافل"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق قوافل </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%b9%d8%b5%d9%81%d9%88%d8%b1.html" class="work-box" target="_blank" title="تطبيق عصفور">
                        <div class="img"> <img src="public/storage/images/works/1617180753_4113.jpg" alt="تطبيق عصفور"> </div>
                        <p class="title"></p>
                        <p class="info-content"> تطبيق عصفور </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%af%d8%b1%d8%a7%d8%b3%d8%a9-%d8%a7%d9%84%d8%b4%d8%b1%d9%8a%d8%ad%d8%a9-%d8%a7%d9%84%d9%85%d8%b3%d8%aa%d9%87%d8%af%d9%81%d8%a9-%d9%84%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%aa%d9%88%d8%b5%d9%8a%d9%84-%d8%b7%d9%84%d8%a8%d8%a7%d8%aa.html" class="work-box" target="_blank" title="دراسة الشريحة المستهدفة لتطبيق توصيل طلبات">
                        <div class="img"> <img src="public/storage/images/works/1617870977_2836.jpg" alt="دراسة الشريحة المستهدفة لتطبيق توصيل طلبات"> </div>
                        <p class="title"></p>
                        <p class="info-content"> دراسة الشريحة المستهدفة لتطبيق توصيل طلبات </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d8%a5%d9%86%d8%b4%d8%a7%d8%a1-%d8%ad%d9%85%d9%84%d8%a7%d8%aa-%d8%a5%d9%86%d8%b3%d8%aa%d9%82%d8%b1%d8%a7%d9%85.html" class="work-box" target="_blank" title="إنشاء حملات إنستقرام">
                        <div class="img"> <img src="public/storage/images/works/1618776208_7033.jpg" alt="إنشاء حملات إنستقرام"> </div>
                        <p class="title"></p>
                        <p class="info-content"> إنشاء حملات إنستقرام </p>
                     </a>
                  </div>
                  <div class="item">
                     <a href="portfolio/%d9%85%d9%88%d9%82%d8%b9-%d8%aa%d8%b9%d8%b1%d9%8a%d9%81%d9%8a-%d8%aa%d8%b7%d8%a8%d9%8a%d9%82-%d8%aa%d9%88%d8%b5%d9%8a%d9%84-%d8%ad%d9%8a%d8%a7%d9%83.html" class="work-box" target="_blank" title="موقع تعريفي تطبيق توصيل حياك">
                        <div class="img"> <img src="public/storage/images/works/1618913370_3535.jpg" alt="موقع تعريفي تطبيق توصيل حياك"> </div>
                        <p class="title"></p>
                        <p class="info-content"> موقع تعريفي تطبيق توصيل حياك </p>
                     </a>
                  </div>
               </div>
            </div>
         </section>
         <section class="contact-us pt-40 pb-40 blue-bg">
            <div class="container">
               <div class="mb-40">
                  <h2 class="section-title"> اتصل بنا </h2>
                  <p class="section-subtitle"> نتشرف بخدمتكم وتواصلكم معنا </p>
               </div>
               <div class="row">
                  <div class="col-12 col-md-4">
                     <div class="contact-box green">
                        <i class="fab icon-fa-whatsapp"></i> 
                        <p class="title dark-color bold font-15 mb-10"> للتواصل عبر الواتساب </p>
                        <p class="info-content"> 0550016001 </p>
                        <a href="https://api.whatsapp.com/send?phone=966550016001" class="box-btn"> للتواصل عبر الواتساب </a> 
                     </div>
                  </div>
                  <div class="col-12 col-md-4">
                     <div class="contact-box blue">
                        <i class="fas icon-fa-phone-1"></i> 
                        <p class="title dark-color bold font-15 mb-10"> للإتصال والإستفسار </p>
                        <p class="info-content"> 0550016001 </p>
                        <a href="tel:0550016001" class="box-btn"> اتصل بنا </a> 
                     </div>
                  </div>
                  <div class="col-12 col-md-4">
                     <div class="contact-box dark">
                        <i class="far icon-fa-envelope"></i> 
                        <p class="title dark-color bold font-15 mb-10"> للمراسلة المباشرة </p>
                        <p class="info-content"> <a href="cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="b5c6d4d9d0c6f5d4d4dcc19bc6d4">[email&#160;protected]</a> </p>
                        <a href="cdn-cgi/l/email-protection.html#a4d7c5c8c1d7e4c5c5cdd08ad7c5" class="box-btn"> للمراسلة المباشرة </a> 
                     </div>
                  </div>
               </div>
               <div class="row justify-content-center">
                  <div class="col-12 col-md-8">
                     <form action="https://agemedia.sa/contact_us" class="contact-form mt-20" id="contact_us_form">
                        <input type="hidden" name="_token" value="xGpbyi1bg7S0dKzIOK8kd80hN9HbnukZYxRI4uk1"> <input type="text" name="name" class="input" placeholder="الاسم : "> <input type="number" name="phone" class="input" placeholder="رقم الهاتف "> <input type="email" name="email" class="input" placeholder="البريد الالكتروني "> 
                        <textarea class="input" name="message" placeholder="الرسالة"></textarea>
                        <div class="text-end"> <button class="button blue">ارسال</button> </div>
                     </form>
                  </div>
               </div>
            </div>
         </section>
      </div>
