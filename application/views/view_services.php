      <div class="content">
         <section class="inner-head" style="background-image: url(public/site/img/home-cover0.jpeg);">
            <div class="container">
               <h1 class="title">خدماتنا</h1>
            </div>
         </section>
         <section class="services pt-40 pb-40">
            <div class="container">
               <div class="row">
                  <div class="col-12 col-sm-6 col-md-4">
                     <div class="service-box">
                        <div class="img"> <img src="public/storage/images/services/161096439473238.png" alt="تصميم المواقع"> </div>
                        <div class="info">
                           <p class="info-content">قائمة على نجاح تجربة المستخدم ، احترافية ، متجاوبة مع جميع الأجهزة ، ذات تصميم جذاب مريح للنظر ، سريعة ، متوافقة مع متطلبات محركات البحث .</p>
                           <a href="%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%a7%d9%84%d9%85%d9%88%d8%a7%d9%82%d8%b9.html" class="button blue-reverce" title="تصميم المواقع">تصميم المواقع</a> 
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4">
                     <div class="service-box">
                        <div class="img"> <img src="public/storage/images/services/161096922310301.png" alt="تصميم تطبيقات الجوال"> </div>
                        <div class="info">
                           <p class="info-content">متوافقة مع مستخدمي الـ ios والأندرويد ، تدعم أكثر من لغة ، ذات لوحة تحكم سهلة ، سريعة ، متعددة الصفحات بما يخدم فكرة التطبيق .</p>
                           <a href="%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%aa%d8%b7%d8%a8%d9%8a%d9%82%d8%a7%d8%aa-%d8%a7%d9%84%d8%ac%d9%88%d8%a7%d9%84.html" class="button blue-reverce" title="تصميم تطبيقات الجوال">تصميم تطبيقات الجوال</a> 
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4">
                     <div class="service-box">
                        <div class="img"> <img src="public/storage/images/services/161096931426570.png" alt="تصميم متجر الكتروني"> </div>
                        <div class="info">
                           <p class="info-content">متعدد الأقسام ، يدعم أكثر من طريقة للدفع ، سهل الاستخدام ، آمن على بيانات المستخدمين ، يدعم أكثر من لغة .</p>
                           <a href="%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d9%85%d8%aa%d8%ac%d8%b1-%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a.html" class="button blue-reverce" title="تصميم متجر الكتروني">تصميم متجر الكتروني</a> 
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4">
                     <div class="service-box">
                        <div class="img"> <img src="public/storage/images/services/161217465254162.png" alt="التسويق الالكتروني"> </div>
                        <div class="info">
                           <p class="info-content">بداية من دراسة السوق والمنافسين والوضع الحالي للنشاط التجاري وبدء العمل على تطويره من خلال خطة احترافية طويلة المدى للوصول للهدف المطلوب .</p>
                           <a href="%d8%a7%d9%84%d8%aa%d8%b3%d9%88%d9%8a%d9%82-%d8%a7%d9%84%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a.html" class="button blue-reverce" title="التسويق الالكتروني">التسويق الالكتروني</a> 
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4">
                     <div class="service-box">
                        <div class="img"> <img src="public/storage/images/services/161217539756419.png" alt="الهوية التجارية"> </div>
                        <div class="info">
                           <p class="info-content">هويتك هي كيانك، وصورتك في أذهان عملائك، أصبحت الهويّة الآن بديلًا للاسم والتخصص والوصف، بل من أهم ما يميز النشاط التجاري، ويبرزك بين منافسيك</p>
                           <a href="%d8%a7%d9%84%d9%87%d9%88%d9%8a%d8%a9-%d8%a7%d9%84%d8%aa%d8%ac%d8%a7%d8%b1%d9%8a%d8%a9.html" class="button blue-reverce" title="الهوية التجارية">الهوية التجارية</a> 
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4">
                     <div class="service-box">
                        <div class="img"> <img src="public/storage/images/services/161217553169452.png" alt="موشن جرافيك"> </div>
                        <div class="info">
                           <p class="info-content">تعتبر فيديوهات الموشن جرافيك من أسهل الأدوات التسويقية لعرض فكرة مشروعك/شركتك او التعريف عنه والذي يبقى في أذهان عملائك طوال الوقت.</p>
                           <a href="%d9%81%d9%8a%d8%af%d9%8a%d9%88-%d9%85%d9%88%d8%b4%d9%86-%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83.html" class="button blue-reverce" title="موشن جرافيك">موشن جرافيك</a> 
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4">
                     <div class="service-box">
                        <div class="img"> <img src="public/storage/images/services/161217569645516.png" alt="السيرفرات الخاصة السعودية"> </div>
                        <div class="info">
                           <p class="info-content">الخادم هو السيرفر، كلاهما بنفس المعنى، ونفس المكونات، مكوّنات الخادم فائقةُ القدرة، وهي كذلك تحتاج اتصالًا آليًّا بإنترنت عالي السرعة</p>
                           <a href="servers.html" class="button blue-reverce" title="السيرفرات الخاصة السعودية">السيرفرات الخاصة السعودية</a> 
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4">
                     <div class="service-box">
                        <div class="img"> <img src="public/storage/images/services/161217618140716.png" alt="سيرفر VPS"> </div>
                        <div class="info">
                           <p class="info-content">توفر شركة إيج ميديا سيرفرات (VPS ) سعوديةً خاصةً، بأسعار مميزة، وضمان سرعة تصفح للبيانات ممتازة، من دون وجود أيَّ خلل في النظام البرمجي.</p>
                           <a href="hosting-vps.html" class="button blue-reverce" title="سيرفر VPS">سيرفر VPS</a> 
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4">
                     <div class="service-box">
                        <div class="img"> <img src="public/storage/images/services/161217632958818.png" alt="منتجات إيج ميديا"> </div>
                        <div class="info">
                           <p class="info-content">الاَن يمكنك الحصول على القالب المناسب لمشروعك، من خلال مجموعة متميزة من قوالب الهواتف الذكية والمتاجر الإلكترونية عالية الجودة باللغتين (العربية / الإنجليزية)،</p>
                           <a href="%d9%85%d9%86%d8%aa%d8%ac%d8%a7%d8%aa-%d8%a7%d9%88%d8%a7%d9%85%d8%b1-%d8%a7%d9%84%d8%b4%d8%a8%d9%83%d8%a9.html" class="button blue-reverce" title="منتجات إيج ميديا">منتجات إيج ميديا</a> 
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4">
                     <div class="service-box">
                        <div class="img"> <img src="public/storage/images/services/161451078312403.png" alt="الأمن السيبراني - أمن المعلومات"> </div>
                        <div class="info">
                           <p class="info-content">خدمة أمن المعلومات (السايبر سيكورتي) توفر الأمان الكامل لمعلوماتك حيث يتم اتخاذ أكثر من نسخة احتياطية وصد أي إختراق خارجي وبناء جدار حماية قوي للمواقع والتطبيقات ومراجعة النظام الأمني على مدار ساعات اليوم .</p>
                           <a href="%d8%a7%d9%84%d8%a7%d9%85%d9%86-%d8%a7%d9%84%d8%b3%d9%8a%d8%a8%d8%b1%d8%a7%d9%86%d9%8a.html" class="button blue-reverce" title="الأمن السيبراني - أمن المعلومات">الأمن السيبراني - أمن المعلومات</a> 
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-sm-6 col-md-4">
                     <div class="service-box">
                        <div class="img"> <img src="public/site/img/icons/web-design.png" alt="المزيد من الخدمات"> </div>
                        <div class="info">
                           <p class="info-content"> عرض المزيد من خدمات إيج ميديا لتقنية المعلومات </p>
                           <a href="%d8%a7%d9%84%d9%85%d8%b2%d9%8a%d8%af-%d9%85%d9%86-%d8%a7%d9%84%d8%ae%d8%af%d9%85%d8%a7%d8%aa.html" class="button blue-reverce" title="المزيد من الخدمات"> المزيد من الخدمات </a> 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="contact-us pt-40 pb-40 blue-bg">
            <div class="container">
               <div class="mb-40">
                  <h2 class="section-title"> اتصل بنا </h2>
                  <p class="section-subtitle"> نتشرف بخدمتكم وتواصلكم معنا </p>
               </div>
               <div class="row">
                  <div class="col-12 col-md-4">
                     <div class="contact-box green">
                        <i class="fab icon-fa-whatsapp"></i> 
                        <p class="title dark-color bold font-15 mb-10"> للتواصل عبر الواتساب </p>
                        <p class="info-content"> 0550016001 </p>
                        <a href="https://api.whatsapp.com/send?phone=966550016001" class="box-btn"> للتواصل عبر الواتساب </a> 
                     </div>
                  </div>
                  <div class="col-12 col-md-4">
                     <div class="contact-box blue">
                        <i class="fas icon-fa-phone-1"></i> 
                        <p class="title dark-color bold font-15 mb-10"> للإتصال والإستفسار </p>
                        <p class="info-content"> 0550016001 </p>
                        <a href="tel:0550016001" class="box-btn"> اتصل بنا </a> 
                     </div>
                  </div>
                  <div class="col-12 col-md-4">
                     <div class="contact-box dark">
                        <i class="far icon-fa-envelope"></i> 
                        <p class="title dark-color bold font-15 mb-10"> للمراسلة المباشرة </p>
                        <p class="info-content"> <a href="cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="88fbe9e4edfbc8e9e9e1fca6fbe9">[email&#160;protected]</a> </p>
                        <a href="cdn-cgi/l/email-protection.html#6c1f0d00091f2c0d0d0518421f0d" class="box-btn"> للمراسلة المباشرة </a> 
                     </div>
                  </div>
               </div>
               <div class="row justify-content-center">
                  <div class="col-12 col-md-8">
                     <form action="https://agemedia.sa/contact_us" class="contact-form mt-20" id="contact_us_form">
                        <input type="hidden" name="_token" value="xGpbyi1bg7S0dKzIOK8kd80hN9HbnukZYxRI4uk1"> <input type="text" name="name" class="input" placeholder="الاسم : "> <input type="number" name="phone" class="input" placeholder="رقم الهاتف "> <input type="email" name="email" class="input" placeholder="البريد الالكتروني "> 
                        <textarea class="input" name="message" placeholder="الرسالة"></textarea>
                        <div class="text-end"> <button class="button blue">ارسال</button> </div>
                     </form>
                  </div>
               </div>
            </div>
         </section>
      </div>
