      <div style="position: fixed; bottom: 0;  margin: 30px; margin-bottom: 80px; z-index: 99999"> <a href="https://api.whatsapp.com/send?phone=966550016001" target="_blank"> <img style="width: 50px; height: 50px" src="public/storage/images/png-whatsapp.png" alt="واتس إيج ميديا"> </a> </div>
      <footer class="footer pt-40 pb-40" style="background-image: url(public/site/img/footer-img.webp);">
         <div class="container">
            <div class="row">
               <div class="col-12 col-lg-4">
                  <a href="#" class="logo"> <img loading="lazy" src="public/site/img/logo.png" alt=" إيج ميديا"> </a> 
                  <p class="info-content">شركة تدعم المشاركة في العالم الرقمي والتواجد حيث يتواجد العملاء وتعلو المنافسة بين الشركات والنشاطات التجارية ، تقدم جميع خدمات البرمجة والتصميم والتسويق الالكتروني .</p>
                  <ul class="social">
                     <li> <a href="https://www.facebook.com/aaitsa1/" target="_blank" title="رابط حساب شركة إيج ميديا لتقنية المعلومات على الفيسبوك"> <i class="fab icon-fa-facebook-f"></i> </a> </li>
                     <li> <a href="https://www.linkedin.com/in/aaitsa1/" target="_blank" title="رابط حساب شركة إيج ميديا لتقنية المعلومات على لينكد إن"> <i class="fab icon-fa-linkedin-in"></i> </a> </li>
                     <li> <a href="https://twitter.com/aaitsa1" target="_blank" title="رابط حساب شركة إيج ميديا لتقنية المعلومات على تويتر"> <i class="fab icon-fa-twitter"></i> </a> </li>
                     <li> <a href="https://www.instagram.com/aaitsa1/" target="_blank" title="رابط حساب شركة إيج ميديا لتقنية المعلومات على انستغرام"> <i class="fab icon-fa-instagram"></i> </a> </li>
                     <li> <a href="https://www.youtube.com/channel/UC9QC5Xjme73ZINQbn86QSKA" target="_blank" title="رابط حساب شركة إيج ميديا لتقنية المعلومات على يوتيوب"> <i class="fab icon-fa-youtube"></i> </a> </li>
                     <li> <a href="https://www.snapchat.com/add/agemedia.sa" title="رابط حساب شركة إيج ميديا لتقنية المعلومات على سناب شات"> <i class="fab icon-fa-snapchat-ghost"></i> </a> </li>
                  </ul>
               </div>
               <div class="col-12 col-lg-2">
               </div>
               <div class="col-12 col-lg-3">
                  <div class="foot-list"> <a href="%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%a7%d9%84%d9%85%d9%88%d8%a7%d9%82%d8%b9.html" title="تصميم المواقع"> تصميم المواقع </a> <a href="%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%aa%d8%b7%d8%a8%d9%8a%d9%82%d8%a7%d8%aa-%d8%a7%d9%84%d8%ac%d9%88%d8%a7%d9%84.html" title="تصميم تطبيقات الجوال"> تصميم تطبيقات الجوال </a> <a href="%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d9%85%d8%aa%d8%ac%d8%b1-%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a.html" title="تصميم متجر الكتروني"> تصميم متجر الكتروني </a> <a href="%d8%a7%d9%84%d8%aa%d8%b3%d9%88%d9%8a%d9%82-%d8%a7%d9%84%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a.html" title="التسويق الالكتروني"> التسويق الالكتروني </a> <a href="%d8%a7%d9%84%d9%87%d9%88%d9%8a%d8%a9-%d8%a7%d9%84%d8%aa%d8%ac%d8%a7%d8%b1%d9%8a%d8%a9.html" title="الهوية التجارية"> الهوية التجارية </a> <a href="%d9%81%d9%8a%d8%af%d9%8a%d9%88-%d9%85%d9%88%d8%b4%d9%86-%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83.html" title="موشن جرافيك"> موشن جرافيك </a> <a href="%d8%b3%d9%8a%d8%a7%d8%b3%d8%a9-%d8%a7%d9%84%d9%85%d8%a4%d8%b3%d8%b3%d8%a9.html" title="سياسة الشركة">سياسة الشركة</a> <a href="%d8%a7%d9%84%d8%b4%d8%b1%d9%88%d8%b7-%d9%88%d8%a7%d9%84%d8%a2%d8%ad%d9%83%d8%a7%d9%85.html" title="الشروط والآحكام">الشروط والآحكام</a> <a href="%d8%a7%d9%84%d8%a7%d8%b3%d8%a6%d9%84%d8%a9-%d8%a7%d9%84%d8%b4%d8%a7%d8%a6%d8%b9%d8%a9.html" title="الاسئلة الشائعة">الاسئلة الشائعة</a> <a href="%d8%ae%d8%b1%d9%8a%d8%b7%d8%a9-%d8%a7%d9%84%d9%85%d9%88%d9%82%d8%b9.html" title="خريطة الموقع">خريطة الموقع</a> <a href="#" title=""></a> </div>
               </div>
               <div class="col-12 col-lg-3">
                  <div class="adress">
                     <p class="title"> العنوان </p>
                     <a href="#" class="link"> <i class="fas icon-fa-map-marked-alt"></i> الرياض ، العليا ، بجوارمكتبة الملك فهد ، بناية الربيعة . </a> 
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <div class="fixed-footer">
         <ul>
            <li> <a href="tel:920005929" title="920005929"> <i class="fas icon-fa-phone-1"></i> <span>رقم الهاتف</span> </a> </li>
            <li> <a href="phone%3d966555108237.html" title="966555108237"> <i class="fab icon-fa-whatsapp"></i> <span>رقم الواتساب</span> </a> </li>
            <li class="portfolio"> <a class="site-btn blue" href="uploads/setting/156802388685.html"> <i class="fas icon-fa-file-download"></i> <span>Portfolio</span> </a> </li>
         </ul>
      </div>
      <script data-cfasync="false" src="cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/javascript"> var right_arrow = "public/site/img/icons/right.png";
         var left_arrow = "public/site/img/icons/left.png"; 
      </script>  <script src="public/site/js/jquery-3.4.1.min.js" type="text/javascript"></script> <script src="../cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" type="text/javascript"></script> <script src="../maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" type="text/javascript"></script> <script src="../cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" type="text/javascript"></script> <script src="../cdnjs.cloudflare.com/ajax/libs/mixitup/3.3.1/mixitup.min.js" type="text/javascript"></script> <script src="public/site/js/main.js" type="text/javascript"></script> <script src="vendor/almasaeed2010/adminlte/plugins/toastr/toastr.min.js" type="text/javascript"></script> <script type="text/javascript"> $(document).ready(function(){
         setTimeout(() => {
         
             var latlng = new google.maps.LatLng(24.6870476,46.6867179);
             var map = new google.maps.Map(document.getElementById('map'), {
                 center: latlng,
                 zoom: 16,
                 disableDefaultUI: false,
                 animation: google.maps.Animation.DROP,
                 mapTypeId: google.maps.MapTypeId.ROADMAP
             });
         
             var marker = new google.maps.Marker({
                 position: latlng,
                 map: map,
             });
         
         }, 4000)
         })
         
         $(document).ready(function() {
         $('img').each(function() {
             if (!$(this).attr('alt'))
                 $(this).attr('alt', 'إيج ميديا لتقنية المعلومات');
         })
         }) 
      </script> <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAcNGRxy3xOCZTxU3TBg-TyUsEgbU1ltU&amp;callback=initMap" type="text/javascript"></script> <script type="text/javascript"> $(document).ready(function(){
         $(document).on('submit','#contact_us_form',function(e){
             e.preventDefault();
             var url = $(this).attr('action')
             $.ajax({
                 url: url,
                 method: 'post',
                 data: new FormData($(this)[0]),
                 dataType:'json',
                 processData: false,
                 contentType: false,
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
                 beforeSend: function(){
                     $('.error_meassages').remove();
                     $('#contact_us_form input').removeClass('border-danger')
                     $('#contact_us_form textarea').removeClass('border-danger')
                 },
                 success: function(response){
                     toastr.success('تم ارسال الرسالة الخاصة بك الي أحد موظفينا وسيتم الرد عليك بأسرع وقت ممكن');
                     $('#contact_us_form input').val('')
                     $('#contact_us_form textarea').val('')
                 },
                 error: function (xhr) {
                     $('.error_meassages').remove();
                     $('#contact_us_form input').removeClass('border-danger')
                     $.each(xhr.responseJSON.errors, function(key,value) {
                         $('#contact_us_form input[name='+key+']').addClass('border-danger')
                         $('#contact_us_form input[name='+key+']').after('<small class="form-text error_meassages text-danger">'+value+'</small>');
         
                         $('#contact_us_form textarea[name='+key+']').addClass('border-danger')
                         $('#contact_us_form textarea[name='+key+']').after('<small class="form-text error_meassages text-danger">'+value+'</small>');
                     });
                 },
             });
         
         });
         }); 
      </script> <script src="../ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="|49" defer=""></script>
   </body>
   <!-- Mirrored from agemedia.sa/من-نحن by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 23 Apr 2021 23:11:07 GMT -->
</html>