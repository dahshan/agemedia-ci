<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php 
// $error_message = '';
// $success_message = '';
// $CI =& get_instance();
// $CI->load->model('Model_common');
// $footer_setting = $CI->Model_common->all_footer_setting();
?>
<!doctype html> 
<html dir="rtl">
   <!-- Mirrored from agemedia.sa/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 23 Apr 2021 23:09:22 GMT -->
   <!-- Added by HTTrack -->
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <!-- /Added by HTTrack -->
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title> شركة إيج ميديا لتقنية المعلومات</title>
      <script src="cdn-cgi/apps/head/ha4mdZBJgn951gZlqOvtMxSxQF0.js"></script>
      <link rel="icon" href="public/site/img/logo.png" type="image/x-icon" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name=viewport content="width=device-width, initial-scale=1.0">
      <meta name=subject content="تقنيه المعلومات">
      <meta name=copyright content="إيج ميديا">
      <meta name=language content="ar">
      <meta name=robots content="index,follow">
      <meta name=revised content="Sunday, July 18th, 2018, 5:15 pm">
      <meta name=topic content="تقنيه المعلومات">
      <meta name=Classification content="Business">
      <meta name=author content="name, إيج ميديا">
      <meta name=designer content="Eslam">
      <meta name=reply-to content="sales@agemedia.sa">
      <meta name=owner content="عبدالله اللغبي">
      <meta name=category content="تقنيه المعلومات">
      <meta name=author content="شركة إيج ميديا لتقنية المعلومات">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta property="og:image" content="https://agemedia.sa/public/site/img/logo.png">
      <meta property="og:image:width" content="1146" />
      <meta property="og:image:height" content="614" />
      <meta name="csrf-token" content="xGpbyi1bg7S0dKzIOK8kd80hN9HbnukZYxRI4uk1" />
      <link rel="apple-touch-icon" sizes="57x57" href="public/site/icons/apple-icon-57x57.png">
      <link rel="apple-touch-icon" sizes="60x60" href="public/site/icons/apple-icon-60x60.png">
      <link rel="apple-touch-icon" sizes="72x72" href="public/site/icons/apple-icon-72x72.png">
      <link rel="apple-touch-icon" sizes="76x76" href="public/site/icons/apple-icon-76x76.png">
      <link rel="apple-touch-icon" sizes="114x114" href="public/site/icons/apple-icon-114x114.png">
      <link rel="apple-touch-icon" sizes="120x120" href="public/site/icons/apple-icon-120x120.png">
      <link rel="apple-touch-icon" sizes="144x144" href="public/site/icons/apple-icon-144x144.png">
      <link rel="apple-touch-icon" sizes="152x152" href="public/site/icons/apple-icon-152x152.png">
      <link rel="apple-touch-icon" sizes="180x180" href="public/site/icons/apple-icon-180x180.png">
      <link rel="icon" type="image/png" sizes="192x192" href="public/site/icons/android-icon-192x192.png">
      <link rel="icon" type="image/png" sizes="32x32" href="public/site/icons/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="96x96" href="public/site/icons/favicon-96x96.png">
      <link rel="icon" type="image/png" sizes="16x16" href="public/site/icons/favicon-16x16.png">
      <link rel="manifest" href="public/site/icons/manifest.json">
      <meta name="msapplication-TileColor" content="#ffffff">
      <meta name="msapplication-TileImage" content="https://agemedia.sa/public/site/icons/ms-icon-144x144.png">
      <meta name="theme-color" content="#ffffff">
      <link rel="canonical" href="index.php" />
      <meta property="og:type" content="website">
      <meta name="title" content="شركة إيج ميديا لتقنية المعلومات">
      <meta name="description" content="إيج ميديا شركة تقنية احترافية, خبراء فى تصميم المواقع الالكترونية،تصميم المتاجر الالكترونية،استضافة المواقع،تصميم تطبيقات الجوال بأحدث التقنيات العالمية">
      <meta name="keywords" content=" إيج ميديا, تصميم مواقع , تصميم تطبيقات،تصميم متاجر , شركة برمجة , تصميم مواقع سعودية, تقنيه المعلومات بالسعودية  " />
      <meta property="og:url" content="https://agemedia.sa/">
      <meta property="og:title" content="شركة إيج ميديا لتقنية المعلومات">
      <meta property="og:image" content="https://agemedia.sa/public/site/img/logo.png">
      <meta property="twitter:card" content="summary_large_image">
      <meta property="twitter:url" content="https://agemedia.sa/">
      <meta property="twitter:title" content="شركة إيج ميديا لتقنية المعلومات">
      <meta property="twitter:description" content="إيج ميديا شركة تقنية احترافية, خبراء فى تصميم المواقع الالكترونية،تصميم المتاجر الالكترونية،استضافة المواقع،تصميم تطبيقات الجوال بأحدث التقنيات العالمية">
      <meta property="twitter:image" content="https://agemedia.sa/public/site/img/logo.png">
      <link rel="stylesheet" href="public/site/css/libs.css">
      <link rel="preload" href="public/site/fonts/icomoon.ttf" as="font" crossorigin="anonymous">
      <link rel="preload" href="public/site/fonts/HSDream-Light.ttf" as="font" crossorigin="anonymous">
      <link rel="stylesheet" href="public/site/css/icomoon.css">
      <link rel="stylesheet" href="public/site/css/style.css">
      <link rel="stylesheet" href="vendor/almasaeed2010/adminlte/plugins/toastr/toastr.min.css">
      <meta name="csrf-token" content="xGpbyi1bg7S0dKzIOK8kd80hN9HbnukZYxRI4uk1">
   </head>
   <body class="dark-theme no-transition" dir="ltr">
      <form class="search-popup" method="post" action="#">
         <input type="hidden" name="_token" value=""> <span class="close-search">x</span> 
         <div class="container">
            <div class="row">
               <div class="col-12 col-sm-8"> <input type="text" name="text" class="form-control" placeholder="كلمات البحث" required=""> </div>
               <div class="col-6 col-sm-2">
                  <select name="service" class="form-control">
                     <option value="2">تصميم مواقع</option>
                     <option value="6">برمجة تطبيقات</option>
                     <option value="8">متاجر الكترونية</option>
                     <option value="10">التسويق الالكتروني</option>
                     <option value="16">الهوية التجارية</option>
                  </select>
               </div>
               <div class="col-6 col-sm-2"> <button class="site-btn blue" type="submit">بحث</button> </div>
            </div>
         </div>
      </form>
      <header class="header">
         <div class="up">
            <div class="container">
               <div class="inner">
                  <div class="right"> <a href="index.php" class="logo"> <img src="public/site/img/logo.png" alt="إيج ميديا"> </a> </div>
                  <div class="left">
                     <img src="public/site/img/vision.png" alt="إيج ميديا" class="vission"> 
                     <div class="mode-control"> <i class="fas icon-fa-sun light active"></i> <i class="fas icon-fa-moon dark"></i> </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="main-nav">
            <div class="overlay"></div>
            <div class="container">
               <div class="nav-inner">
                  <div class="nav-control">
                     <i class="fas icon-fa-search search-btn"></i> 
                     <ul class="nav-list">
                        <li class="list-item <?php if($this->uri->segment(1) == 'home' || $this->uri->segment(1) == '') echo"active";?>"> <a href="index.php" class="link">الرئيسية</a> </li>
                        <li class="list-item <?php if($this->uri->segment(1) == 'about') echo"active";?>"> <a href="about" class="link">عن الشركة</a> </li>
                        <li class="list-item">
                           <a href="#" class="link"> خدماتنا </a> <i class="fas icon-fa-angle-down"></i> 
                           <ul class="drop">
                              <li> <a href="%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%a7%d9%84%d9%85%d9%88%d8%a7%d9%82%d8%b9.html"> <img loading="lazy" alt="تصميم المواقع" src="public/storage/images/services/161096439473238.png"> تصميم المواقع </a> </li>
                              <li> <a href="%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d8%aa%d8%b7%d8%a8%d9%8a%d9%82%d8%a7%d8%aa-%d8%a7%d9%84%d8%ac%d9%88%d8%a7%d9%84.html"> <img loading="lazy" alt="تصميم تطبيقات الجوال" src="public/storage/images/services/161096922310301.png"> تصميم تطبيقات الجوال </a> </li>
                              <li> <a href="%d8%aa%d8%b5%d9%85%d9%8a%d9%85-%d9%85%d8%aa%d8%ac%d8%b1-%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a.html"> <img loading="lazy" alt="تصميم متجر الكتروني" src="public/storage/images/services/161096931426570.png"> تصميم متجر الكتروني </a> </li>
                              <li> <a href="%d8%a7%d9%84%d8%aa%d8%b3%d9%88%d9%8a%d9%82-%d8%a7%d9%84%d8%a7%d9%84%d9%83%d8%aa%d8%b1%d9%88%d9%86%d9%8a.html"> <img loading="lazy" alt="التسويق الالكتروني" src="public/storage/images/services/161217465254162.png"> التسويق الالكتروني </a> </li>
                              <li> <a href="%d8%a7%d9%84%d9%87%d9%88%d9%8a%d8%a9-%d8%a7%d9%84%d8%aa%d8%ac%d8%a7%d8%b1%d9%8a%d8%a9.html"> <img loading="lazy" alt="الهوية التجارية" src="public/storage/images/services/161217539756419.png"> الهوية التجارية </a> </li>
                              <li> <a href="%d9%81%d9%8a%d8%af%d9%8a%d9%88-%d9%85%d9%88%d8%b4%d9%86-%d8%ac%d8%b1%d8%a7%d9%81%d9%8a%d9%83.html"> <img loading="lazy" alt="موشن جرافيك" src="public/storage/images/services/161217553169452.png"> موشن جرافيك </a> </li>
                           </ul>
                        </li>
                        <li class="list-item <?php if($this->uri->segment(1) == 'works') echo"active";?>"> <a href="works" class="link">اعمالنا</a> </li>
                        <li class="list-item <?php if($this->uri->segment(1) == 'blog') echo"active";?>"> <a href="blog" class="link">المدونة</a> </li>
                        <li class="list-item <?php if($this->uri->segment(1) == 'media') echo"active";?>"> <a href="media" class="link">الرصد الاعلامي</a> </li>
                        <li class="list-item <?php if($this->uri->segment(1) == 'contact') echo"active";?>"> <a href="contact" class="link">اتصل بنا</a> </li>
                        <li class="list-item search-item"> <i class="fas icon-fa-search search-btn"></i> </li>
                     </ul>
                     <button class="nav-btn"> <span></span><span></span><span></span> </button> 
                  </div>
                  <div class="btns"> <a href="tel:0550016001" class="nav-contact"> <i class="fas icon-fa-phone-1"></i> 0550016001 </a> <a href="https://api.whatsapp.com/send?phone=966550016001" class="nav-contact"> <i class="fab icon-fa-whatsapp"></i> 0550016001 </a> </div>
               </div>
            </div>
         </div>
      </header>
