      <div class="content">
         <section class="inner-head" style="background-image: url(public/site/img/home-cover0.jpeg);">
            <div class="container">
               <h1 class="title">الرصد الاعلامي</h1>
            </div>
         </section>
         <section class="blogs pt-40 pb-40">
            <div class="container">
               <div class="row">
                  <div class="col-12 col-md-6">
                     <div class="blog-item d-flex">
                        <div class="blog-txt">
                           <a href="media/%d8%a3%d9%88%d8%a7%d9%85%d8%b1-%d8%a7%d9%84%d8%b4%d8%a8%d9%83%d8%a9-%d8%aa%d8%aa%d8%b5%d8%af%d8%b1-%d9%82%d8%a7%d8%a6%d9%85%d8%a9-%d9%87%d9%8a%d8%a6%d8%a9-%d8%a7%d9%84%d8%a7%d8%aa%d8%b5%d8%a7%d9%84%d8%a7%d8%aa-%d8%a7%d9%84%d8%b3%d8%b9%d9%88%d8%af%d9%8a%d8%a9.html" class="blue-color title" title="إيج ميديا تتصدر قائمة هيئة الاتصالات السعودية"> إيج ميديا تتصدر قائمة هيئة الاتصالات السعودية </a> 
                           <p class="grey-txt">&quot; صاحب النجاح الأكبر &quot; كان هذا هو الوصف المناسب عندما اختارت هيئة الاتصالات السعودية عدد التطبيقات الأكبر من بين التطبيقات المقدمة لها من برمجة وتنفيذ مؤسستنا .
                              تصدرت مؤسستنا لقائمة الشركات الم�
                           </p>
                           <a href="media/%d8%a3%d9%88%d8%a7%d9%85%d8%b1-%d8%a7%d9%84%d8%b4%d8%a8%d9%83%d8%a9-%d8%aa%d8%aa%d8%b5%d8%af%d8%b1-%d9%82%d8%a7%d8%a6%d9%85%d8%a9-%d9%87%d9%8a%d8%a6%d8%a9-%d8%a7%d9%84%d8%a7%d8%aa%d8%b5%d8%a7%d9%84%d8%a7%d8%aa-%d8%a7%d9%84%d8%b3%d8%b9%d9%88%d8%af%d9%8a%d8%a9.html" class="read-blog green" title="إيج ميديا تتصدر قائمة هيئة الاتصالات السعودية">مشاهده التفاصيل</a> 
                        </div>
                        <a href="media/%d8%a3%d9%88%d8%a7%d9%85%d8%b1-%d8%a7%d9%84%d8%b4%d8%a8%d9%83%d8%a9-%d8%aa%d8%aa%d8%b5%d8%af%d8%b1-%d9%82%d8%a7%d8%a6%d9%85%d8%a9-%d9%87%d9%8a%d8%a6%d8%a9-%d8%a7%d9%84%d8%a7%d8%aa%d8%b5%d8%a7%d9%84%d8%a7%d8%aa-%d8%a7%d9%84%d8%b3%d8%b9%d9%88%d8%af%d9%8a%d8%a9.html" title="إيج ميديا تتصدر قائمة هيئة الاتصالات السعودية" class="blog-img"> <img src="public/storage/images/media/161426760597569.png" alt="إيج ميديا تتصدر قائمة هيئة الاتصالات السعودية"> </a> 
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="blog-item d-flex">
                        <div class="blog-txt">
                           <a href="media/%d8%aa%d8%b9%d8%a7%d9%82%d8%af-%d8%a7%d8%aa%d8%ad%d8%a7%d8%af-%d8%a7%d9%84%d9%83%d8%b1%d8%a9-%d9%88%d8%a3%d9%88%d8%a7%d9%85%d8%b1-%d8%a7%d9%84%d8%b4%d8%a8%d9%83%d8%a9.html" class="blue-color title" title="تعاقد اتحاد الكرة وإيج ميديا"> تعاقد اتحاد الكرة وإيج ميديا </a> 
                           <p class="grey-txt">كان لنا الشرف كوننا الشركة التي قام اتحاد الكرة بالتعاقد معها لتطوير الموقع الخاص بها ، حيث تم ذلك في مؤتمر صحفي في قاعة المؤتمرات الخاصة بالاتحاد المصري بحضور مدير المكتب الإعلامي بالاتحاد المصري الكابتن &quot; عز�</p>
                           <a href="media/%d8%aa%d8%b9%d8%a7%d9%82%d8%af-%d8%a7%d8%aa%d8%ad%d8%a7%d8%af-%d8%a7%d9%84%d9%83%d8%b1%d8%a9-%d9%88%d8%a3%d9%88%d8%a7%d9%85%d8%b1-%d8%a7%d9%84%d8%b4%d8%a8%d9%83%d8%a9.html" class="read-blog green" title="تعاقد اتحاد الكرة وإيج ميديا">مشاهده التفاصيل</a> 
                        </div>
                        <a href="media/%d8%aa%d8%b9%d8%a7%d9%82%d8%af-%d8%a7%d8%aa%d8%ad%d8%a7%d8%af-%d8%a7%d9%84%d9%83%d8%b1%d8%a9-%d9%88%d8%a3%d9%88%d8%a7%d9%85%d8%b1-%d8%a7%d9%84%d8%b4%d8%a8%d9%83%d8%a9.html" title="تعاقد اتحاد الكرة وإيج ميديا" class="blog-img"> <img src="public/storage/images/media/161877106265337.png" alt="تعاقد اتحاد الكرة وإيج ميديا"> </a> 
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="blog-item d-flex">
                        <div class="blog-txt">
                           <a href="media/%d9%83%d9%84%d9%85%d8%a9-%d8%a7%d9%84%d8%ae%d8%a8%d9%8a%d8%b1-%d8%a7%d9%84%d8%aa%d9%82%d9%86%d9%8a-%d8%b9%d8%a8%d8%af%d8%a7%d9%84%d9%84%d9%87-%d8%a7%d9%84%d8%b3%d8%a8%d8%b9-%d8%b9%d9%86-%d8%a7%d9%84%d9%85%d8%a4%d8%b3%d8%b3%d8%a9.html" class="blue-color title" title="كلمة الخبير التقني عبدالله السبع عن الشركة"> كلمة الخبير التقني عبدالله السبع عن الشركة </a> 
                           <p class="grey-txt">تشرفنا في شركة إيج ميديا بـ استشهاد الخبير التقني &quot; عبدالله السبع &quot; والذي يعمل كمحرر تقني في صحيفة إندبندنت العربية&nbsp; بالشركة وأعمالها ، وذِكره أننا شركة رائدة في مجال تصميم المواق�</p>
                           <a href="media/%d9%83%d9%84%d9%85%d8%a9-%d8%a7%d9%84%d8%ae%d8%a8%d9%8a%d8%b1-%d8%a7%d9%84%d8%aa%d9%82%d9%86%d9%8a-%d8%b9%d8%a8%d8%af%d8%a7%d9%84%d9%84%d9%87-%d8%a7%d9%84%d8%b3%d8%a8%d8%b9-%d8%b9%d9%86-%d8%a7%d9%84%d9%85%d8%a4%d8%b3%d8%b3%d8%a9.html" class="read-blog green" title="كلمة الخبير التقني عبدالله السبع عن الشركة">مشاهده التفاصيل</a> 
                        </div>
                        <a href="media/%d9%83%d9%84%d9%85%d8%a9-%d8%a7%d9%84%d8%ae%d8%a8%d9%8a%d8%b1-%d8%a7%d9%84%d8%aa%d9%82%d9%86%d9%8a-%d8%b9%d8%a8%d8%af%d8%a7%d9%84%d9%84%d9%87-%d8%a7%d9%84%d8%b3%d8%a8%d8%b9-%d8%b9%d9%86-%d8%a7%d9%84%d9%85%d8%a4%d8%b3%d8%b3%d8%a9.html" title="كلمة الخبير التقني عبدالله السبع عن الشركة" class="blog-img"> <img src="public/storage/images/media/161452163021673.png" alt="كلمة الخبير التقني عبدالله السبع عن الشركة"> </a> 
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="blog-item d-flex">
                        <div class="blog-txt">
                           <a href="media/%d9%83%d9%84%d9%85%d8%a9-%d8%a7%d9%84%d9%85%d9%85%d8%ab%d9%84-%d8%a7%d9%84%d9%85%d8%a8%d8%af%d8%b9-%d8%a8%d8%af%d8%b1-%d8%a7%d9%84%d9%84%d8%ad%d9%8a%d8%af-%d8%b9%d9%86-%d8%a7%d9%84%d9%85%d8%a4%d8%b3%d8%b3%d8%a9.html" class="blue-color title" title="كلمة الممثل المبدع بدر اللحيد عن الشركة"> كلمة الممثل المبدع بدر اللحيد عن الشركة </a> 
                           <p class="grey-txt">&quot; أفكاركم أوامر &quot; استخدمها الممثل السعودي المُبدع &quot; بدر اللحيد &quot; واصفاً لقدرتنا في إيج ميديا على تحويل جميع أفكاركم و مبادراتكم إلى أرض الواقع .
                              قام الممثل &quot; بدر اللحيد &quot; بالشكر في خدمات أوامر الشب�
                           </p>
                           <a href="media/%d9%83%d9%84%d9%85%d8%a9-%d8%a7%d9%84%d9%85%d9%85%d8%ab%d9%84-%d8%a7%d9%84%d9%85%d8%a8%d8%af%d8%b9-%d8%a8%d8%af%d8%b1-%d8%a7%d9%84%d9%84%d8%ad%d9%8a%d8%af-%d8%b9%d9%86-%d8%a7%d9%84%d9%85%d8%a4%d8%b3%d8%b3%d8%a9.html" class="read-blog green" title="كلمة الممثل المبدع بدر اللحيد عن الشركة">مشاهده التفاصيل</a> 
                        </div>
                        <a href="media/%d9%83%d9%84%d9%85%d8%a9-%d8%a7%d9%84%d9%85%d9%85%d8%ab%d9%84-%d8%a7%d9%84%d9%85%d8%a8%d8%af%d8%b9-%d8%a8%d8%af%d8%b1-%d8%a7%d9%84%d9%84%d8%ad%d9%8a%d8%af-%d8%b9%d9%86-%d8%a7%d9%84%d9%85%d8%a4%d8%b3%d8%b3%d8%a9.html" title="كلمة الممثل المبدع بدر اللحيد عن الشركة" class="blog-img"> <img src="public/storage/images/media/161452223823842.png" alt="كلمة الممثل المبدع بدر اللحيد عن الشركة"> </a> 
                     </div>
                  </div>
                  <div class="col-12 col-md-6">
                     <div class="blog-item d-flex">
                        <div class="blog-txt">
                           <a href="media/%d8%b4%d9%87%d8%a7%d8%af%d8%a9-%d8%a7%d9%84%d8%a3%d9%8a%d8%b2%d9%88-9001-%d9%84%d9%85%d8%a4%d8%b3%d8%b3%d8%a9-%d8%a3%d9%88%d8%a7%d9%85%d8%b1-%d8%a7%d9%84%d8%b4%d8%a8%d9%83%d8%a9.html" class="blue-color title" title="شهادة الأيزو 9001 لشركة إيج ميديا"> شهادة الأيزو 9001 لشركة إيج ميديا </a> 
                           <p class="grey-txt">تتوالى نجاحات شركة إيج ميديا التي صنعتها يداً بيد مع شركائها وعملائها في تحويل أفكارهم إلى مشاريع قائمة ومنصات إلكترونية تفاعلية على أرض الواقع، حيث تميزت الأعمال التي نفذتها (إيج ميديا) وساهمت في تطويرها وبرم�</p>
                           <a href="media/%d8%b4%d9%87%d8%a7%d8%af%d8%a9-%d8%a7%d9%84%d8%a3%d9%8a%d8%b2%d9%88-9001-%d9%84%d9%85%d8%a4%d8%b3%d8%b3%d8%a9-%d8%a3%d9%88%d8%a7%d9%85%d8%b1-%d8%a7%d9%84%d8%b4%d8%a8%d9%83%d8%a9.html" class="read-blog green" title="شهادة الأيزو 9001 لشركة إيج ميديا">مشاهده التفاصيل</a> 
                        </div>
                        <a href="media/%d8%b4%d9%87%d8%a7%d8%af%d8%a9-%d8%a7%d9%84%d8%a3%d9%8a%d8%b2%d9%88-9001-%d9%84%d9%85%d8%a4%d8%b3%d8%b3%d8%a9-%d8%a3%d9%88%d8%a7%d9%85%d8%b1-%d8%a7%d9%84%d8%b4%d8%a8%d9%83%d8%a9.html" title="شهادة الأيزو 9001 لشركة إيج ميديا" class="blog-img"> <img src="public/storage/images/media/161519988253608.png" alt="شهادة الأيزو 9001 لشركة إيج ميديا"> </a> 
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="contact-us pt-40 pb-40 blue-bg">
            <div class="container">
               <div class="mb-40">
                  <h2 class="section-title"> اتصل بنا </h2>
                  <p class="section-subtitle"> نتشرف بخدمتكم وتواصلكم معنا </p>
               </div>
               <div class="row">
                  <div class="col-12 col-md-4">
                     <div class="contact-box green">
                        <i class="fab icon-fa-whatsapp"></i> 
                        <p class="title dark-color bold font-15 mb-10"> للتواصل عبر الواتساب </p>
                        <p class="info-content"> 0550016001 </p>
                        <a href="https://api.whatsapp.com/send?phone=966550016001" class="box-btn"> للتواصل عبر الواتساب </a> 
                     </div>
                  </div>
                  <div class="col-12 col-md-4">
                     <div class="contact-box blue">
                        <i class="fas icon-fa-phone-1"></i> 
                        <p class="title dark-color bold font-15 mb-10"> للإتصال والإستفسار </p>
                        <p class="info-content"> 0550016001 </p>
                        <a href="tel:0550016001" class="box-btn"> اتصل بنا </a> 
                     </div>
                  </div>
                  <div class="col-12 col-md-4">
                     <div class="contact-box dark">
                        <i class="far icon-fa-envelope"></i> 
                        <p class="title dark-color bold font-15 mb-10"> للمراسلة المباشرة </p>
                        <p class="info-content"> <a href="cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="0b786a676e784b6a6a627f25786a">[email&#160;protected]</a> </p>
                        <a href="cdn-cgi/l/email-protection.html#ef9c8e838a9caf8e8e869bc19c8e" class="box-btn"> للمراسلة المباشرة </a> 
                     </div>
                  </div>
               </div>
               <div class="row justify-content-center">
                  <div class="col-12 col-md-8">
                     <form action="https://agemedia.sa/contact_us" class="contact-form mt-20" id="contact_us_form">
                        <input type="hidden" name="_token" value="xGpbyi1bg7S0dKzIOK8kd80hN9HbnukZYxRI4uk1"> <input type="text" name="name" class="input" placeholder="الاسم : "> <input type="number" name="phone" class="input" placeholder="رقم الهاتف "> <input type="email" name="email" class="input" placeholder="البريد الالكتروني "> 
                        <textarea class="input" name="message" placeholder="الرسالة"></textarea>
                        <div class="text-end"> <button class="button blue">ارسال</button> </div>
                     </form>
                  </div>
               </div>
            </div>
         </section>
      </div>
